﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using NLog;

namespace HrAdmin2.Controllers
{
    public class BaseMvcController : Controller
    {
        // TODO: injectable Controller dependencies such as db context, logger, etc...
        // (needs custom IControllerFactory registered on app startup)
        // e.g. http://www.codeproject.com/Articles/560798/ASP-NET-MVC-Controller-Dependency-Injection-for-Be

        protected HrAdminDbContext db = new HrAdminDbContext();
        protected CurrentUserInfo ui;
        protected BaseVm ViewModel;
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        // http://msdn.microsoft.com/en-us/library/system.web.mvc.controller.initialize(v=vs.118).aspx
        protected override void Initialize(System.Web.Routing.RequestContext requestContext)
        {
            ui = db.GetSingleByProc<CurrentUserInfo>("GetUserInfoNew", new { LanId = requestContext.HttpContext.User.Identity.Name });
            ViewModel = new BaseVm { CurrentUser = ui }; // replaceable with BaseVm subclass at runtime...
            base.Initialize(requestContext);
        }

        // inspired slightly by http://stackoverflow.com/questions/8144695/asp-net-mvc-custom-handleerror-filter-specify-view-based-on-exception-type
        protected override void OnException(ExceptionContext filterContext)
        {
            base.OnException(filterContext);
            try
            {
                HandleError(filterContext.Exception);
            }
            catch (Exception xcp)
            {
                throw new Exception("Error handling failed in BaseMvcController", xcp);
            }
            filterContext.Result = View("~/Views/Shared/Error.cshtml");
            filterContext.ExceptionHandled = true;
            filterContext.HttpContext.Response.Clear();
            filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError.GetTypeCode();
            filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        }

        protected void HandleError(Exception xcp) {
            var oxcp = GetOriginalException(xcp);
            var lei = new LogEventInfo
            {
                Level = LogLevel.Error,
                Exception = oxcp,
                //                    Message = "Error occurred on PostEmail.  Sent Email object: " + Json.Encode(sent),
                Message = oxcp.Message + " in " + this.GetType().FullName + ": " + System.Reflection.MethodBase.GetCurrentMethod().Name,
                TimeStamp = DateTime.Now,
                LoggerName = logger.Name
            };
            // enumerate appSettings from web.config (TODO: use StringBuilder instead):
            var ws = System.Web.Configuration.WebConfigurationManager.AppSettings;
            foreach (var k in ws.AllKeys)
            {
                lei.Message += "<br/>" + k + ": " + ws[k];
            }
            lei.SetStackTrace(new System.Diagnostics.StackTrace(oxcp), 0);
            logger.Log(lei);
        }

        private Exception GetOriginalException(Exception xcp)
        {
            return (xcp.InnerException == null) ? xcp : GetOriginalException(xcp.InnerException);
        }

    }
}

namespace HrAdmin2.Models
{
    public class CurrentUserInfo
    {
        public int UserId { get; set; } // can be different from real user id if impersonating...
        public string RealLanId { get; set; }
        public string LanId { get; set; } // impersonated LanId if impersonating
        public string RoleName { get; set; } // impersonated Role
        public string FirstName { get; set; } // impersonated Role
        public string EmailAddress { get; set; }
        public int? PositionId { get; set; }
    }
}