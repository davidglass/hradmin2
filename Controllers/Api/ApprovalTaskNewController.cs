﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Helpers;

using HrAdmin2.Models;
using HrAdmin2.Models.Util;
using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    [BaseApiAuth]
    public class ApprovalTaskNewController : BaseApiController
    {
        public HttpResponseMessage Put(ApprovalTaskUpdate updateTask)
        {
            updateTask.UpdaterLanId = ui.LanId;
            try
            {
                var approver = db.GetSingleByProc<TaskApprover>("UpdateApprovalStatus", updateTask);
                hrm.StatusCode = HttpStatusCode.OK;
                hrm.Content = new StringContent(Json.Encode(approver));
//                hrm.StatusCode = db.Exec("UpdateApprovalStatus", updateTask) ?
//                    HttpStatusCode.OK : HttpStatusCode.NotModified;
                Notifier.NotifyNextApprover(updateTask, db);
            }
            catch (Exception xcp)
            {
                logger.Log(NLog.LogLevel.Error, xcp.Message);
                hrm = ControllerContext.Request.CreateErrorResponse(
                // InternalServer Error since Unauthorized triggers login box on IE8
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message
                );
            }
            return hrm;
        }
    }
}