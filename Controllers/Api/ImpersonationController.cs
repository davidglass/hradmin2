﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HrAdmin2.Models.ViewModels;
using System.Web.Helpers;
using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    [BaseApiAuth]
    public class ImpersonationController : BaseApiController
    {
        public HttpResponseMessage Put(Impersonation imp)
        {
            try
            {
                // commented out EF-style update...
                //db.JobClasses.Attach(jc);
                //var e = db.Entry<JobClass>(jc);
                //e.Property("IsActive").IsModified = true;
                //var updated = db.SaveChanges();
                if (!db.Exec("UpdateImpersonation", new { LanId = User.Identity.Name, ImpersonatedUserId = imp.ImpersonatingUserId }))
                {
                    throw new Exception("Impersonation Update failed.");
                }
                hrm.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception xcp)
            {
                logger.Log(NLog.LogLevel.Error, xcp.Message);
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }
    }
}
