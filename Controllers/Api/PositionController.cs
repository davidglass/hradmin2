﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers.Api
{
    public class PositionController : BaseApiController
    {
        public PositionVm Get(int id)
        {
            Position position = db.GetSingleByProc<Position>("GetPositionDetail", parameters: new { PosId = id, LanId = ui.LanId });
            var pvm = new PositionVm(position, db)
            {
                CurrentUser = ui
            };
            return pvm;
        }

        // *NOTE*, must use different param name to distinguish this signature from Get(id): 
        [ActionName("GetPdHistory")] 
        public PdHistoryVm GetPdHistory(int positionid)
        {
            return new PdHistoryVm
            {
                CurrentUser = ui,
                Pos = db.GetSingleByProc<Position>("GetPosition", new { Id = positionid }),
                //PositionId = positionid, // pass thru
                History = db.GetByProc<PositionDescriptionRow>("GetPositionDescriptions", parameters: new { PositionId = positionid })
            };
        }

    // commented out later stuff...currently unused...

        //        private CtsIntranetAppContext db = new CtsIntranetAppContext();
        //private HrAdminDbContext db = new HrAdminDbContext();

        // GET api/Position
        //public IEnumerable<Position> GetPositions()
        //{
        //    return db.Positions.AsEnumerable();
        //}

        //GET api/Position/5
/*        public PositionVm GetPosition(int id)
        {
            // DG: by convention, id of 0 means return dummy instance of model to pre-fill default form values:
            if (id == 0)
            {
                return new PositionVm(new Position(), db);
            }
            Position position = db.Positions.Find(id);
            if (position == null)
            {
                throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
            }
            var pvm = new PositionVm(position, db);
            return pvm;
        }
*/
        // TODO: consolidate the two calls into a single call.
        // if position is vacant, return default values for new ActionRequest and Appointment.
        //  PositionDetail:
        //  PositionTab
        //  AppointmentTab(s)
        //    ActionRequest
        //    EmployeeInfo
        //    AppointmentInfo
        //    PositionInfo
//        public PositionDetailTabs GetPositionDetail (int id)
//        public PositionDetailTabs GetPosition(int id)
//        {
//            // built-in EF Find():
//            //Position position = db.Positions.Find(id);
//            //if (position == null)
//            //{
//            //    throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
//            //}
//            Position position = db.GetSingleByProc<Position>("GetPositionDetail", parameters: new { PosId = id });
//            var pvm = new PositionVm(position, db);
//            return new PositionDetailTabs(pvm, db);
//        }

//        // TODO: this is really a POST, not a GET.
//        // should at least set Created (201) status and include new Location header in response.
//        // see "Creating a Resource" here:
//        // http://www.asp.net/web-api/overview/creating-web-apis/creating-a-web-api-that-supports-crud-operations
//        [ActionName("Par")]
//        public HttpResponseMessage PostPar(int id, Par p)
//        {
//            ParDetail newPar;
//            ParDetailVm newParVm;
//            HttpResponseMessage rsp;

////            switch (p.ActionType)
////            {
////                case "U0":
////                    newPar = db.GetSingleByProc<ParDetail>("CreateNewHirePar",
////                        parameters: new
////                        {
////                            LanId = User.Identity.Name,
////                            PosId = p.PositionId
////                        });
//////                    newPar.ActionReasonId = 0; // must set default or binding fails...
////                    newParVm = new ParDetailVm(newPar, db);
////                    // TODO: insert record, return with new Id...
////                    rsp = Request.CreateResponse<ParDetailVm>(HttpStatusCode.Created, newParVm);
////                    rsp.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = newPar.Id }));
////                    return rsp;
////                default:
//                    try
//                    {
//                        //create workflow for Par
//                        int approvalWorkflowId = (new ApprovalWorkflowController()).CreateApprovalWorkflow(
//                            ApprovalWorkflow.approvalApp.PAR, p.PositionId, p.EmployeeId);
//                        p.ApprovalWorkflowId = approvalWorkflowId;

//                        newPar = db.GetSingleByProc<ParDetail>("CreatePar",
//                            parameters: new
//                            {
//                                LanId = User.Identity.Name,
//                                PosId = p.PositionId,
//                                ApprovalWorkflowId = approvalWorkflowId,
//                                ActionType = p.ActionType
//                            });
//                        //                    newPar.ActionReasonId = 0; // must set default or binding fails...
//                        newParVm = new ParDetailVm(newPar, db);
//                        // TODO: insert record, return with new Id...
//                        rsp = Request.CreateResponse<ParDetailVm>(HttpStatusCode.Created, newParVm);
//                        rsp.Headers.Location = new Uri(Url.Link("DefaultApi", new { id = newPar.Id }));
//                        return rsp;
//                    }
//                    catch (Exception xcp)
//                    {
//                        return new HttpResponseMessage()
//                        {
//                            StatusCode = HttpStatusCode.InternalServerError,
//                            ReasonPhrase = "Create Failed: "+xcp.Message
//                        };
//                    }
//            //}
//        }

//        public HttpResponseMessage Put(string id, PosUpdateVals pu)
////        public HttpResponseMessage Put(string posids, PosUpdateVals puv)
//        {
//            bool result = false;
//            try
//            {
//                result = db.Exec("UpdatePositions", new { PosIds = String.Format(",{0},", id), NewSupervisorEmpId = pu.SupervisorEmpId });
//                if (!result)
//                {
//                    throw new Exception("Position bulk-update failed.");
//                }
//            }
//            catch (Exception xcp)
//            {
//                return new HttpResponseMessage()
//                {
//                    StatusCode = HttpStatusCode.InternalServerError,
//                    ReasonPhrase = "Update Failed: " + xcp.Message
//                };
//            }
//            return new HttpResponseMessage(HttpStatusCode.OK);
//        }

//        // List of ApptTabs is returned for multi-fill situations,
//        // but most of the time only 1 will be used.
//        //public List<ApptTab> GetIncumbents(int posid)
//        //{
//        //    // TODO: always return empty / default Appointment at index 0 of ApptTabs
//        //    // to pre-fill defaults (e.g. fill level) for new hire.
//        //    // when this is done, fake tab below can be removed.
//        //    var apptlist = new ApptTabs(posid, db).Tabs;
//        //    // pre-pend empty ApptTab for New Hire...
//        //    //             var newAppt = new EmployeeAppt() {  // blank / default / empty appointment for New Hire
//        //    //    Id = 0
//        //    //}

//        //    if (apptlist.Count ==0) { // vacant position
//        //        apptlist.Add(new ApptTab(
//        //            new EmployeeAppt() {
//        //                Id = 0,
//        //                PreferredName = "[new]",
//        //                LastName = "[new]"
//        //            }));
//        //    }
//        //    return apptlist;
//        //}

//        protected override void Dispose(bool disposing)
//        {
//            db.Dispose();
//            base.Dispose(disposing);
//        }
    }
}

//namespace HrAdmin2.Models
//{
//    public class PosUpdateVals
//    {
//        public int SupervisorEmpId { get; set; }
//    }
//}