﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using System.Data;
using System.Web.Helpers;

using HrAdmin2.Models;
using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    [BaseApiAuth]
    public class PositionDutyController : BaseApiController
    {
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                var pd = db.PositionDuties.Find(id);
                if (pd == null)
                {
                    throw new Exception("Duty not found.");
                }
                if (!HasAccess(pd.PositionDescriptionId, ModelAction.Delete))
                {
                    throw new Exception("Unauthorized.");
                }
                // the ridiculously verbose way of deleting by ID from EF without pre-query.
                // skipping now since pre-querying for PdId anyway...
                //var pd = new PositionDuty
                //{
                //    Id = id
                //};
                //var entstate = db.Entry(pd).State;
                //db.PositionDuties.Attach(pd);
                db.PositionDuties.Remove(pd);
                var deleted = db.SaveChanges();
                if (deleted != 1)
                {
                    throw new Exception("delete PositionDuty failed.");
                }
                else { hrm.StatusCode = HttpStatusCode.OK; }
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        public HttpResponseMessage Post(PositionDuty pd)
        {
            try
            {
                if (!HasAccess(pd.PositionDescriptionId, ModelAction.Create))
                {
                    throw new Exception("Unauthorized.");
                }
                // hack since EF doesn't support default next value sequences:
                // sequence returns Int64, not Int32 (should be safe to cast for a while)
                pd.Id = (int)db.Database.SqlQuery<Int64>("select next value for dbo.PositionDuty_seq").FirstOrDefault();
                db.PositionDuties.Add(pd);
                // this should set new ID on pd:
                int saved = db.SaveChanges();
                hrm.Content = new StringContent(Json.Encode(pd));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                uxcp.InnerException.GetType().ToString() + ":" + uxcp.Message
                );
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
                //hrm.Content = new StringContent(Json.Encode(
                //    new
                //    {
                //        error = xcp.InnerException.GetType().ToString() + ":" + (xcp.InnerException.InnerException == null ? "" : xcp.InnerException.InnerException.Message)
                //    }));
                //hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Put(PositionDuty pd)
        {

            // moved to base class:
//            var hrm = new HttpResponseMessage();
            try
            {
                if (!HasAccess(pd.PositionDescriptionId, ModelAction.Update))
                {
                    throw new Exception("Unauthorized.");
                }
                db.PositionDuties.Attach(pd);
                var e = db.Entry<PositionDuty>(pd);
                e.State = EntityState.Modified;
                var updated = db.SaveChanges();
                if (updated < 1)
                {
                    throw new Exception("PositionDuty update failed...");
                }
            }
            catch (Exception xcp)
            {
                logger.Error("PositionDuty update failed: " + xcp.Message);
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        // TODO: move this into base class, inject AccessCheck sproc name...
        private bool HasAccess(int id, ModelAction xn)
        {
            var permitted = ActionFlags.Read;
            if (xn != ModelAction.Read)
            {
                // if PD Workflow status is Created and Position is not own or user is Admin/HR, add Write+Delete:
                // TODO: lookup PositionDescription Id from Id if Delete action...
                //var pmal = db.GetByProc<ModelActionVal>("GetPdPermittedActions", new { PdId = id, LanId = ui.LanId, IsDuty = (xn == ModelAction.Delete) });
                var pmal = db.GetByProc<ModelActionVal>("GetPdPermittedActions", new { PdId = id, LanId = ui.LanId });
                foreach (var pma in pmal)
                {
                    if (!pma.Value.HasValue) { continue; }
                    permitted = permitted | (ActionFlags)pma.Value.Value;
                }

                if (!permitted.HasFlag((ActionFlags)xn))
                {
                    return false;
                }
            }

            // TODO: lookup PdId from Duty Id for delete action here...
            return db.GetSingleByProc<AccessCheck>("PdPosIsInSpanOfControl", new
            {
                PdId = id,
                LanId = ui.LanId
            }).IsSub;
        }
    }
}