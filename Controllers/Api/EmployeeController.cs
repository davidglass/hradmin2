﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

// for [ScriptIgnore]:
using System.Web.Script.Serialization;
// for [NotMapped]:
using System.ComponentModel.DataAnnotations.Schema;

using HrAdmin2.Models.ViewModels;
using HrAdmin2.Models;

namespace HrAdmin2.Controllers.Api
{
    public class EmployeeController : BaseApiController
    {
        // TODO: id == "current" ? lookup from CurrentUser, else ParseInt.
//        public EmployeeSummaryVm GetDetail(string id)
        public EmployeeSummaryVm Get(string id)
        {
            int idInt;
            Int32.TryParse(id, out idInt); // set to 0 on fail (e.g. id="current")
            var rp = new
            {
                Id = idInt, // 
                LanId = ui.LanId // potentially Impersonated...
            };

//            var ei = new EmployeeSummary(db, "GetEmpSummary", ActiveRecord.EntityType.Proc, rp)
            var ei = new EmployeeInfo
            {
                Context = db,
                Source = "GetEmpSummary",
                SourceType = ActiveRecord.EntityType.Proc,
                RecordParams = rp
            }
            .Get<EmployeeInfo>();

            ei.Context = db; // need to re-add Context to returned generic ActiveRecord (can't cast to unknown runtime type)

            // TODO (2014-08-18): write, test GetDirectReports proc...
            // this function could also be put into vm.Init() ???
            var dr = (List<DirectReport>)ei.GetAll<DirectReport>("GetDirectReports", new { Id = ei.Id, LanId = ui.LanId });

            var evm = new EmployeeSummaryVm
            {
                CurrentUser = ui,
                EmpInfo = ei,
                DirectReports = dr
            };
            return evm;
        }
    }

    public class EmployeeSummaryVm : BaseVm
    {
        public EmployeeInfo EmpInfo { get; set; }
        public List<DirectReport> DirectReports { get; set; }
    }

    // TODO: expand ModelBase with ActiveRecord-like functionality, BaseTableName, static CRUD etc.
//    public class DirectReport : ActiveRecord
    public class DirectReport // not AR if child relation of AR...
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? EmployeeId { get; set; }
        //[System.ComponentModel.DataAnnotations.Key]
        public int PositionId { get; set; }
        public string LegacyClassCode { get; set; }
        public string WorkingTitle { get; set; }
        public DateTime? PdDate { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public int PdId { get; set; }
        public int DaysOld { get; set; }
        public string PdJobClass { get; set; }
        //public bool IsVacant { get; set; }
    }

    public class EmployeeInfo : ActiveRecord
    {
        //public EmployeeSummary()
        //    : base()
        //{
        //}
        //public EmployeeSummary(HrAdminDbContext ctx, string src, EntityType stype, object rp)
        //    :base(ctx, src, stype, rp)
        //{
        //}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int? PositionId { get; set; } // nullable for ex-employees.  proc returns only current appointments.
        public string WorkingTitle { get; set; }
        public string JobClass { get; set; }
        public int OrgUnitId { get; set; }
        public string OrgUnitName { get; set; }

        [ScriptIgnore]
        public HrAdminDbContext Context {
            get
            {
                return base.Context;
            }
            set
            {
                base.Context = value;
            }
        }
        [ScriptIgnore]
        //public string Source { get; set; } // can be Proc, Table, or View
        public string Source
        {
            get
            {
                return base.Source;
            }
            set
            {
                base.Source = value;
            }
        }

        [ScriptIgnore]
        //public EntityType SourceType { get; set; }
        public EntityType SourceType
        {
            get
            {
                return base.SourceType;
            }
            set
            {
                base.SourceType = value;
            }
        }

        [ScriptIgnore]
        //public object RecordParams { get; set; }
        public object RecordParams
        {
            get
            {
                return base.RecordParams;
            }
            set
            {
                base.RecordParams = value;
            }
        }
    }

    public class ActiveRecord
    {
        public enum EntityType { Proc, Table, View }
        public int Id { get; set; }  // trying to move this
        //        public System.Data.Entity.DbContext Context { get; set; }
        // TODO: extend DbContext, creating BaseDbContext...for now just hardcoding HrAdminDbContext...
        [ScriptIgnore]
        protected HrAdminDbContext Context { get; set; }
        [ScriptIgnore]
        protected string Source { get; set; } // can be Proc, Table, or View
        [ScriptIgnore]
        protected EntityType SourceType { get; set; }
        [ScriptIgnore]
        protected object RecordParams { get; set; }

        //public ActiveRecord()
        //{
        //}

        ////public ActiveRecord(System.Data.Entity.DbContext ctx, source, EntityType sourceType, object RecordParams) {
        //// ctor:
        //public ActiveRecord(HrAdminDbContext ctx, string src, EntityType stype, object rp) {
        //    Context = ctx;
        //    Source = src;
        //    SourceType = stype;
        //    RecordParams = rp;
        //}

        public T Get<T>()
        {
            switch (SourceType)
            {
                case EntityType.Proc:
                    {
                        var mi = typeof(Models.HrAdminDbContext).GetMethod("GetSingleByProc");
                        //var mytype = this.GetType();
                        //var getOne = mi.MakeGenericMethod(mytype);
                        var getOne = mi.MakeGenericMethod(typeof (T));
                        object[] args = new object[2];
                        args[0] = Source;
                        args[1] = RecordParams;
                        var result = getOne.Invoke(Context, args);
                        //return result;
                        return (T)Convert.ChangeType(result, typeof(T));
                    }
                default:
                    {
                        return default(T);
                        // can't return null on generic method...;
                    }
            }
        }

        public List<T> GetAll<T>(string procName, object rp)
        {
            // assuming Proc for now...later may add FK-joins / EF automation / etc.
            var mi = typeof(Models.HrAdminDbContext).GetMethod("GetByProc");
//            var mytype = this.GetType();
            var getAll = mi.MakeGenericMethod(typeof(T));
            //var procParams = new { Id = Id };
            object[] args = new object[2];
            args[0] = procName;
            args[1] = rp;
            var result = getAll.Invoke(this.Context, args);
            //return result;
            return (List<T>)Convert.ChangeType(result, typeof(List<T>));
        }
    }
}
