﻿using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using NLog;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Helpers;

namespace HrAdmin2.Controllers.Api
{
    //public class ApprovalWorkflowController : ApiController
    public class ApprovalWorkflowController : BaseApiController
    {
        // these were moved to BaseApiController:
        //private static Logger logger = LogManager.GetCurrentClassLogger();
        //private HrAdminDbContext db = new HrAdminDbContext();

        // GET api/ApprovalWorkflow/5
        //TESTING
        public ApprovalWorkflowVm Get(int id)
        {
            var awvm = new ApprovalWorkflowVm(id);
            awvm.SetAuthorization(ui.LanId);
            return awvm;
        }

        /// <summary>
        /// PUT api/ApprovalWorkflow/5
        /// Used to update status via hr/admin override
        /// Should have values for HrOverrideComment, HrOverridePersonId, and a new status
        /// </summary>
        /// <param name="updateWorkflow">Values to update of workflow</param>
        public HttpResponseMessage Put(ApprovalWorkflow updateWorkflow)
        {
            try
            {
                if (ui.RoleName != "HR" && ui.RoleName != "Admin")
                {
                    return new HttpResponseMessage()
                    {
                        Content = new StringContent(Json.Encode(
                            new
                            {
                                error = "Unauthorized."
                            })),
                        StatusCode = HttpStatusCode.InternalServerError
                    };
                }
                //if (!String.IsNullOrEmpty(updateWorkflow.HrOverridePersonLanId))
                //{
                    //lanId set, assign id
                    //Employee e = db.GetByProc<Employee>("GetEmployeeIdOfLanId",
//                        parameters: new { lanId = updateWorkflow.HrOverridePersonLanId }).First();
                    Employee e = db.GetSingleByProc<Employee>("GetEmployeeIdOfLanId",
                        parameters: new { lanId = ui.LanId });
                    if (e != null) updateWorkflow.HrOverridePersonId = e.Id;
//                }
                //should have override person Id, otherwise updates handled via tasks.
                //if (updateWorkflow.HrOverridePersonId <= 0)
                //    return new HttpResponseMessage()
                //    {
                //        Content = new StringContent(Json.Encode(
                //            new
                //            {
                //                error = "Missing Override Information"
                //            })),
                //        StatusCode = HttpStatusCode.InternalServerError
                //    };
                //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                
                if (updateWorkflow.HrOverrideComment == null) updateWorkflow.HrOverrideComment = "";

                if (!CheckStatusOfWorkflowUpdate(updateWorkflow))
                {
                    //if false then need to save here
                    //return new HttpResponseMessage(HttpStatusCode.BadRequest);
                    return new HttpResponseMessage() {
                        Content = new StringContent(Json.Encode(
                            new {
                                error = "Invalid Status Change"
                            })),
                        StatusCode = HttpStatusCode.InternalServerError
                    };
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            catch (Exception xcp)
            {
                var lei = new LogEventInfo(LogLevel.Error, logger.Name, "error updating approval workflow.");
                //lei.SetStackTrace(new System.Diagnostics.StackTrace(xcp), 0);
                //lei.Properties.Add("ErrorSource", xcp.Source);
                lei.Properties.Add("ErrorSource", logger.Name);
                lei.Properties.Add("ErrorClass", xcp.GetType().ToString());
                lei.Properties.Add("ErrorMethod", xcp.TargetSite.Name);
                lei.Properties.Add("ErrorMessage", xcp.Message);
                lei.Properties.Add("InnerErrorMessage", xcp.InnerException.Message);
                lei.Properties.Add("StackTrace", xcp.StackTrace.ToString());
                logger.Log(lei);
                var hrm = new HttpResponseMessage()
                {
                    //Content = new StringContent(Json.Encode(
                    //    new
                    //    {
                    //        error = xcp.Message
                    //    })),
                    Content = new StringContent(lei.Message),
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return hrm;
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        #region Unused Calls

        //        // GET api/ApprovalWorkflow 
        //        public IEnumerable<ApprovalWorkflow> Get()
        //        {
        //            db.Configuration.ProxyCreationEnabled = false;
        //            var approvalWorkflows = db.ApprovalWorkflows.Include(a => a.App).Include(a => a.ApprovalTasks);
        //            return approvalWorkflows.AsEnumerable();
        //        }


        //NOT NEEDED, update via tasks      
        //public HttpResponseMessage Put(int id, ApprovalWorkflow approvalWorkflow);

        // GET api/ApprovalWorkflow/5
        //public ApprovalWorkflow Get(int id)
        //{
        //    try
        //    {                
        //        //get workflow data
        //        ApprovalWorkflow workflow = db.GetSingleByProc<ApprovalWorkflow>("GetApprovalWorkflow",
        //            parameters: new { Id = id });

        //        //get task list
        //        workflow.ApprovalTasks = db.GetByProc<ApprovalTask>("GetApprovalWorkflowTasks",
        //            parameters: new { WorkflowId = id });

        //        return workflow;

        //    }
        //    catch (Exception e)
        //    {
        //        throw new HttpResponseException(Request.CreateResponse(HttpStatusCode.NotFound));
        //    }
        //}


        //        //NOT NEEDED, create via new PDF using repository.
        //        /// <summary>
        //        /// POST api/ApprovalWorkflow
        //        /// Save workflow to db and create tasks for workflow.
        //        /// Workflow should have appId assigned and either PositionId or PersonId.
        //        /// </summary>
        //        /// <param name="appWorkflow">ApprovalWorkflow with appId assigned and either PositionId or PersonId</param>
        //        /// <returns></returns>
        //        //public int Post(ApprovalWorkflow appWorkflow)
        //        //{
        //        //    int newId = -1;

        //        //    try
        //        //    {
        //        //        newId = db.CreateStandardParApprovalWorkflow(appWorkflow.AppId, appWorkflow.PositionId, appWorkflow.PersonId);
        //        //    }
        //        //    catch
        //        //    {
        //        //        //TODO: how to use Request.CreateResponse to include response header as well?
        //        //    }

        //        //    return newId;
        //        //}


        //        //[HttpDelete]
        //        /// <summary>
        //        /// DELETE api/ApprovalWorkflow/5
        //        /// sets status to canceled for workflow and all tasks.
        //        /// </summary>
        //        /// <param name="id">Id of workflow to cancel</param>
        //        /// <returns></returns>
        //        public HttpResponseMessage Delete(int id)
        //        {
        //            try
        //            {
        //                ApprovalWorkflow approvalWorkflow = db.ApprovalWorkflows.Find(id);
        //                if (approvalWorkflow == null)
        //                {
        //                    return new HttpResponseMessage(HttpStatusCode.NotFound);
        //                }

        //                foreach (var task in approvalWorkflow.ApprovalTasks)
        //                {
        //                    //set active and created tasks to canceled
        //                    if (task.StatusCodeId != StatusCodeValue.Approved || task.StatusCodeId != StatusCodeValue.Rejected)
        //                    {
        //                        task.StatusCodeId = StatusCodeValue.Canceled;
        //                        db.Entry(task).State = EntityState.Modified;
        //                    }
        //                }

        //                approvalWorkflow.StatusCodeId = StatusCodeValue.Canceled;
        //                db.Entry(approvalWorkflow).State = EntityState.Modified;
        //                db.SaveChanges();
        //            }
        //            catch (DbUpdateConcurrencyException)
        //            {
        //                throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.NotFound));
        //                //return Request.CreateResponse(HttpStatusCode.Conflict, new { Success = false });
        //            }
        //            catch (Exception)
        //            {
        //                //throw new HttpResponseException(new HttpResponseMessage(HttpStatusCode.BadRequest));
        //                return new HttpResponseMessage(HttpStatusCode.BadRequest);
        //            }
        //            return new HttpResponseMessage(HttpStatusCode.OK);
        //        }


        #endregion



        #region Approval Workflow Repository

//        /// <summary>
//        /// Get all approvals related to given position
//        /// </summary>
//        /// <param name="positionId">Id of position to use as filter</param>
//        /// <returns>List of filtered approvals</returns>
//        private IEnumerable<ApprovalWorkflow> GetApprovalsOfPosition(int positionId)
//        {

//            var approvalResult = this.Get<ApprovalWorkflow>(s => s.PositionId == positionId);

//            return approvalResult.ToList<ApprovalWorkflow>();
//        }

//        /// <summary>
//        /// Get all approvals related to given person.
//        /// </summary>
//        /// <param name="personId">Id of person to use as filter</param>
//        /// <returns>List of filtered approvals</returns>
//        private List<ApprovalWorkflow> GetApprovalsOfPerson(int personId)
//        {
//            var approvalResult = this.Get<ApprovalWorkflow>(s => s.PersonId == personId);

//            return approvalResult.ToList<ApprovalWorkflow>();
//        }

//        /// <summary>
//        /// Get all approvals related to given app
//        /// </summary>
//        /// <param name="appId">Id of app to use as filter</param>
//        /// <returns>List of filtered approvals</returns>
//        private List<ApprovalWorkflow> GetAppsApprovals(int appId)
//        {
//            var approvalResult = this.Get<ApprovalWorkflow>(s => s.AppId == appId);

//            return approvalResult.ToList<ApprovalWorkflow>();
//        }

//        private List<ApproveGroup> GetApproveGroups()
//        {
//            var results = this.Get<ApproveGroup>();

//            return results.ToList<ApproveGroup>();
//        }

//        /// <summary>
//        /// Based off GetUnExpiredDelegates in DelegatesRepository
//        /// </summary>
//        /// <param name="personId"></param>
//        /// <returns></returns>
//        private List<ApprovalTaskDto> GetWorkflowsTasks(int workflowId)
//        {

//            List<ApprovalTaskDto> dto = this.GetDtoByProc<ApprovalTaskDto>("Approval.GetApprovalWorkflowsTasks",
//                                                    new { Id = workflowId }).ToList();

//            return dto;

//        }


        //default startChainWithPostion is false
        public int CreateApprovalWorkflow(ApprovalWorkflow.approvalApp app, int? positionId, int? personId)
        {
            return CreateApprovalWorkflow(app, positionId, personId, false);
        }

        /// <summary>
        /// Return created workflow's id
        /// </summary>
        /// <param name="app"></param>
        /// <param name="positionId"></param>
        /// <param name="personId"></param>
        /// <returns>created workflow's id, -1 if error</returns>
        public int CreateApprovalWorkflow(ApprovalWorkflow.approvalApp app, int? positionId, int? personId, bool startChainWithPosition)
        {
            int id = -1;
            
            //error check
            if ((!positionId.HasValue || positionId <= 0) && (!personId.HasValue || personId <= 0))
            {
                //error TODO: add nlog
                return id;
            }
            
            try
            {
                //positionId = positionId.HasValue ? positionId.Value : 0;
                //personId = personId.HasValue ? personId.Value : 0;
                ApprovalWorkflow aw = new ApprovalWorkflow() { App = app, PositionId = positionId, PersonId = personId };

                if (app == ApprovalWorkflow.approvalApp.PDF)//PDF
                {
                    //pdf
                    aw.ApprovalCode = ApprovalWorkflow.approvalCode.AppointingWithReview; //AppointingWithReview
                }
                else
                {
                    aw.ApprovalCode = ApprovalWorkflow.approvalCode.AppointingAuthority; //appointing authority type for PAR
                }

                aw.CreatedDate = DateTime.Now;
                aw.StatusCode = ApprovalWorkflow.statusCode.Created;

                //create parameter list
                dynamic parameters = new { };

                //using if else because cannot pass anon parameter with value of null
                if (positionId.HasValue && personId.HasValue)
                {
                    parameters = new
                    {
                        StatusCodeId = (int)aw.StatusCode,
                        ApprovalCodeId = (int)aw.ApprovalCode,
                        PositionId = aw.PositionId,
                        PersonId = aw.PersonId,
                        AppId = (int)aw.App,
                        CreatedDate = aw.CreatedDate
                    };
                }
                else if (positionId.HasValue) //null personId
                {
                    parameters = new
                    {
                        StatusCodeId = (int)aw.StatusCode,
                        ApprovalCodeId = (int)aw.ApprovalCode,
                        PositionId = aw.PositionId,
                        AppId = (int)aw.App,
                        CreatedDate = aw.CreatedDate
                    };

                }
                else if(personId.HasValue) //null positionId
                {
                    parameters = new
                    {
                        StatusCodeId = (int)aw.StatusCode,
                        ApprovalCodeId = (int)aw.ApprovalCode,
                        PersonId = aw.PersonId,
                        AppId = (int)aw.App,
                        CreatedDate = aw.CreatedDate
                    };
                }

                //save to DB to create Id
                aw.Id = db.GetSingleByProc<ApprovalWorkflow>("CreateApprovalWorkflow", parameters).Id; //get Id value returned from stored procedure
                id = aw.Id;
                
                //create task list and return
                CreateApprovalChainTaskList(aw, startChainWithPosition);
                
            }
            catch (Exception xcp)
            {
                return -1;
                //return new HttpResponseMessage()
                //{
                //    StatusCode = HttpStatusCode.InternalServerError,
                //    ReasonPhrase = "Create Failed: " + xcp.Message
                //};
            }

            return id;
        }


        /// <summary>
        /// Create a standard task list based on PAR requirements.
        /// Saves task list to Workflow property.
        /// </summary>
        private List<ApprovalTask> CreateApprovalChainTaskList(ApprovalWorkflow aw, bool startChainWithPosition)
        {

            //get position Id
            int basePositionId = aw.PositionId != null ? aw.PositionId.Value : 0;

            if (basePositionId == 0)
            {
                //Get position Id based on personId
                //var lookupResult = this.Get<OrganizationalAssignment>(s => s.PersonId == aw.PersonId);
                //basePositionId = lookupResult.First<OrganizationalAssignment>().PositionId;

                //TEST TEST TEST Perhaps add to earlier step?
                basePositionId = (int)db.GetSingleByProc<ApprovalWorkflow>("GetPositionIdOfEmployee",
                    new { employeeId = aw.PersonId }).PositionId; //get Id value returned from stored procedure
            }

            //use position id to get task list. 
            //List starts with position number at top of org chart and works down to starting position
            //List<ApprovalTask> chain = this.GetChainOfCommand(basePositionId);
            //List<ChainLink> chain = db.GetByProc<ChainLink>("dbo.GetChainOfCommand", new { Id = basePositionId });
            List<ChainLink> chain = new List<ChainLink>();
            try
            {
                chain = db.GetByProc<ChainLink>("dbo.GetChainOfCommand", new { Id = basePositionId });
            }
            catch (Exception xcp)
            {
                logger.Error(xcp.Message);
            }
            //chain is in reverse order
            chain.Reverse();

            List<ApprovalTask> tasks = new List<ApprovalTask>();

            if (aw.ApprovalCode == ApprovalWorkflow.approvalCode.AppointingAuthority)
            {
                //appointing authority for PAR
                tasks = createTasks(chain, aw.Id, startChainWithPosition);
            }
            else if (aw.ApprovalCode == ApprovalWorkflow.approvalCode.AppointingWithReview)
            {
                //appointing authority with reviews and acknowledgement for Pdf
                tasks = createTasksWithReview(chain, aw.Id);
            }
            else
            {
                //default is appointing authority for PAR
                tasks = createTasks(chain, aw.Id, startChainWithPosition);
            }

            return tasks;

        }

        /// <summary>
        /// Create list of tasks based off supervisor chain
        /// </summary>
        /// <param name="positionIds">List with position Ids of supervisor chain</param>
        /// <param name="workflowId">workflow tasks belong to</param>
        /// <returns></returns>
        private List<ApprovalTask> createTasks(List<ChainLink> positionIds, int workflowId, bool startChainWithPosition)
        {

            List<ApprovalTask> tasks = new List<ApprovalTask>();

            //create list of correctly ordered positionIds
            //List<int> positionIds = new List<int>();
            if (positionIds.Count != 1 && startChainWithPosition != true)
            {
                //remove first member of chain which is requestor. 
                positionIds.RemoveAt(0);
            }
            else
            {
                //special case for director
            }


            //default status, id of 5 is "Created"
            int order = 1;

            //create each task, and write to database
            foreach (ChainLink approvalPostion in positionIds)
            {
                ApprovalTask newTask = new ApprovalTask()
                {
                    ApproverPositionId = approvalPostion.Id,
                    StatusCode = ApprovalWorkflow.statusCode.Created,
                    Comment = String.Empty,
                    TaskCode = ApprovalTask.taskCode.Standard,
                    WorkflowOrder = order++
                };
                //save to DB
                newTask.Id = CreateApprovalTask(newTask, workflowId);
                tasks.Add(newTask);
            }

            return tasks;
        }



        private List<ApprovalTask> createTasksWithReview(List<ChainLink> positionIds, int workflowId)
        {

            List<ApprovalTask> tasks = new List<ApprovalTask>();

            //save employee Id for acknowledge task, and then remove from list so approval chain can be created
            int employeeId = positionIds[0].Id;
            positionIds.RemoveAt(0);

            //default status, id of 5 is "Created"
            int order = 1;


            //Initial HR Review
            ApprovalTask hrReview = new ApprovalTask()
            {
                ApproveGroupId = 1,//hr
                StatusCode = ApprovalWorkflow.statusCode.Created,
                Comment = String.Empty,
                TaskCode = ApprovalTask.taskCode.Standard,
                WorkflowOrder = order++
            };
            hrReview.Id = CreateApprovalTask(hrReview, workflowId);
            tasks.Add(hrReview);

            //create each task, and write to database
            foreach (ChainLink approvalPostion in positionIds)
            {
                ApprovalTask newTask = new ApprovalTask()
                {
                    ApproverPositionId = approvalPostion.Id,
                    StatusCode = ApprovalWorkflow.statusCode.Created,
                    Comment = String.Empty,
                    TaskCode = ApprovalTask.taskCode.Standard,
                    WorkflowOrder = order++
                };
                newTask.Id = CreateApprovalTask(newTask, workflowId);
                tasks.Add(newTask);
            }


            //Employee Acknoledgement 
            ApprovalTask employeeAcknowledge = new ApprovalTask()
            {
                ApproverPositionId = employeeId,//employee workflow is for
                StatusCode = ApprovalWorkflow.statusCode.Created,
                Comment = String.Empty,
                TaskCode = ApprovalTask.taskCode.MoveFwdRegardless,
                WorkflowOrder = order++
            };
            employeeAcknowledge.Id = CreateApprovalTask(employeeAcknowledge, workflowId);
            tasks.Add(employeeAcknowledge);


            //HR Sign-off
            ApprovalTask hrSignOff = new ApprovalTask()
            {
                ApproveGroupId = 1,//hr
                StatusCode = ApprovalWorkflow.statusCode.Created,
                Comment = String.Empty,
                TaskCode = ApprovalTask.taskCode.SignOff,
                WorkflowOrder = order++
            };
            hrSignOff.Id = CreateApprovalTask(hrSignOff, workflowId);
            tasks.Add(hrSignOff);

            return tasks;
        }


        /// <summary>
        /// Save task to DB with stored procedure
        /// </summary>
        /// <param name="task">new Task to save to DB</param>
        /// <param name="workflowId">Id of workflow that task belongs to</param>
        /// <returns>int ID</returns>
        private int CreateApprovalTask(ApprovalTask task, int workflowId)
        {
            int id = -1;

            dynamic parameters = new { };

            //using if else because cannot pass anon parameter with value of null
            //task either assigned to specific position or to a group
            if (task.ApproverPositionId.HasValue)
            {
                parameters = new
                {
                    ApprovalWorkflowId = workflowId,
                    StatusCodeId = (int)task.StatusCode,
                    TaskCodeId = (int)task.TaskCode,
                    PositionId = task.ApproverPositionId,
                    Comment = task.Comment,
                    WorkflowOrder = task.WorkflowOrder
                };
            }
            else //group
            {
                parameters = new
                {
                    ApprovalWorkflowId = workflowId,
                    StatusCodeId = (int)task.StatusCode,
                    TaskCodeId = (int)task.TaskCode,
                    ApproveGroupId = task.ApproveGroupId,
                    Comment = task.Comment,
                    WorkflowOrder = task.WorkflowOrder
                };

            }

            try
            {
                //save to DB to create Id
                id = db.GetSingleByProc<ApprovalTask>("CreateApprovalTask", parameters).Id;
                //get Id value returned from stored procedure

            }
            catch { };

            return id;
        }


        private bool CheckStatusOfWorkflowUpdate(ApprovalWorkflow workflow)
        {

            //get workflow data
            ApprovalWorkflow aw = db.GetSingleByProc<ApprovalWorkflow>("GetApprovalWorkflow",
                parameters: new { Id = workflow.Id });

            //check to see if status has changed, return if hasn't
            if (aw.StatusCode == workflow.StatusCode) return false;

            //no current logic for changing to active (done via initial workflow start in task controller) 
            //or created (done only at creation)
            if (workflow.StatusCode == ApprovalWorkflow.statusCode.Active
                || workflow.StatusCode == ApprovalWorkflow.statusCode.Created) return false;

            //get task list
            aw.ApprovalTasks = db.GetByProc<ApprovalTask>("GetApprovalWorkflowTasks",
                parameters: new { WorkflowId = aw.Id });

            //cleanup tasks, set created and active to canceled. 
            foreach (var task in aw.ApprovalTasks)
            {
                if (task.StatusCode == ApprovalWorkflow.statusCode.Created || task.StatusCode == ApprovalWorkflow.statusCode.Active)
                {
                    task.StatusCode = ApprovalWorkflow.statusCode.Canceled;
                    db.Exec("SaveApprovalTask", new
                    {
                        Id = task.Id,
                        StatusCodeId = (int)task.StatusCode,
                        TaskCodeId = (int)task.TaskCode,
                        PositionId = task.ApproverPositionId.HasValue ? task.ApproverPositionId : 0,
                        ApproveGroupId = task.ApproveGroupId.HasValue ? task.ApproveGroupId : 0,
                        Comment = task.Comment,
                        WorkflowOrder = task.WorkflowOrder
                    });
                };
            }

            //save workflow's new status
            aw.StatusCode = workflow.StatusCode;
            db.Exec("SaveApprovalWorkflow", 
                new {
                    Id = aw.Id,
                    StatusCodeId = (int)aw.StatusCode,
                    ApprovalCodeId = (int)aw.ApprovalCode,
                    PositionId = aw.PositionId,
                    PersonId = aw.PersonId,
                    AppId = (int)aw.App
                });

            //create new task
            int newTaskId = (db.GetSingleByProc<ApprovalTask>("CreateApprovalTask", 
                new {
                    ApprovalWorkflowId = aw.Id,
                    StatusCodeId = (int)aw.StatusCode,
                    TaskCodeId = (int)ApprovalTask.taskCode.Standard,
                    ApproveGroupId = 1, //HR
                    Comment = workflow.HrOverrideComment,
                    WorkflowOrder = aw.ApprovalTasks.Count + 1
                })).Id;

            //save final approver to task
            bool result = db.Exec("SaveApprovalTask",
                new {
                    Id = newTaskId,
                    StatusCodeId = (int)aw.StatusCode,
                    TaskCodeId = (int)ApprovalTask.taskCode.Standard,
                    ApproveGroupId = 1, //HR
                    Comment = workflow.HrOverrideComment,
                    WorkflowOrder = aw.ApprovalTasks.Count + 1,
                    FinalApproverPersonId = workflow.HrOverridePersonId,
                    CompletedDate = DateTime.Now
                });

            return result;
        }

        #endregion
    }
}
