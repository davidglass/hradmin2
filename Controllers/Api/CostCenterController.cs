﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HrAdmin2.Models;
using System.Web.Helpers;

namespace HrAdmin2.Controllers.Api
{
    public class CostCenterController : BaseApiController
    {
        public HttpResponseMessage Post(CostCenter cc)
        {
            try
            {
                db.CostCenters.Add(cc);
                int saved = db.SaveChanges();
                hrm.Content = new StringContent(Json.Encode(cc));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                        new
                        {
                            error = uxcp.InnerException.GetType().ToString() + ":" + uxcp.Message
                        }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.InnerException.GetType().ToString() + ":" + xcp.InnerException.InnerException.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Delete(int id)
        {
            try {
                db.Exec("DeleteCostCenter", new { Id = id });
                hrm.StatusCode = HttpStatusCode.OK;
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }

        public HttpResponseMessage Put(CostCenter cc)
        {
            try
            {
                db.CostCenters.Attach(cc);
                var e = db.Entry<CostCenter>(cc);
                // currently this is the only editable property...
                e.Property("IsActive").IsModified = true;
                var updated = db.SaveChanges();
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }
    }
}