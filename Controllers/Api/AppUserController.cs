﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HrAdmin2.Models;
using System.Web.Helpers;

namespace HrAdmin2.Controllers.Api
{
    public class AppUserController : BaseApiController
    {
        public HttpResponseMessage Put(AppUser au)
        {
            try
            {
                db.AppUsers.Attach(au);
                var e = db.Entry<AppUser>(au);
                // currently this is the only editable property...
                e.Property("RoleId").IsModified = true;
                var updated = db.SaveChanges();
            }
            catch (Exception xcp)
            {
                hrm.Content = new StringContent(Json.Encode(
                    new
                    {
                        error = xcp.Message
                    }));
                hrm.StatusCode = HttpStatusCode.InternalServerError;
            }
            return hrm;
        }
    }
}
