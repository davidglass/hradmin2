﻿using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using NLog;
using RazorEngine;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;

namespace HrAdmin2.Controllers.Api
{
//    public class ApprovalTaskController : ApiController
    public class ApprovalTaskController : BaseApiController
    {
        // TODO: add Logger and DbContext to BaseApiController, extend that.
        // private static Logger logger = LogManager.GetCurrentClassLogger();
        // db is in BaseApiController...
        //private HrAdminDbContext db = new HrAdminDbContext();

        #region Unused Calls

        // GET api/ApprovalTask
        //public IEnumerable<ApprovalTaskDto> Get();

        //// GET: api/ApprovalTask/id
        //// Gets all tasks for given workflow
        //public IEnumerable<ApprovalTaskDto> GetDetail(int approvalWorkflowId);

        //// POST api/ApprovalTask
        //public int Post(ApprovalTaskDto task) ;
        // just using Model for now, not a DTO since not adding or removing model properties.

        //delete by seting status to Cancel on Put
        //DELETE api/ApprovalTask/5
        //public HttpResponseMessage Delete(int id);

        #endregion

        /// <summary>
        /// PUT api/ApprovalTask/5
        /// If updateing status with accept or reject then need to include FinalApproverPersonId
        /// </summary>
        /// <param name="updateTask">Values to update of task</param>
        public HttpResponseMessage Put(ApprovalTask updateTask)
        {
            updateTask.FinalApproverPersonLanId = ui.LanId;
            try
            {

                //temp bug fix
                if (updateTask.Comment == null) updateTask.Comment = "";

                if (!String.IsNullOrEmpty(updateTask.FinalApproverPersonLanId))
                {
                    //lanId set, assign id
                    Employee e = db.GetByProc<Employee>("GetEmployeeIdOfLanId",
                        parameters: new { lanId = updateTask.FinalApproverPersonLanId }).First();
                    if (e != null) updateTask.FinalApproverPersonId = e.Id;
                }

                if (!CheckStatusOfTaskUpdate(updateTask))
                {
                    //if false then need to save here
                    SaveTask(updateTask);
                }
            }
            catch (DbUpdateConcurrencyException)
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }
            catch (Exception xcp)
            {
                var lei = new LogEventInfo(LogLevel.Error, logger.Name, "error updating approval task.");
                //lei.SetStackTrace(new System.Diagnostics.StackTrace(xcp), 0);
                //lei.Properties.Add("ErrorSource", xcp.Source);
                lei.Properties.Add("ErrorSource", logger.Name);
                lei.Properties.Add("ErrorClass", xcp.GetType().ToString());
                lei.Properties.Add("ErrorMethod", xcp.TargetSite.Name);
                lei.Properties.Add("ErrorMessage", xcp.Message);
                if (xcp.InnerException != null)
                {
                    lei.Properties.Add("InnerErrorMessage", xcp.InnerException.Message);
                }
                lei.Properties.Add("StackTrace", xcp.StackTrace.ToString());
                logger.Log(lei);
                var hrm = new HttpResponseMessage()
                {
                    //Content = new StringContent(Json.Encode(
                    //    new
                    //    {
                    //        error = xcp.Message
                    //    })),
                    Content = new StringContent(lei.Message),
                    StatusCode = HttpStatusCode.InternalServerError
                };
                return hrm;
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }



        #region repository


        /// <summary>
        /// Checks to see if status of task is updated, and if so updates the workflow according to buisness rules.
        /// </summary>
        /// <param name="task">task that is receiving update</param>
        /// <returns>True if status changed, false if same</returns>
        private bool CheckStatusOfTaskUpdate(ApprovalTask task)
        {

            //get workflow data
            ApprovalWorkflow aw = db.GetSingleByProc<ApprovalWorkflow>("GetApprovalWorkflowOfTask",
                parameters: new { TaskId = task.Id });

            //get task list
            aw.ApprovalTasks = db.GetByProc<ApprovalTask>("GetApprovalWorkflowTasks",
                parameters: new { WorkflowId = aw.Id });
            
            
            //List<Object> updatedEntities = new List<object>();


            //database version of task
            ApprovalWorkflow.statusCode statusDb = aw.ApprovalTasks.Where(s => s.Id == task.Id).Select(s => s.StatusCode).FirstOrDefault();
            //ApprovalTask dbTask = this.ApprovalTasks.Where(s => s.Id == task.Id).First();

            //check to see if status has changed, return if hasn't
            if (statusDb == task.StatusCode) return false;

            //if task status was changed then update workflow
            if (task.StatusCode == ApprovalWorkflow.statusCode.Approved)
            {
                //Assume final approver is assigned
                //if no final approver assigned, assume assigned position completed task
                //if (task.FinalApproverPersonId == null && task.ApproverPositionId != null)
                //    task.FinalApproverPersonId = this.OrganizationalAssignments.Where(s => s.PositionId == task.ApproverPositionId).Select(s => s.PersonId).FirstOrDefault();

                task.CompletedDate = DateTime.Now;

                //check to see if PDF (Appointing With Review)
                if (aw.ApprovalCode == ApprovalWorkflow.approvalCode.AppointingWithReview)
                {
                    TaskApproveForAppointingWithReview(aw, task);
                    SaveTask(task);
                    return true;
                }

                //PAR Standard approval
                Position approverPosition = db.GetSingleByProc<Position>("GetPosition", parameters: new { Id = task.ApproverPositionId });

                if (approverPosition.IsAppointingAuthority == true)
                {
                    aw.StatusCode = ApprovalWorkflow.statusCode.Approved;
                    //Cancel any other active or created tasks in workflow
                    List<ApprovalTask> activeTasks = aw.ApprovalTasks.ToList().FindAll(
                        s => s.StatusCode == ApprovalWorkflow.statusCode.Active || s.StatusCode == ApprovalWorkflow.statusCode.Created);
                    foreach (var t in activeTasks)
                    {
                        //ignore task that is being updated
                        if (t.Id != task.Id)
                        {
                            t.StatusCode = ApprovalWorkflow.statusCode.Canceled;
                            SaveTask(t);
                        }
                    }
                    SaveTask(task);
                    SaveWorkflow(aw);

                    //send email to HR to notify them ready to process
                    SendHrProccessEmailDb(task);

                    return true;
                }

                //non-appointing authority approval should move workflow forward
                AdvanceWorkflow(aw, task.WorkflowOrder);
                SaveTask(task);
                SaveWorkflow(aw);
                return true;
            }
            else if (task.StatusCode == ApprovalWorkflow.statusCode.Rejected)
            {
                //Assume final approver is assigned
                //if no final approver assigned, assume assigned position completed task
                //if (task.FinalApproverPersonId == null && task.ApproverPositionId != null) 
                //    task.FinalApproverPersonId = this.OrganizationalAssignments.Where(s => s.PositionId == task.ApproverPositionId).Select(s => s.PersonId).FirstOrDefault();
                
                task.CompletedDate = DateTime.Now;

                //special situation for acknowledgemtn tasks to move forward regardless. 
                if (task.TaskCode == ApprovalTask.taskCode.MoveFwdRegardless)
                {
                    if (statusDb != ApprovalWorkflow.statusCode.Active)
                    {
                        SaveTask(task);
                        return true;
                    }

                    //if task was active then move workflow forward.
                    AdvanceWorkflow(aw, task.WorkflowOrder);
                    SaveTask(task);
                    return true;
                }

                //set workflow to rejected.
                aw.StatusCode = ApprovalWorkflow.statusCode.Rejected;
                
                //Cancel any other active or created tasks in workflow
                List<ApprovalTask> activeTasks = aw.ApprovalTasks.ToList().FindAll(
                    s => s.StatusCode == ApprovalWorkflow.statusCode.Active || s.StatusCode == ApprovalWorkflow.statusCode.Created);
                foreach (var t in activeTasks)
                {
                    //ignore task that is being updated
                    if (t.Id != task.Id)
                    {
                        t.StatusCode = ApprovalWorkflow.statusCode.Canceled;
                        SaveTask(t);
                    }
                }
                SaveTask(task);
                SaveWorkflow(aw);
                return true;

            }
            else if (task.StatusCode == ApprovalWorkflow.statusCode.Canceled)
            {
                //if final approver assigned then give it a completed date
                if (task.FinalApproverPersonId.HasValue && task.FinalApproverPersonId != 0)
                    task.CompletedDate = DateTime.Now;

                if (statusDb != ApprovalWorkflow.statusCode.Active)
                {
                    SaveTask(task);
                    return true;
                }

                //if task was active then move workflow forward.
                AdvanceWorkflow(aw, task.WorkflowOrder);
                SaveTask(task);
                return true;
            }
            else if (task.StatusCode == ApprovalWorkflow.statusCode.Active)
            {
                //Making active. Should email, but nothing else needed.
                //flag workflow as active status
                aw.StatusCode = ApprovalWorkflow.statusCode.Active;

                //case when workflow is first submitted, make sure task being made active is not assigned a final approver
                task.FinalApproverPersonId = null;

                SaveWorkflow(aw);
                EmailNotice(task);
            }

            //if final approver assigned then give it a completed date
            if (task.FinalApproverPersonId.HasValue && task.FinalApproverPersonId != 0)
                task.CompletedDate = DateTime.Now;

            SaveTask(task);
            return true;
        }


        /// <summary>
        /// Used to deal with approving a task in PDF workflow
        /// </summary>
        /// <param name="aw"></param>
        /// <param name="task"></param>
        /// <returns></returns>
        private void TaskApproveForAppointingWithReview(ApprovalWorkflow aw, ApprovalTask task)
        {

            //check to see if at HR sign off stage
            if (task.TaskCode == ApprovalTask.taskCode.SignOff)
            {
                //approve workflow
                aw.StatusCode = ApprovalWorkflow.statusCode.Approved;

                //Cancel any other active tasks in workflow
                List<ApprovalTask> activeTasks = aw.ApprovalTasks.ToList().FindAll(s => s.StatusCode == ApprovalWorkflow.statusCode.Active);
                foreach (var t in activeTasks)
                {
                    //ignore task that is being updated
                    if (t.Id != task.Id)
                    {
                        t.StatusCode = ApprovalWorkflow.statusCode.Canceled;
                        SaveTask(t);
                    }
                }
                SaveWorkflow(aw);
                return;
            }

            //standard changes assigned to HR Group or given move fwd regardless status should move chain forward.            
            if (task.ApproveGroupId == 1 || task.TaskCode == ApprovalTask.taskCode.MoveFwdRegardless)
            {
                AdvanceWorkflow(aw, task.WorkflowOrder);
                return;
            }

            //check to see if appointing authority. 
            //If so move past all other standard approvals and go to employee acknowledgement 
            Position approverPosition = db.GetSingleByProc<Position>("GetPosition", parameters: new { Id = task.ApproverPositionId });

            if (approverPosition.IsAppointingAuthority == true)
            {
                //Cancel any other standard tasks in workflow
                List<ApprovalTask> activeTasks = aw.ApprovalTasks.ToList().FindAll(s => s.TaskCode == ApprovalTask.taskCode.Standard);
                foreach (var t in activeTasks)
                {
                    //ignore task that is being updated
                    if (t.Id != task.Id)
                    {
                        //if task is active or created then cancel it.
                        if (t.StatusCode == ApprovalWorkflow.statusCode.Created || t.StatusCode == ApprovalWorkflow.statusCode.Active)
                        {
                            t.StatusCode = ApprovalWorkflow.statusCode.Canceled;
                            SaveTask(t);
                        }
                    }
                }

                //make employee acknowledgement active. Should only be on task with move fwd regardless type
                ApprovalTask acknowledgeTask = aw.ApprovalTasks.ToList().FindAll(s => s.TaskCode == ApprovalTask.taskCode.MoveFwdRegardless).Last();
                acknowledgeTask.StatusCode = ApprovalWorkflow.statusCode.Active;
                SaveTask(acknowledgeTask);
                EmailNotice(acknowledgeTask);

                return;
            }

            //non-appointing authority approval should move workflow forward
            AdvanceWorkflow(aw, task.WorkflowOrder);
            return;
        }



        //advance the workflow
        private void AdvanceWorkflow(ApprovalWorkflow aw, int? currentOrder)
        {
            //should have current order
            if (!currentOrder.HasValue || currentOrder == 0)
                return;

            //go through task list. Find next in order with value of created and make it active         
            for (int i = (int)currentOrder + 1; i <= aw.ApprovalTasks.Count; i++)
            {
                var nextTask = aw.ApprovalTasks.Find(s => s.WorkflowOrder == i);
                if (nextTask.StatusCode == ApprovalWorkflow.statusCode.Created)
                {
                    //If task has no one assigned to it (when assigned to vacant position without delegate)
                    //then cancel task and move on.
                    if(db.GetByProc<Employee>("GetApproversOfTask", parameters: new { TaskId = nextTask.Id }).Count == 0)
                    {
                        nextTask.StatusCode = ApprovalWorkflow.statusCode.Canceled;
                        SaveTask(nextTask);
                        AdvanceWorkflow(aw, i);
                        return;
                    }

                    nextTask.StatusCode = ApprovalWorkflow.statusCode.Active;
                    SaveTask(nextTask);
                    EmailNotice(nextTask);
                    return;
                }
            }

            return;
        }


        private void SaveTask(ApprovalTask task)
        {
            
            dynamic parameters = new{};

            if(task.FinalApproverPersonId.HasValue && task.FinalApproverPersonId != 0)
            {
                parameters = new { 
                    Id = task.Id,
                    StatusCodeId = (int)task.StatusCode,
	                TaskCodeId = (int)task.TaskCode,
	                PositionId = task.ApproverPositionId.HasValue ? task.ApproverPositionId : 0,
	                ApproveGroupId = task.ApproveGroupId.HasValue ? task.ApproveGroupId : 0,
	                Comment = task.Comment,
	                WorkflowOrder = task.WorkflowOrder,
	                FinalApproverPersonId = task.FinalApproverPersonId,
	                CompletedDate = task.CompletedDate
                };
            }
            else
            {
                parameters = new { 
                    Id = task.Id,
                    StatusCodeId = (int)task.StatusCode,
	                TaskCodeId = (int)task.TaskCode,
	                PositionId = task.ApproverPositionId.HasValue ? task.ApproverPositionId : 0,
	                ApproveGroupId = task.ApproveGroupId.HasValue ? task.ApproveGroupId : 0,
	                Comment = task.Comment,
	                WorkflowOrder = task.WorkflowOrder
                };
            }

            bool result = db.Exec("SaveApprovalTask", parameters);
        }

        private void SaveWorkflow(ApprovalWorkflow aw)
        {

            db.Exec("SaveApprovalWorkflow", parameters: new
            {
                Id = aw.Id,
	            StatusCodeId = (int)aw.StatusCode,
	            ApprovalCodeId = (int)aw.ApprovalCode,
	            PositionId = aw.PositionId.HasValue ? aw.PositionId : 0,
	            PersonId = aw.PersonId.HasValue ? aw.PersonId : 0,
	            AppId = (int)aw.App
            });
        }



        // send current Par contents as email
        // TODO: accept additional model for email params (e.g. CC/BCC/Subject/etc.)?
        // add "[x] BCC me" checkbox by send button?
        private void EmailNotice(ApprovalTask task)
        {

            ApprovalEmailVm vm = db.GetByProc<ApprovalEmailVm>("GetApprovalEmailVm",
                parameters: new { TaskId = task.Id }).First();
            vm.LoadTaskData();
            vm.LoadDefaultEmailData();
            
            //set Urul path
            var fullUrl = HttpContext.Current.Request.Url.ToString();
            var urlPath = fullUrl.Substring(0, fullUrl.LastIndexOf("/api/")+1);
            vm.UrlPath = urlPath;

            var approot = HttpContext.Current.Server.MapPath("~");
            var template = File.ReadAllText(String.Format("{0}/Views/Approval/EmailTemplate.cshtml", approot));            

            //send default email
            SendApprovalEmailDb(vm, template);

            //send email to delegates
            foreach (var employee in vm.ApprovalTaskVm.ActiveDelegates)
            {
                //reset email values
                vm.EmailToAddress = employee.Email;
                vm.EmailToName = employee.LastName + ", " + employee.PreferredName;
                vm.EmailToPreferedName = employee.PreferredName;
                vm.EmployeePositionIdSentTo = (int)db.GetSingleByProc<ApprovalWorkflow>("GetPositionIdOfEmployee",
                    new { employeeId = employee.Id }).PositionId; //get Id value returned from stored procedure
                SendApprovalEmailDb(vm, template);
            }

            //Request.CreateResponse((saveCount == 1) ? HttpStatusCode.OK : HttpStatusCode.InternalServerError);
        }

        private void SendApprovalEmail(ApprovalEmailVm vm, string razorTemplate)
        {
            //SendApprovalEmailDb(vm, razorTemplate); return;

            var rslt = Razor.Parse(razorTemplate, vm);

            // TODO: checkbox for [] send me a copy, appending to Bcc list.

            var mFrom = new MailAddress("no-reply@cts.wa.gov", "CTS PAR System");
            
            //SWITCH WHEN READY TO GO LIVE WITH EMAILS
            var mTo = new MailAddress(WebConfigurationManager.AppSettings["parEmailTo"], WebConfigurationManager.AppSettings["parEmailToName"]);
            //var mTo = new MailAddress(vm.EmailToAddress, vm.EmailToName);
            
            var mm = new MailMessage(mFrom, mTo)
            {
                IsBodyHtml = true,
                Subject = String.Format("Approval Needed For PAR #{0} {1}: {2}", vm.ParId, vm.ParActionCode, vm.ParEmployeeLastName),
                Body = rslt
            };

            //SWITCH TO QUEUE METHOD TO SAVE FROM DELAY
            SmtpClient smtpClient = new SmtpClient(WebConfigurationManager.AppSettings["smtpServer"]);

            // DG: this should probably use a try/catch and log any failures. (I know, my stuff should too!)
            //smtpClient.Send(mm);
            // moved try/catch higher in call stack:
            try
            {
                smtpClient.Send(mm);
            }
            catch (Exception xcp)
            {
                //throw new Exception("error sending mail", xcp);
            }
            
            //save sent email to DB
            db.Exec("CreateParEmail", parameters: new
            {
                ParId = vm.ParId,
                HtmlBody = mm.Body,
                TimeSent = DateTime.Now,
                SentTo = mm.To[0].Address,
                SentBcc = "",
                SentFrom = mm.From.Address,
                AttachmentIds = ""
            });

           
        }


        //send via db service
        private void SendApprovalEmailDb(ApprovalEmailVm vm, string razorTemplate)
        {
            try
            {
                string body = Razor.Parse(razorTemplate, vm);
                string subject = String.Format("Approval Needed For PAR #{0} {1}: {2}", vm.ParId, vm.ParActionCode, vm.ParEmployeeLastName);
                string fromAddress = "no-reply@cts.wa.gov";
                string fromDisplayName = "CTS PAR System";

                db.Exec("CreateEmail", parameters: new
                {
                    fromAddress = fromAddress,
                    fromDisplayName = fromDisplayName,
                    toAddress = vm.EmailToAddress,
                    toDisplayName = vm.EmailToName,
                    body = body,
                    subject = subject,
                    isBodyHtml = true,
                    source = "ApprovalTask",
                    sourceId = vm.ApprovalTaskId
                });

                //save sent email to DB
                //db.Exec("CreateParEmail", parameters: new
                //{
                //    ParId = vm.ParId,
                //    HtmlBody = body,
                //    TimeSent = DateTime.Now,
                //    SentTo = vm.EmailToAddress,
                //    SentBcc = "",
                //    SentFrom = fromAddress,
                //    AttachmentIds = ""
                //});
            }
            catch { };
        }


        private void SendHrProccessEmailDb(ApprovalTask task)
        {
            ApprovalEmailVm vm = db.GetByProc<ApprovalEmailVm>("GetApprovalEmailVm",
                parameters: new { TaskId = task.Id }).First();
            vm.LoadTaskData();
            vm.LoadDefaultEmailData();

            //set Urul path
            var fullUrl = HttpContext.Current.Request.Url.ToString();
            var urlPath = fullUrl.Substring(0, fullUrl.LastIndexOf("/api/") + 1);
            vm.UrlPath = urlPath;

            var approot = HttpContext.Current.Server.MapPath("~");
            var template = File.ReadAllText(String.Format("{0}/Views/Approval/EmailApprovedHrTemplate.cshtml", approot));

            
            try
            {
                string body = Razor.Parse(template, vm);
                string subject = String.Format("Processing needed for Approved PAR #{0}: {1} for {2}", vm.ParId, vm.ParActionCode, vm.ParEmployeeLastName);
                string fromAddress = "no-reply@cts.wa.gov";
                string fromDisplayName = "CTS PAR System";

                db.Exec("CreateEmail", parameters: new
                {
                    fromAddress = fromAddress,
                    fromDisplayName = fromDisplayName,
                    toAddress = "hro@cts.wa.gov",
                    toDisplayName = "HR Office",
                    body = body,
                    subject = subject,
                    isBodyHtml = true,
                    source = "ApprovalTask",
                    sourceId = vm.ApprovalTaskId
                });
               
            }
            catch { };
        }



        #endregion

    }
}
