﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using System.Web.Helpers;

using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    [BaseApiAuth]
    public class PositionDescriptionController : BaseApiController
    {
        // TODO: parameterless Get to retrieve list...
        public PositionDescriptionVm Get(int id)
        {
            if (!HasAccess(id, ModelAction.Read))
            {
                // TODO: return HRM with Unauthorized status?
                throw new Exception("Unauthorized.");
            }
            // END section to move to private method...

            var pd = db.PositionDescriptions.Find(id);
            var pdvm = new PositionDescriptionVm
            {
                CurrentUser = ui,
                Description = pd,
                Duties = db.GetByProc<PositionDuty>("GetPositionDuties", new { PositionDescriptionId = id }),
                ApprovalTasks = db.GetByProc<ApprovalTaskPd>("GetPdApprovalTasks", new { PdId = id }),
                IncumbentName = db.GetSingleByProc<PdIncumbent>("GetPdIncumbentName", new {PdId = id}).IncumbentName
            };
            return pdvm;
        }

        // *NOTE*, must use different param name to distinguish this signature from Get(id): 
        // this is somewhat superfluous with PdQuery below...
        [ActionName("GetPdHistory")]
        public PdHistoryVm GetPdHistory(int positionid)
        {
            return new PdHistoryVm
            {
                CurrentUser = ui,
                Pos = db.GetSingleByProc<Position>("GetPosition", new { Id = positionid }),
                //PositionId = positionid, // pass thru
                History = db.GetByProc<PositionDescriptionRow>("GetPositionDescriptions", parameters: new { PositionId = positionid })
            };
        }

//        public PdqVm Get()
        // FromUri needed to format PdQuery from QueryString...
        public PdqVm Get([FromUri] PdQuery pq)
//        public List<PositionDescriptionRow> Get()
        {
            var rq = Request; // check for QueryString params...

            // TODO: generate q from Query String params..., Get(PdQuery q)
            var q = pq == null ? new PdQuery() { IncludeOld = false } : pq;
            var pdq = new PdqVm(db, ui, q);
            return pdq;
//            return db.GetByProc<PositionDescriptionRow>("GetPdList", new { LanId = ui.LanId });
        }

        // TODO: move generic DeleteById function into base class...
        public HttpResponseMessage Delete(int id)
        {
            try
            {
                if (!HasAccess(id, ModelAction.Delete))
                {
                    // TODO: return HRM with Unauthorized status?
                    throw new Exception("Unauthorized.");
                }

                // TODO: "instead-of" trigger to prevent deleting Completed / Approved PDs...
                // the ridiculously verbose way of deleting by ID from EF without pre-query.
                var pd = new PositionDescription
                    {
                        Id = id
                    };
                db.PositionDescriptions.Attach(pd);
                db.PositionDescriptions.Remove(pd);
                var deleted = db.SaveChanges();
                if (deleted != 1)
                {
                    throw new Exception("delete PositionDescription failed.");
                }
                else { hrm.StatusCode = HttpStatusCode.OK; }
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        public HttpResponseMessage Put(PositionDescription pd)
        {
            try
            {
                if (!HasAccess(pd.Id, ModelAction.Update))
                {
                    // TODO: return HRM with Unauthorized status?
                    throw new Exception("Unauthorized.");
                }

                db.PositionDescriptions.Attach(pd);
                db.Entry(pd).State = System.Data.EntityState.Modified;
                var updated = db.SaveChanges();
                if (updated != 1)
                {
                    throw new Exception("update PositionDescription failed.");
                }
                else { hrm.StatusCode = HttpStatusCode.OK; }
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                    //                System.Net.HttpStatusCode.Unauthorized, // Unauthorized response triggers login box on IE8
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message
                );
            }
            return hrm;
        }

        // *NOTE*, only valid field for incoming PD is PositionId.
        public HttpResponseMessage Post(PositionDescription pd)
        {
            try
            {
                if (pd.PositionId.HasValue && !HasAccess(pd.Id, ModelAction.Create))
                {
                    throw new Exception("Unauthorized.");
                }

                PositionDescriptionRow newPd = new PositionDescriptionRow();

                // LinkedParId is set when generating new Pdf from PAR:
                // TODO: separate proc for CreatePdFromModifyPar (no PD approval gen)
                if (pd.LinkedParId.HasValue)
                {
                    newPd = (pd.PositionId.HasValue) ?
                        db.GetSingleByProc<PositionDescriptionRow>("CreatePdFromPar", new
                        {
                            PositionId = pd.PositionId.Value,
                            LanId = ui.LanId,
                            LinkedParId = pd.LinkedParId.Value,
                            WorkingTitle = pd.WorkingTitle,
                            ProposedJobId = pd.ProposedJobId,
                            PositionTypeCXW = pd.PositionTypeCXW,
                            xProposedJvacEvalPts = pd.xProposedJvacEvalPts,
                            IsSupervisor = pd.IsSupervisor
                        })
                        : db.GetSingleByProc<PositionDescriptionRow>("CreateBlankPositionDescription", new
                        {
                            LanId = ui.LanId,
                            LinkedParId = pd.LinkedParId.Value,
                            WorkingTitle = pd.WorkingTitle,
                            ProposedJobId = pd.ProposedJobId,
                            PositionTypeCXW = pd.PositionTypeCXW,
                            xProposedJvacEvalPts = pd.xProposedJvacEvalPts,
                            IsSupervisor = pd.IsSupervisor
                        });
                }

                else
                {
                    // *NOTE*, blank PD requires no approval.
                    // New Position PAR approval linked to blank PD would suffice.
                    newPd = (!pd.PositionId.HasValue) ? db.GetSingleByProc<PositionDescriptionRow>("CreateBlankPositionDescription", new { LanId = ui.LanId })
                    : db.GetSingleByProc<PositionDescriptionRow>("CreatePdWithApproval", new
                    {
                        PositionId = pd.PositionId,
                        LanId = ui.LanId
                    });
                }
                hrm.Content = new StringContent(Json.Encode(newPd));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        [ActionName("Clone")]
        public HttpResponseMessage PostClone(int id)
        {
            // TODO: general utility for trying procs and returning results...
            try
            {
                if (!HasAccess(id, ModelAction.Create))
                {
                    // TODO: return HRM with Unauthorized status?
                    throw new Exception("Unauthorized.");
                }
                var clone = db.GetSingleByProc<PositionDescriptionRow>("ClonePositionDesc", new { PdId = id, LanId = ui.LanId });
                // Workflow here or directly in ClonePositionDesc proc above
                hrm.Content = new StringContent(Json.Encode(clone));
                hrm.StatusCode = HttpStatusCode.Created;
            }
            catch (System.Data.UpdateException uxcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                uxcp.Message);
            }
            catch (Exception xcp)
            {
                hrm = ControllerContext.Request.CreateErrorResponse(
                System.Net.HttpStatusCode.InternalServerError,
                xcp.Message);
            }
            return hrm;
        }

        // TODO: move this into base class, inject AccessCheck sproc name...
        private bool HasAccess(int id, ModelAction xn)
        {
            // TODO: get ActionFlags for current user, ModelAction.
            var permitted = ActionFlags.Read;
            if (xn != ModelAction.Read && xn != ModelAction.Create)
            {
                // if PD Workflow status is Created and Position is not own or user is Admin/HR, add Write+Delete:
                var pmal = db.GetByProc<ModelActionVal>("GetPdPermittedActions", new { PdId = id, LanId = ui.LanId });
                foreach (var pma in pmal)
                {
                    if (!pma.Value.HasValue) { continue; } // skip nulls
                    permitted = permitted | (ActionFlags)pma.Value.Value;
                }

                if (!permitted.HasFlag((ActionFlags)xn))
                {
                    return false;
                }
            }

            return db.GetSingleByProc<AccessCheck>("PdPosIsInSpanOfControl", new
            {
                PdId = id,
                LanId = ui.LanId
            }).IsSub;
        }
    }
}

namespace HrAdmin2.Models
{
    public class ModelActionVal
    {
        public int? Value { get; set; }
    }

    public class PdIncumbent
    {
        public string IncumbentName { get; set; }
    }

    public class AccessCheck
    {
        public bool IsSub { get; set; }
    }
}