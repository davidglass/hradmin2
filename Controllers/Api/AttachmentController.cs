﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Diagnostics;
using System.IO;
using System.Web.Mvc;
using HrAdmin2.Models;
using System.Threading.Tasks;
using HrAdmin2.Models.ViewModels;

using System.Net.Http.Headers;

namespace HrAdmin2.Controllers.Api
{
    public class AttachmentController : BaseApiController
    {

        //private HrAdminDbContext db = new HrAdminDbContext();

        // GET api/Attachment
        public IEnumerable<AttachmentInfoVm> Get()
        {
            try
            {

                List<AttachmentInfoVm> attachments = db.GetByProc<AttachmentInfoVm>("GetAttachmentInfo",
                    parameters: new {}); 
                
                return attachments;


            }
            catch (Exception e)
            {
                throw e;
            }
        }

        // GET api/Attachment/5
        //returns file result for downloading specific file
        public Attachment Get(int id)
        {
            try
            {

                Attachment attachment = db.GetSingleByProc<Attachment>("GetAttachment",
                    parameters: new { Id = id });               

                return attachment;

            }
            catch (Exception e)
            {
                throw e;
            }
            
        }

        // TODO: consider moving this to an Mvc controller instead of Api?
        // TODO: or consider moving this to Api/ParController.
        // GET api/Attachment/5/attachment 
        [System.Web.Http.ActionName("Attachment")]
        public HttpResponseMessage GetFile(int id)
        {
            Attachment attachment = db.GetSingleByProc<Attachment>("GetParAttachment",
                parameters: new { Id = id });               
            // assuming file was found in db,
            var r = Request.CreateResponse(HttpStatusCode.OK);
            r.Content = new StreamContent(new MemoryStream(attachment.Content));
            r.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            r.Content.Headers.ContentDisposition.FileName = attachment.FileName;
            r.Content.Headers.ContentDisposition.Size = attachment.SizeInBytes;
            return r;
        }

        // POST api/Attachment
        public async Task<HttpResponseMessage> PostFormData()
        //public async Task<AttachmentInfoVm> PostFormData()
        {
            //http://www.strathweb.com/2012/08/a-guide-to-asynchronous-file-uploads-in-asp-net-web-api-rtm/
            //http://www.asp.net/web-api/overview/working-with-http/sending-html-form-data,-part-2
            // Check if the request contains multipart/form-data. 
            if (!Request.Content.IsMimeMultipartContent())
            {
                throw new HttpResponseException(HttpStatusCode.UnsupportedMediaType);
            }

            try
            {

                //Request.Content.ReadAsMultipartAsync<MultipartMemoryStreamProvider>(new MultipartMemoryStreamProvider()).ContinueWith((tsk) => 
                //    { 
                //        MultipartMemoryStreamProvider provider = tsk.Result; 

                var provider = new MultipartMemoryStreamProvider();
                
                
                // Read the form data. 
//                var multipart = Request.Content.ReadAsMultipartAsync(provider).ContinueWith(t =>
                // moving response into async part -DG
                var multipart = Request.Content.ReadAsMultipartAsync(provider).ContinueWith<HttpResponseMessage>(t =>
                    {
                        //var temp = provider
                        var reference = provider.Contents.First(i => i.Headers.ContentDisposition.Name.Contains("\"appointmentId"));
                        var item = provider.Contents.First(i => i.Headers.ContentDisposition.Name == "\"file\"");
                        //foreach (var item in provider.Contents)                        
                        {

                            //setup appointment Id
                            var appointmentId = reference.Headers.ContentDisposition.Name.TrimEnd('"');
                            int p = appointmentId.LastIndexOf("=");
                            appointmentId = appointmentId.Substring(p + 1);

                            var attachment = new Attachment() { Description = "" };
                            

                            //setup filename
                            attachment.FileName = item.Headers.ContentDisposition.FileName.TrimEnd('"');
                            int position = attachment.FileName.LastIndexOf("\\");
                            attachment.FileName = attachment.FileName.Substring(position + 1);

                            attachment.SizeInBytes = (long)item.Headers.ContentLength;

                            //if contenttype is null then assume coming from MVC controller with type set to name of file.
                            if (item.Headers.ContentType == null)
                            {
                                attachment.MimeType = item.Headers.ContentDisposition.Name;
                                //have to trim off quotes
                                attachment.MimeType = attachment.MimeType.Substring(1, attachment.MimeType.Length - 2);
                            }
                            else
                            {
                                attachment.MimeType = item.Headers.ContentType.MediaType;
                            }

                            byte[] fileData = new byte[attachment.SizeInBytes];
                            // DG: this is including (prepending/appending) extraneous header and part dividers to actual file data, e.g.:
                            // -----------------------------7dd1ca9600d5a
                            // Content-Disposition: form-data; name="file"; filename="C:\downloads\dev tools\jstree-v.pre1.0\jquery.jstree.js"
                            // Content-Type: application/javascript
                            // [ACTUAL FILE CONTENT IS HERE]
                            // -----------------------------7dd1ca9600d5a--
                            Stream stream = item.ReadAsStreamAsync().Result;
                            stream.Read(fileData, 0, Int32.Parse(attachment.SizeInBytes.ToString()));
                            attachment.Content = fileData;

                            // added exec check (must be in async block) -DG
                            // TODO: return AttachmentInfo object via db.GetSingleByProc<>
                            if (! db.Exec("AddAttachment",
                                parameters: new
                                {
                                    FileName = attachment.FileName,
                                    Description = attachment.Description,
                                    Content = attachment.Content,
                                    SizeInBytes = attachment.SizeInBytes,
                                    MimeType = attachment.MimeType,
                                    //TODO: hard coded
                                    AppointmentId = appointmentId
                                })
                            ) {
                                return new HttpResponseMessage() {
                                    // IE can't handle content with error codes in non-AJAX response:
                                    //StatusCode = HttpStatusCode.InternalServerError,
                                    Content = new StringContent("attachment failed.")
                                };
                            }
                            //async complete return
                            //return new AttachmentInfoVm()
                            //{
                            //    FileName = attachment.FileName,
                            //    Description = attachment.Description,
                            //    SizeInBytes = attachment.SizeInBytes,
                            //    MimeType = attachment.MimeType
                            //};
                            HttpResponseMessage response = new HttpResponseMessage();
                            //add response for fine uploader
                            response.Content = new StringContent("{\"success\":true}");
                            return response;
                        }
                    });

                return await multipart;
                //HttpResponseMessage response = new HttpResponseMessage();
                ////add response for fine uploader
                //response.Content = new StringContent("{\"success\":true}");
                //return response;

            }
            catch (System.Exception e)
            {
                //return new AttachmentInfoVm();
                return Request.CreateErrorResponse(HttpStatusCode.InternalServerError, e);
            }

        }

        //currently only delete or add attachments
        // PUT api/values/5
        //public void Put(int id, [FromBody]string value)
        //{
        //}

        // DELETE api/values/5
        public HttpResponseMessage Delete(int id)
        {
            try
            {
//                string qry = "Delete from [Location].[dbo].[Attachment] where [Id] =" + id;
                if (!db.Exec("DeleteParAttachment", new { Id = id }))
                {
                    throw new Exception("DeleteParAttachment failed.");
                }
                //var cmd = new SqlCommand { CommandText = qry };

                //return ExecuteNoneQuery(cmd);
            }
            catch
            {
                return new HttpResponseMessage(HttpStatusCode.InternalServerError)
                {
                    ReasonPhrase = "delete failed."
                };
            }
            
            return new HttpResponseMessage(HttpStatusCode.OK);
        }
    
    }
}
