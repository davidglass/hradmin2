﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web;

using System.Web.Configuration;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;
using System.Web.Helpers;
using HrAdmin2.Filters;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using System.Net.Mail;
using System.IO;

using NLog;
using RazorEngine;

using System.Web.Mvc;
using ActionName = System.Web.Http.ActionNameAttribute;
using AuthFilter;

namespace HrAdmin2.Controllers.Api
{
    [BaseApiAuth]
    public class ParController : BaseApiController
    {
        // Attachment list functionality moved to ParDetailVm
        //[ActionName("Attachment")]
        //public List<ParAttachment> GetAttachments(int id)
        //{
        //    // TODO: get existing ParAttachments from db by ParId...
        //    return new List<ParAttachment> {
        //        new ParAttachment {
        //            Id = 0,
        //            ParId = id,
        //            FileName = "FakeFileName.pdf"
        //        }
        //    };
        //}

        [ActionName("Attachment")]
        // POST api/Par/5/attachment
        //        public async Task<HttpResponseMessage> PostAttachment(int id)
        public HttpResponseMessage PostAttachment(int id)
        {
            var rq = Request;
            var ctx = (HttpContextWrapper)rq.Properties["MS_HttpContext"];
            HttpPostedFileBase fb = ctx.Request.Files.Get(0); // first file object (actually HttpPostedFileWrapper subclass)
            var fn = fb.FileName.Substring(fb.FileName.LastIndexOf("\\") + 1);

            try
            {
                var data = (new StreamContent(fb.InputStream)).ReadAsByteArrayAsync().Result;
                var pa = db.GetSingleByProc<ParAttachment>("AddParAttachment", parameters: new
                {
                    FileName = fn,
                    Description = "",
                    Content = data,
                    SizeInBytes = (int)fb.InputStream.Length, // Length is Long.
                    MimeType = fb.ContentType,
                    ParId = id
                });
                // TODO: modify AddAttachment to return full new attachment info, including timestamp, etc.
                // ALSO, use either SCOPE_IDENTITY() or sequence for new attachment id (not select top 1)
                // for now, manually setting FileName and ParId since only Id is returned.
                //pa.FileName = fn;
                //pa.ParId = id;

                var hrm = new HttpResponseMessage()
                {
                    Content = new StringContent(Json.Encode(pa))
                };
                var h = hrm.Headers;
                return hrm;
            }

            catch (Exception xcp)
            {
                var hrm = new HttpResponseMessage()
                {
                    // *NOTE*, IE prevents access to response content on non-AJAX (hidden frame target) post.
                    // if http error status is returned, so omit or set to OK despite error here.
                    // TODO: set up error logging.
                    //StatusCode = HttpStatusCode.OK,
                    Content = new StringContent(Json.Encode(
                        new
                        {
                            error = xcp.Message
                        }))
                };
                return hrm;
            }
        }

        //// DELETE api/Par/5
        public HttpResponseMessage Delete(int id)
        {
//            ApptActionRequest p = db.Requests.Find(id);
            bool deleted = false;
            try
            {
                deleted = db.Exec("DeletePar", parameters: new { ParId = id });
            }
            catch (Exception xcp)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, xcp);
            }
            // this fails since Par subclass (ParDetail) exists:
            // "Invalid column name 'Discriminator'...etc."
            //Par p = db.Pars.Find(id);
            //if (p == null)
            //{
            //    return Request.CreateResponse(HttpStatusCode.NotFound);
            //}

            //db.Pars.Remove(p);
            ////db.Requests.Remove(p);

            //try
            //{
            //    db.SaveChanges();
            //}
            //catch (DbUpdateConcurrencyException ex)
            //{
            //    return Request.CreateErrorResponse(HttpStatusCode.NotFound, ex);
            //}

            return Request.CreateResponse(deleted ? HttpStatusCode.OK : HttpStatusCode.InternalServerError);
//            return Request.CreateResponse(HttpStatusCode.OK, p);
        }

        private string ConvertEmptyStringToNull(string mystring)
        {
            return String.IsNullOrEmpty(mystring) ? null : mystring;
        }

        private void ConvertEmptyCharPropsToNull(ParDetail pd)
        {
            // else space-pads empty strings stored as db char fields:
            pd.ApptFillOtEligCode = ConvertEmptyStringToNull(pd.ApptFillOtEligCode);
            pd.ApptPayScaleReason = ConvertEmptyStringToNull(pd.ApptPayScaleReason);
            pd.ApptTimeMgtStatus = ConvertEmptyStringToNull(pd.ApptTimeMgtStatus);
            pd.ContractTypeCode = ConvertEmptyStringToNull(pd.ContractTypeCode);
            pd.EmpAddrCountyCode = ConvertEmptyStringToNull(pd.EmpAddrCountyCode);
            pd.EmpAddrState = ConvertEmptyStringToNull(pd.EmpAddrState);
            pd.EmpGender = ConvertEmptyStringToNull(pd.EmpGender);
            pd.FromAgencyCode = ConvertEmptyStringToNull(pd.FromAgencyCode);
            pd.PosEeGroup = ConvertEmptyStringToNull(pd.PosEeGroup);
            pd.PosEeSubgroup = ConvertEmptyStringToNull(pd.PosEeSubgroup);
            pd.PosPersonnelSubArea = ConvertEmptyStringToNull(pd.PosPersonnelSubArea);
            pd.PosShiftCode = ConvertEmptyStringToNull(pd.PosShiftCode);
            pd.PosWorkersCompCode = ConvertEmptyStringToNull(pd.PosWorkersCompCode);
            pd.TargetAgencyCode = ConvertEmptyStringToNull(pd.TargetAgencyCode);
            pd.WorkScheduleCode = ConvertEmptyStringToNull(pd.WorkScheduleCode);
        }

        // ValidationActionFilter is redundant here since validation is triggered by EF SaveChanges().
        //[ValidationActionFilter]
        public HttpResponseMessage Put(int id, ParDetail pd) // NOTE, formatter that converts POST body to Model omits URL params. (id)
        {
            StripTimeZone(pd);
            if (pd.ContractTypeCode != null && pd.ContractTypeCode.Equals("--"))
            {
                // double-hyphen value added since blanks break Angular binding.  reverting here for db:
                pd.ContractTypeCode = "  ";
            }

            ConvertEmptyCharPropsToNull(pd);
            if (!String.IsNullOrEmpty(pd.EmpSSN))
            {
                // strip non-numeric input, if any:
                pd.EmpSSN = System.Text.RegularExpressions.Regex.Replace(pd.EmpSSN, "[^\\d]", "");
            }
            //var vc = new System.ComponentModel.DataAnnotations.ValidationContext(pd);
            if (!ModelState.IsValid && pd.Validate)
            {
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, ModelState);
            }
            var u = User.Identity.Name;

            // update ParDetail, then return current record with changes...
            var pvm = new ParDetailVm(pd, db);
            try
            {
                var updated = pvm.Update(validate: pd.Validate);
            }
            catch (Exception xcp)
            {
                var errrsp = Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, xcp);
                return errrsp;
            }

            //var updated = pvm.Update(validate: false); // updates Par info without validation, re-queries
            // PUT should return nothing...re-get if data refresh is needed.
            //            return Request.CreateResponse(HttpStatusCode.OK, updated);
            // return ModelState even if not officially validating:
            return Request.CreateResponse(HttpStatusCode.OK, pvm.ValErr);
        }

        private void StripTimeZone(ParDetail pd)
        {
            // strip timezone offset from dates:
            if (pd.ApptPayScaleEffectiveDate.HasValue) { pd.ApptPayScaleEffectiveDate = pd.ApptPayScaleEffectiveDate.Value.Date; }
            if (pd.ApptPayNextIncrease.HasValue) { pd.ApptPayNextIncrease = pd.ApptPayNextIncrease.Value.Date; }
            if (pd.ApptStepMEligibleDate.HasValue) { pd.ApptStepMEligibleDate = pd.ApptStepMEligibleDate.Value.Date; }
            if (pd.ContractEndDate.HasValue) { pd.ContractEndDate = pd.ContractEndDate.Value.Date; }
            if (pd.EmpDS01_Anniversary.HasValue) { pd.EmpDS01_Anniversary = pd.EmpDS01_Anniversary.Value.Date; }
            if (pd.EmpDS02_Appointment.HasValue) { pd.EmpDS02_Appointment = pd.EmpDS02_Appointment.Value.Date; }
            if (pd.EmpDS03_CtsHire.HasValue) { pd.EmpDS03_CtsHire = pd.EmpDS03_CtsHire.Value.Date; }
            if (pd.EmpDS04_PriorPid.HasValue) { pd.EmpDS04_PriorPid = pd.EmpDS04_PriorPid.Value.Date; }
            if (pd.EmpDS05_Seniority.HasValue) { pd.EmpDS05_Seniority = pd.EmpDS05_Seniority.Value.Date; }
            if (pd.EmpDS07_UnbrokenService.HasValue) { pd.EmpDS07_UnbrokenService = pd.EmpDS07_UnbrokenService.Value.Date; }
            if (pd.EmpDS09_VacLeaveFrozen.HasValue) { pd.EmpDS09_VacLeaveFrozen = pd.EmpDS09_VacLeaveFrozen.Value.Date; }
            if (pd.EmpDS18_PersonalHolidayElg.HasValue) { pd.EmpDS18_PersonalHolidayElg = pd.EmpDS18_PersonalHolidayElg.Value.Date; }
            if (pd.EmpDS26_PersonalLeaveElg.HasValue) { pd.EmpDS26_PersonalLeaveElg = pd.EmpDS26_PersonalLeaveElg.Value.Date; }
            if (pd.EmpDOB.HasValue) { pd.EmpDOB = pd.EmpDOB.Value.Date; }
        }

        // send current Par contents as email
        // TODO: accept additional model for email params (e.g. CC/BCC/Subject/etc.)?
        // add "[x] BCC me" checkbox by send button?
        // POST api/Par/5/email
        [ActionName("Email")]
        public HttpResponseMessage PostEmail(int id)
        {
            var sent = new ParEmail();
            // TODO: create separate, email-specific View / template.
            var parDetail = db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = id, LanId = User.Identity.Name });
            var vm = new ParDetailVm(parDetail, db)
            {
                ViewMode = "read" // TODO: make this enum...
            };

            var approot = HttpContext.Current.Server.MapPath("~");
            var template = File.ReadAllText(String.Format("{0}/Views/Par/EmailTemplate.cshtml", approot));
            var rslt = Razor.Parse(template, vm);

            var ws = WebConfigurationManager.AppSettings;
            // TODO: checkbox for [] send me a copy, appending to Bcc list.
            var sc = new SmtpClient(WebConfigurationManager.AppSettings["smtpServer"]);
            var mFrom = new MailAddress("no-reply@cts.wa.gov", "CTS PAR System");
            //var mTo = new MailAddress(WebConfigurationManager.AppSettings["parEmailTo"]
            //    , WebConfigurationManager.AppSettings["parEmailToName"]);
            var mToString = WebConfigurationManager.AppSettings["parEmailTo"];
            // omitting ToName to enable multiple Tos:
            //var mToName = WebConfigurationManager.AppSettings["parEmailToName"];

            string[] mToList = mToString.Split(new char[] { ',' });

            var mm = new MailMessage()
//                var mm = new MailMessage(mFrom, mTo)
            {
                IsBodyHtml = true,
                Subject = String.Format("CTS PAR #{0}: {1}, {2}",
                    parDetail.Id, parDetail.ActionDescription, parDetail.EmpLastName),
                Body = rslt,
                From = mFrom
            };
            for (int k = 0; k < mToList.Length; k++)
            {
                mm.To.Add(new MailAddress(mToList[k]));
            }
            var mCcString = WebConfigurationManager.AppSettings["parEmailCc"];
            if (mCcString != null)
            {
                string[] mCcList = mCcString.Split(new char[] { ',' });
                for (int j = 0; j < mCcList.Length; j++)
                {
                    mm.CC.Add(new MailAddress(mCcList[j]));
                }
            }
            var mBccString = WebConfigurationManager.AppSettings["parEmailBcc"];
            if (mBccString != null)
            {
                string[] mBccList = mBccString.Split(new char[] { ',' });
                for (int h = 0; h < mBccList.Length; h++)
                {
                    mm.Bcc.Add(new MailAddress(mBccList[h]));
                }
            }
            var attachedFiles = db.GetByProc<HrAdmin2.Models.Attachment>("GetParAttachedFiles", parameters: new { ParId = id });
            int[] attachIds = new int[attachedFiles.Count];
            int i = 0;
            foreach (var f in attachedFiles)
            {
                attachIds[i++] = f.AttachmentId;
                mm.Attachments.Add(
                    new System.Net.Mail.Attachment(
                        new MemoryStream(f.Content),
                        f.FileName)
                ); // TODO: include MIME type?
            }
            sent.AttachmentIds = String.Join(",", attachIds);
            sent.HtmlBody = mm.Body;
            sent.ParId = id;
            sent.TimeSent = DateTime.Now;
            sent.SentBcc = AddressesToCsv(mm.Bcc);
            sent.SentFrom = mm.From.Address;
            sent.SentTo = mm.To[0].Address;
            // unhandled BaseApiController exceptions are now handled automatically by Filters.LoggedExceptionFilterAttribute...
            //try
            //{
            sc.Send(mm);
            db.ParEmails.Add(sent);
            var saveCount = db.SaveChanges(); // this is failing for Cher, aborting transaction after insert.
            return Request.CreateResponse(HttpStatusCode.OK);
        }

        private Exception GetOriginalException(Exception xcp) {
            return (xcp.InnerException == null) ? xcp : GetOriginalException(xcp.InnerException); 
        }

        [ActionName("ParEmp")]
        public ParDetail GetParWithEmployee(int id, int empid)
        {
            if (empid == 999999)
            {
                throw new Exception("invalid Employee ID!");
            }
            var pd = db.GetSingleByProc<ParDetail>("GetParDetail", parameters: new { Id = id, EmpId = empid, LanId = User.Identity.Name });
            return pd;
            //return new ParDetailVm(pd, db)
            //{
            //    ViewMode = "edit"
            //};
        }

//        public ParDetailVm Post(Par p)
        public ParDetailVm Post(ParDetail p) // switched to ParDetail to enable creating PARs from PositionDescriptions...
        {
            // first set PosSupervisorPosId from current user
            // if PAR is being spawned from a PD-f:
            if (p.PdfId.HasValue && !p.PosSupervisorPosId.HasValue && !ui.RoleName.Equals("HR") && !ui.RoleName.Equals("Admin"))
            {
                // TODO: verify this works...created 2014-11-14 DG
                // currently sups lack CRUD for positionless PDs.
                p.PosSupervisorPosId = ui.PositionId;
            }

            int approvalWorkflowId = -1;
            //create workflow for Par
            if (p.PosSupervisorPosId > 0 && p.PositionId < 1)
            {
                //new position case
                approvalWorkflowId = (new ApprovalWorkflowController()).CreateApprovalWorkflow(
                    ApprovalWorkflow.approvalApp.PAR, p.PosSupervisorPosId, null, true);
            }
            else
            {
                approvalWorkflowId = (new ApprovalWorkflowController()).CreateApprovalWorkflow(
                    ApprovalWorkflow.approvalApp.PAR, p.PositionId, p.EmployeeId);
            }                       
            p.ApprovalWorkflowId = approvalWorkflowId;

            //if (p.IsPayChangeOnly)
            //{
            //    // look up Appointment PayScale TypeArea, set PAR action reason to
            //    // "NonRep - Exempt" ? Exempt (Reason.Id = 17, code = '24')
            //    // "NonRep - WMS" ? WMS (Reason.Id = 85, code = '07')
            //    // else Permanent (Reason.Id = 60, code = '71')
            //}

            var pd = db.GetSingleByProc<ParDetail>("CreatePar",
                    parameters: new
                    {
                        LanId = User.Identity.Name,
                        ApprovalWorkflowId = approvalWorkflowId,
                        EmployeeId = p.EmployeeId, // automatically 0 if omitted
                        PosId = p.PositionId, // automatically 0 if omitted
//                        OrgUnitId = p.OrgUnitId,
                        OrgUnitId = p.PosOrgUnitId.HasValue ? p.PosOrgUnitId : 0,
                        PosSupervisorPosId = p.PosSupervisorPosId.HasValue ? p.PosSupervisorPosId.Value : 0,
                        ActionType = p.ActionCode,
                        // must explicitly set to 0 here if null
                        // *NOTE*, IsPayChangeOnly will automatically trigger ActionReasonId to be set based on rubric above
                        ActionReasonId = p.ActionReasonId.HasValue ? p.ActionReasonId.Value : 0,
                        IsTransferIn = p.IsTransferIn,
                        IsTransferOut = p.IsTransferOut,
                        IsPayChangeOnly = p.IsPayChangeOnly,
                        IsNameChangeOnly = p.IsNameChangeOnly,
                        IsNoShow = p.IsNoShow,
                        // next 6 params are added for creating PAR from PDF:
                        // TODO: move them from ParDetail to Par ???
                        PosWorkingTitle = p.PosWorkingTitle,
                        PosJobId = p.PosJobId,
                        PersonnelSubArea = p.PosPersonnelSubArea,
                        PosIsSupervisor = p.PosIsSupervisor,
                        PosPointValue = p.PosPointValue,
                        PdfId = p.PdfId.HasValue ? p.PdfId : 0
                    });
            // technically (ReSTfully), this should return a 201 Created with Location header to new resource.
            // since request is XHR, would need to manually capture Location header and redirect on client.
            // see http://stackoverflow.com/questions/14748820/missing-location-header-on-jquery-ajax-post
            return new ParDetailVm(pd, db);
            //switch (p.ActionType)
            //{
            //    case "U5":
            //        var pd = db.GetSingleByProc<ParDetail>("CreatePar",
            //                parameters: new
            //                {
            //                    LanId = User.Identity.Name,
            //                    EmployeeId = p.EmployeeId,
            //                    PosId = p.PositionId, // technically PosId could be omitted...
            //                    ActionType = p.ActionType
            //                });
            //            //                    newPar.ActionReasonId = 0; // must set default or binding fails...
            //            return new ParDetailVm(pd, db);
            //    default: throw new Exception("create Par failed.");
            //}
        }

        private string AddressesToCsv(MailAddressCollection addressList)
        {
            var csv = new string[addressList.Count];
            int i = 0;
            foreach (MailAddress ma in addressList)
            {
                csv[i++] = ma.Address;
            }
            return String.Join(",", csv);
        }

    }
}
