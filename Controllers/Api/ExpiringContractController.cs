﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

using HrAdmin2.Models;

namespace HrAdmin2.Controllers.Api
{
    public class ExpiringContractController : BaseApiController
    {
        // GET api/expiringcontract
        public List<ExpiringContract> Get()
        {
            return db.GetByProc<ExpiringContract>("GetExpiringContracts", new { });
        }

        // GET api/expiringcontract/5
        public string Get(int id)
        {
            return "value";
        }
    }
}
