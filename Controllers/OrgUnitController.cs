﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class OrgUnitController : Controller
    {
        HrAdminDbContext db = new HrAdminDbContext();
        //
        // GET: /OrgUnit/
        public ActionResult Index()
        {
            return new RedirectResult(Url.Content("~/orgunit/31008202")); // redirect to CTS root OrgUnit if unspecified
        }

        public ActionResult Detail(int id)
        {
            // Pre-expanding root node now:
            db.Exec("ExpandUserOrgUnitNode",
                parameters: new
                {
                    NodeId = id,
                    //                            AppUserId = AppUserId(),
                    //AppUserId = 1,
                    LanId = User.Identity.Name,
                    IncludeSubNodes = false
                }
            );

            var tree = new OrgNodeJsTree(
                db.GetByProc<OrgNode>("GetOrgStructure", parameters: new
                {
                    RootId = id,
                    // TODO: Auth, pass AppUserId
                    //UserId = 1
                    LanId = User.Identity.Name
                })
            );
            var omm = new OrgManagerVm {
                Crumbs = db.GetByProc<OuLinkVm>("GetOuBreadcrumbs", parameters: new {Id = id}),
                Tree = tree,
                OrgUnitSelection = db.GetByProc<SelectListItem>("GetOuSelection", parameters: new { }),
                ContractList = db.GetByProc<SelectListItem>("GetContractSelection", parameters: new { }),
                CountyList = db.GetByProc<SelectListItem>("GetCountySelectList", parameters: new { }),
                // TODO: modify this proc to return Text and Value!!!
                JobClassList = db.GetByProc<SelectListItem>("GetJobClassSelection", parameters: new { }),
                WorkScheduleList = db.GetByProc<SelectListItem>("GetWorkScheduleRules", parameters: new {})
            };
            // pre-pend empty selections:
            PrependBlankSelection(omm.JobClassList);
            PrependBlankSelection(omm.ContractList);
            PrependBlankSelection(omm.CountyList);
            PrependBlankSelection(omm.WorkScheduleList);
            // TODO: convert other selects to Lists, *not* plain IEnumerables
            //omm.JobClassList.Insert(0, new SelectListItem()
            //{
            //    Text = " - select - ",
            //    Value = "",
            //    Selected = true
            //});
//            return View(tree);
            return View(omm);
        }

        private void PrependBlankSelection(List<SelectListItem> optlist)
        {
            optlist.Insert(0, new SelectListItem()
            {
                Text = " - select - ",
                Value = "",
                Selected = true
            });
        }

    }

    public class OuLinkVm : ModelBase
    {
        public string ShortName { get; set; }
    }

    public class OrgManagerVm {
        public List<OuLinkVm> Crumbs { get; set; }
        public OrgNodeJsTree Tree { get; set; }
        public IEnumerable<SelectListItem> OrgUnitSelection { get; set; }
        public List<SelectListItem> ContractList { get; set; }
        public List<SelectListItem> CountyList { get; set; }
        public List<SelectListItem> JobClassList { get; set; }
        public List<SelectListItem> WorkScheduleList { get; set; }
    }
}
