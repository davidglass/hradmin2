﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
    public class ExpirationController : BaseMvcController
    {
        // GET: /Expiration/
        // *NOTE*, this controller includes only BaseVm.
        // navigation to specific Expiration types (Contract, Delegation) use Angular hash routing within single view.
        // viewmodels will be loaded via ApiControllers unless / until there is a performance reason for embedding directly in Views via MvcController.
        // this can be seen as a prototype for a SPA (Single Page Application).
        public ActionResult Index()
        {
            return View("~/Views/Admin/Expiration.cshtml", ViewModel);
        }
    }
}
