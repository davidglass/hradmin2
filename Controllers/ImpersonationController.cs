﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    public class ImpersonationController : BaseMvcController
    {
        //
        // GET: /Impersonation/

        public ActionResult Index()
        {
            List<Impersonation> imps = db.GetByProc<Impersonation>("GetImpersonatedUserId", new { LanId = User.Identity.Name });
            var vm = new ImpersonationVm
            {
                CurrentUser = ui,
                UserSelection = db.GetByProc<SelectListItem>("GetUserEmpSelection", new { }),
                Imp = (imps.Count == 1) ? imps[0] : new Impersonation()
            };
            vm.UserSelection.Insert(0, new SelectListItem { Text = " - select employee - ", Value = "", Selected = true });
            return View("~/Views/Admin/Impersonation.cshtml", vm);
        }

    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class ImpersonationVm : BaseVm
    {
        public List<SelectListItem> UserSelection { get; set; }
        public Impersonation Imp { get; set; }
        //public System.Security.Principal.IIdentity RealId { get; set; }
    }

    public class Impersonation
    {
        public int? ImpersonatingUserId { get; set; }
    }
}