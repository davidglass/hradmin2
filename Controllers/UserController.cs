﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
    public class UserController : BaseMvcController
    {
//        private HrAdminDbContext db = new HrAdminDbContext();
        
        public ActionResult Index()
        {
            var uvm = new UserMgtVm {
                CurrentUser = ui,
                CurrentEmployees = db.GetByProc<SelectListItem>("GetCurrentEmpSelection", new { })
            };
            uvm.CurrentEmployees.Insert(0, new SelectListItem { Value = "", Text = " - select - " });
            var u = db.GetByProc<EmpUser>("GetCurrentAppUsers", new { });
            // convert List to Dict for easy indexed access in client:
            var aud = new Dictionary<string, EmpUser>();
            foreach (var au in u)
            {
                aud[au.EmployeeId.ToString()] = au; // could also cast to varchar in proc...
            }
            uvm.EmpUsers = aud;

            return View("~/Views/Admin/UserRoles.cshtml", uvm);
        }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class EmpUser
    {
        public int Id { get; set; } // AppUser.Id
        public int EmployeeId { get; set; }
        public int RoleId { get; set; }
    }

    public class UserMgtVm : BaseVm
    {
        public List<SelectListItem> CurrentEmployees { get; set; }
        public Dictionary<string, EmpUser> EmpUsers { get; set; }
    }
}