﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class PositionController : BaseMvcController
    {
        public ActionResult Index(PositionQuery q)
        {
            try
            {
                List<PositionRow> pl = db.GetByProc<PositionRow>("GetPositionList", parameters:
                    new
                    {
                        LanId = ui.LanId,
                        ClassCode = q.ClassCode,
                        CostCenter = q.CostCenter,
                        IsVacant = q.IsVacant
                    });
                var avm = new PositionListVm
                {
                    Positions = pl,
                    CurrentUser = ui
                };
//                return View(pl);
                return View(avm);
            }
            catch (Exception xcp)
            {
                // TODO: log error
                return xcp.Message == "Access Denied." ? View("AuthError") : View("Error");
            }
        }

        // TODO: replace calls to ~/Position/[id] with ~/Spa#/Position:id (handle routing in SPA)
        public ActionResult Detail(int id)
        {
            // this redirect is only here in case anything has been missed.  (correct path should be in current views)
            return new RedirectResult("~/spa#/position/" + id);
//            try
//            {
//                Position position = db.GetSingleByProc<Position>("GetPositionDetail", parameters: new { PosId = id, LanId = ui.LanId });
//                var pvm = new PositionVm(position, db)
//                {
//                    CurrentUser = ui
//                };
////                return View(pvm);
//                return View("NgPosDetail", pvm);
////                return View("~/Views/Spa/Index.cshtml", pvm);
//            }
//            catch (Exception xcp)
//            {
//                // TODO: log error
//                return xcp.Message == "Access Denied." ? View("AuthError") : View("Error");
//            }
        }
    }

    public class PositionQuery
    {
        public string ClassCode { get; set; }
        public int? CostCenter { get; set; }
        public bool? IsVacant { get; set; }
    }
}

namespace HrAdmin2.Models
{
    // moved this to Models\ViewModels, moved namespace too:
    //public class BaseVm
    //{
    //    public CurrentUserInfo CurrentUser { get; set; }
    //}

    public class PositionListVm : BaseVm
    {
        public List<PositionRow> Positions { get; set; }
    }

    public class PositionRow : ModelBase
    {
        public string OuName { get; set; }
        public int CostCenter { get; set; }
        public string ClassCode { get; set; }
        public string JobTitle { get; set; }
        public string WorkingTitle { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        // TODO: add supervisor name, title
        public int? SupervisorPositionId { get; set; }
        public int? SupervisorEmpId { get; set; }
    }
}