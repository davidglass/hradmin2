﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    [BaseMvcAuthorize]
    public class ApprovalController : BaseMvcController
    {

        private HrAdminDbContext db = new HrAdminDbContext();

        //
        // GET: /Approval/

        public ActionResult Detail(int id)
        {

            // this gives HR, Admin roles agency-wide span:
            var ac = db.GetSingleByProc<AccessCheck>("PosIsInSpanOfControl", new { PositionId = id, LanId = ui.LanId });
            if (!ac.IsSub)
            {
                return View("AuthError");
            }

            var avm = new ApprovalVm(id, db) { CurrentUser = ui };
            //return View(avm);
            return View("~/Views/Approval/DetailNg.cshtml", avm);
            
        }

        public ActionResult GetPartialTemplate()
        {
            return PartialView("~/Views/Approval/ApprovalPartial.cshtml");
        }

        public ActionResult Index()
        {
            try
            {
                var emp = (db.GetByProc<AppUser>("GetAppUser", new { lanId = ui.LanId })).First();
                var appointment = db.GetSingleByProc<Appointment>("GetPositionIdOfEmployee", new { employeeId = emp.EmployeeId });
                //if (pos.Id == 0) return View("AuthError");
                return RedirectToAction("Detail", "Approval", new { id = appointment.PositionId });                
            }
            catch
            {
                return View("AuthError");
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}
