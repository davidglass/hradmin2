﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
//    public class Search2Controller : Controller
    public class Search2Controller : BaseMvcController
    {
        //private Models.HrAdminDbContext db = new Models.HrAdminDbContext();
        //
        // GET: /Search2/

        public ActionResult Index(SearchParams sp)
        {
            if (sp.q == null)
            {
                // no query = empty result
                return View("NgSearch",
                    new SearchResultVm
                    {
                        CurrentUser = ui,
                        Results = new List<SearchResult>(),
                        Params = sp
                    }
                );
            }
            sp.e = (sp.e == null) ? "" : sp.e;

            List<SearchResult> sr = db.GetByProc<SearchResult>("Search", parameters: new {
                Query = sp.q,
                IncludeHistory = sp.IncludeHistory,
                IncludeAbolished = sp.IncludeAbolished
            });
            var vm = new SearchResultVm
            {
                CurrentUser = ui,
                Results = sr,
                Params = sp
            };
//            return View(SearchResult);
//            return View(vm);
            return View("NgSearch", vm);
        }

    }
}

namespace HrAdmin2.Models
{
    // *NOTE*, Id from ModelBase here is mapped to Employee.Id (unfortunately):
    public class SearchResult : Models.ModelBase
    {
        public bool IsAbolished { get; set; }
        public string FirstName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string LastName { get; set; }
        public int PositionId { get; set; }
        public int OrgUnitId { get; set; }
        public string OuDesc { get; set; }
        public string Title { get; set; }
    }

    public class SearchParams
    {
        public string q { get; set; } // Query
        public string e { get; set; } // Entity
        public string IncludeAbolished { get; set; }
        public string IncludeHistory { get; set; }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class SearchResultVm : BaseVm
    {
        public List<SearchResult> Results { get; set; }
        public SearchParams Params { get; set; }
    }
}