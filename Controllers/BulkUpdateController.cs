﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels; // moved BaseVm here

using BaseVm = HrAdmin2.Models.ViewModels.BaseVm; // remove this after replacing all Models namespace refs

namespace HrAdmin2.Controllers
{
    public class BulkUpdateController : BaseMvcController
    {
        public ActionResult Index()
        {
            var vm = new BulkUpdateVm {
                CurrentUser = ui,
                PosSelection = db.GetByProc<SelectListItem>("GetPositionSelection", new {}),
                SupervisorEmpSelection = db.GetByProc<SelectListItem>("GetSupervisorEmpSelection", new {})
            };
            vm.SupervisorEmpSelection.Insert(0, new SelectListItem { Value = "", Text = " - select - " });

            return View("~/Views/Admin/BulkUpdate.cshtml", vm);
        }

    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class BulkUpdateVm : BaseVm
    {
        public List<SelectListItem> PosSelection { get; set; }
        public List<SelectListItem> SupervisorEmpSelection { get; set; }
    }
}
