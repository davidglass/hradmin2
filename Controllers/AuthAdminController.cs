﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Http; // for enumerating ApiControllers...
using HrAdmin2.Models;

namespace HrAdmin2.Controllers
{
    public class AuthAdminController : Controller
    {
        private HrAdminDbContext db = new HrAdminDbContext();

        //
        // GET: /AuthAdmin/

        public ActionResult Index()
        {
            var types = System.Reflection.Assembly.GetExecutingAssembly().ExportedTypes;
            var controllers = new Controllers {
                                  Mvc = (IEnumerable<string>)types.Where(type => type.IsSubclassOf(typeof(Controller))).Select(type => type.Name.Substring(0, type.Name.Length - 10)), // truncate 'Controller'
                                  Api = (IEnumerable<string>)types.Where(type => type.IsSubclassOf(typeof(ApiController))).Select(type => type.Name.Substring(0, type.Name.Length - 10))
                              };
            var rplist = db.GetByProc<RolePermission>("GetAppRolePermissions", new { });
            var arpd = new Dictionary<string, List<RolePermission>>();
            foreach (string k in controllers.Mvc)
            {
                arpd[k] = new List<RolePermission>();
            }

            foreach (RolePermission rp in rplist)
            {
                arpd[rp.ControllerName].Add(rp);
            }

            controllers.AppRolePerm = arpd;

            return View(controllers);
        }
    }

    public class Controllers
    {
        public IEnumerable<string> Api { get; set; }
        public IEnumerable<string> Mvc { get; set; }
        public Dictionary<string, List<RolePermission>> AppRolePerm { get; set; }
    }

    public class RolePermission
    {
        public string ControllerName { get; set; } // for attaching to named controller enumeration
        public int ControllerId { get; set; }
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public bool CanCreate { get; set; }
        public bool CanRead { get; set; }
        public bool CanUpdate { get; set; }
        public bool CanDelete { get; set; }
    }
}
