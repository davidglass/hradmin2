﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
    public class NgPartialController : BaseMvcController
    {
        //
        // GET: /NgPartial/PartialName

        // TODO: consider caching / expiration strategy for Angular partials.
        // Currently Angular may cache partials too aggressively.
        public ActionResult Template(string pview)
        {
            // TODO: stop merging variable data (e.g. CurrentUser) into these.
            // model data here should only be for generating UI (e.g. dropdown lists).
            return PartialView(String.Format("~/Views/NgPartial/{0}.cshtml", pview), GetViewModelForPartial(pview));
        }

        private BaseVm GetViewModelForPartial(string pview)
        {
            switch (pview)
            {
                case "EmployeeInfo":
                    return null;
                    //return new BaseVm {
                    //    CurrentUser = ui
                    //}; // empty EmpVm for now...

                case "PositionDescription":
                    var pdvm = new PdDetailVm
                    {
                        CurrentUser = ui,
                        JobClasses = db.GetByProc<SelectListItem>("GetJobClassSelection", new {})
                    };
                    // prepend blank option:
                    pdvm.JobClasses.Insert(0, new SelectListItem() { Text = " - select - ", Value = "" });
                    return pdvm;

                //NOTE, PdList, Position, and PositionPdList are now pure HTML, need no server controller...
                //case "PositionPdList":
                //    // TODO: investigate; returning BaseVms like this may not really be necessary.
                //    // $rootScope.vm should preserve CurrentUser...
                //    return new BaseVm
                //    {
                //        CurrentUser = ui
                //    };
                //case "PdList":
                //    // *NOTE*, this is an alternative way to embed template and results into a single request.  Typically more appropriate to get data results from separate API call.
                //    return new PositionDescListVm
                //    {
                //          PositionRows = db.GetByProc<PositionDescRow>("GetPositionListWithPd", new {})
                //    };

                //case "Position":
                //    // *NOTE*, Position content is fetched from Api/PositionController, merged with template.
                //    return null;
                //    //return new BaseVm
                //    //{
                //    //    CurrentUser = ui
                //    //};

                default:
                    return new BaseVm
                    {
                        CurrentUser = ui
                    };
            }
        }
    }
}

namespace HrAdmin2.Models
{
    public class PositionDescRow {
        public int PositionId { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string EffectiveDateString { get; set; }
        public int? PdId { get; set; }
        public string WorkingTitle { get; set; }
        public string JobMatch { get; set; }
        public string JobClassLeg { get; set; }
        public string ProposedJobClassLeg { get; set; }
        public string IncumbentLast { get; set; }
        public string IncumbentFirst { get; set; }
        public string PositionType { get; set; }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    // *NOTE*, this is only for providing server-generated form components (e.g. drop-down lists, etc.):
    public class PdDetailVm : BaseVm
    {
        public List<SelectListItem> JobClasses { get; set; }
    }

    public class PositionDescListVm : BaseVm
    {
        public List<PositionDescRow> PositionRows { get; set; }
    }
}