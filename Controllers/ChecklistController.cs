﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using AuthFilter;

namespace HrAdmin2.Controllers
{
    public class ChecklistController : BaseMvcController
    {

        public ActionResult Index()
        {
            return View(new ChecklistsVm
            {
                CurrentUser = ui,
                Checklists = db.GetByProc<Checklist>("GetChecklists", new { })
            });
        }

        public ActionResult Detail(int id)
        {
            try
            {
                var emplist = db.GetByProc<SelectListItem>("GetCurrentEmpSelection", parameters: new { });
                emplist.Insert(0, new SelectListItem()
                {
                    Text = " - select - ",
                    Value = "",
                    Selected = true
                });

                Checklist checklist = db.GetSingleByProc<Checklist>("GetChecklistDetail", parameters: new { ParId = id, LanId = ui.LanId });
                var ckvm = new ChecklistVm

                {
                    CurrentUser = ui,
                    Empchecklist = checklist,
                    CurrentEmp = emplist,
                    CCL = new List<string>()
                    //EmailCC = new List<String>()
                };
                return View(ckvm);
            }
            catch (Exception xcp)
            {
                // TODO: log error
                return xcp.Message == "Access Denied." ? View("AuthError") : View("Error");
            }
        }

        public ActionResult EmailPreview(int id)
        {
            Checklist checklist = db.GetSingleByProc<Checklist>("GetChecklistDetail", parameters: new { ParId = id, LanId = ui.LanId });
            var ckvm = new ChecklistVm

            {
                CurrentUser = ui,
                Empchecklist = checklist,
            };
            return View("EmailTemplate", ckvm); // Views\Checklist\EmailTemplate.cshtml (explicit)
        }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class ChecklistsVm : BaseVm
    {
        public List<Checklist> Checklists { get; set; }
        


    }

    public class ChecklistVm : BaseVm
    {
        public Checklist Empchecklist { get; set; }
        public List<SelectListItem> CurrentEmp { get; set; }
        public string ViewMode { get; set; }
        public List<string> CCL { get; set; }
       
    }

    public class ChecklistCreateLink : BaseVm
    {
        public int ParId { get; set; }
        public string Approot { get; set; }
     }

}

namespace HrAdmin2.Models
{
    public class Checklist : ModelBase
    {

        public int ParId { get; set; }
        public string ParType { get; set; }
        public string Name { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string Supervisor { get; set; }
        public int CostCenter1 { get; set; }
        public string PositionNumber { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public string Orgunit { get; set; }
        public string OrgUnitShortName { get; set; }
        public string Cubicle { get; set; }
        public string Comments { get; set; }
        public int? UserIdType { get; set; }
        public int SimilarTo { get; set; }
        public string SimilarToLanID { get; set; }
        public bool ComputerEquip { get; set; }
        public bool StateDevice { get; set; }
        public bool VPNToken { get; set; }
        public bool ActiveSync { get; set; }
        public bool Broadband { get; set; }
        public bool Telecom { get; set; }
        public bool Scan { get; set; }
        public bool BuildingBadge { get; set; }
        public bool SDCBadge { get; set; }
        public bool Keys { get; set; }
        public bool CostCenter { get; set; }
        public bool NamePlate { get; set; }
        public bool BusinessCards { get; set; }
        public DateTime? UpdateDt { get; set; }
        public string UpdateBy { get; set; }
        public int CkEmpId { get; set; }
        public bool UpdateADAccount { get; set; }
        public string CubicleTo { get; set; }
    }

    public class ChecklistEmail
    {
        public int Id { get; set; }
        public int ParId { get; set; }
        public string HtmlBody { get; set; }
        public DateTime TimeSent { get; set; }
        public string SentTo { get; set; }
        public string SentBcc { get; set; }
        public string SentFrom { get; set; }
        //public string SentCC { get; set; }
     }

    public class ChecklistEmailCreated
    {
        public int Id { get; set; }
        public int ParId { get; set; }
        public string HtmlBody { get; set; }
        public DateTime TimeSent { get; set; }
        public string SentTo { get; set; }
        public string SentBcc { get; set; }
        public string SentFrom { get; set; }
        //public string SentCC { get; set; }
    }



    [Table("Checklist")]
    public class ChecklistTable

    {
      
         [Key]
        [DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int ParId { get; set; }
        public string Cubicle { get; set; }
        public string Comments { get; set; }
        public int? UserIdType { get; set; }
        public int SimilarTo { get; set; }
        //public string SimilarToLanID { get; set; }
        public bool ComputerEquip { get; set; }
        public bool StateDevice { get; set; }
        public bool VPNToken { get; set; }
        public bool ActiveSync { get; set; }
        public bool Broadband { get; set; }
        public bool Telecom { get; set; }
        public bool Scan { get; set; }
        public bool BuildingBadge { get; set; }
        public bool SDCBadge { get; set; }
        public bool Keys { get; set; }
        public bool CostCenter { get; set; }
        public bool NamePlate { get; set; }
        public bool BusinessCards { get; set; }
        public DateTime? UpdateDt { get; set; }
        public int UpdateBy { get; set; }
        public int CkEmpId { get; set; }
        public bool UpdateADAccount { get; set; }
        public string CubicleTo { get; set; }
    }

    
}

