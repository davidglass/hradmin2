﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models;
using System.Net.Http;

namespace HrAdmin2.Controllers
{
    public class SearchController : Controller
    {
        private HrAdminDbContext db = new HrAdminDbContext();

        //
        // GET: /Search/[?q=querystring]
        public ActionResult Index()
        {
            // q param triggers search:
            if (String.IsNullOrEmpty(Request.QueryString["q"])) {
                return View();
            }
            else {
                return View(db.GetByProc<PositionAppointment>("GetPositionAppointments",
                    parameters: new { q=Request.QueryString["q"] }
                    ));
            }
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}