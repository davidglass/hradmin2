﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

using HrAdmin2.Models;
using HrAdmin2.Models.ViewModels;
using AuthFilter;

namespace HrAdmin2.Controllers
{
    //[BaseMvcAuthorize]
    public class PositionHierarchyController : BaseMvcController
    {
        public ActionResult Index()
        {
            // if invoked without id, default to Director (5202) as root
            var posTree = db.GetByProc<PositionHierarchy>("GetReportingStructure", new { LanId = ui.LanId, RootPosId = 5202 });
            var posTreeVm = new PositionHierarchyVm(posTree)
            {
                CurrentUser = ui
            };
            return View("ServerIndex", posTreeVm);
        }

        //
        // GET: /PositionHierarchy/Details/5

        public ActionResult Details(int id)
        {
            return View();
        }

    }
}
