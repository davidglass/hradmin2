﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using HrAdmin2.Models.ViewModels;

namespace HrAdmin2.Controllers
{
    public class JobClassController : BaseMvcController
    {
        public ActionResult Index()
        {
            var jvm = new JobClassListVm
            {
                JobClasses = db.GetByProc<JobClassRow>("GetJobClasses", new { }),
                CurrentUser = ui
            };
            return View("~/Views/Admin/JobClass.cshtml", jvm);
        }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class JobClassListVm : BaseVm
    {
        public List<JobClassRow> JobClasses { get; set; }
    }

    public class JobClassRow : ModelBase
    {
        public int HrmsJobId { get; set; }
        public string JobTitle { get; set; }
        public string LegacyCode { get; set; }
//        public int FilledPositions { get; set; }
        public int TotalPositions { get; set; }
        public int Vacancies { get; set; }
        public bool IsActive { get; set; }
    }
}