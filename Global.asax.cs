﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Configuration;
using NLog;

namespace HrAdmin2
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        protected static Logger logger = LogManager.GetCurrentClassLogger();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Application["connstr"] = WebConfigurationManager.ConnectionStrings["HrAdminDbContext"].ConnectionString;
            //Application["appname"] = WebConfigurationManager.AppSettings["AppName"];

            // per 
            // http://patrickdesjardins.com/blog/the-model-backing-the-context-has-changed-since-the-database-was-created-ef4-3
            System.Data.Entity.Database.SetInitializer<Models.HrAdminDbContext>(null);

            // added this to handle PUT, DELETE overrides...
            // see http://www.asp.net/web-api/overview/working-with-http/http-message-handlers
            GlobalConfiguration.Configuration.MessageHandlers.Add(new Controllers.Api.XHttpMethodDelegatingHandler());

            // http://social.msdn.microsoft.com/Forums/vstudio/en-US/a5adf07b-e622-4a12-872d-40c753417645/web-api-error-the-objectcontent1-type-failed-to-serialize-the-response-body-for-content?forum=wcf
            // this produces ugly $values:
            // http://stackoverflow.com/questions/17560136/entity-framework-5-with-web-api
            //GlobalConfiguration.Configuration.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore;
            // trying to avoid TZ-based datetime shift (from midnight to 7/8 am) -DG:
            // doesn't work since it subtracts an hour for Daylight Savings dates.
            //var json = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
            //json.SerializerSettings.DateTimeZoneHandling = Newtonsoft.Json.DateTimeZoneHandling.Local;
        }

        // this 
        protected void Application_Error()
        {
            var xcp = Server.GetLastError();
            var oxcp = GetOriginalException(xcp);
            var lei = new LogEventInfo
            {
                Level = LogLevel.Error,
                Exception = oxcp,
                //                    Message = "Error occurred on PostEmail.  Sent Email object: " + Json.Encode(sent),
                Message = oxcp.Message + " in " + this.GetType().FullName + ": " + System.Reflection.MethodBase.GetCurrentMethod().Name,
                TimeStamp = DateTime.Now,
                LoggerName = logger.Name
            };
            // enumerate appSettings from web.config (TODO: use StringBuilder instead):
            var ws = WebConfigurationManager.AppSettings;
            foreach (var k in ws.AllKeys)
            {
                lei.Message += "<br/>" + k + ": " + ws[k];
            }
            lei.SetStackTrace(new System.Diagnostics.StackTrace(oxcp), 0);
            try
            {
                logger.Log(lei);
            }
            catch (Exception xcp2)
            {
                throw new Exception("Error handling failed.", xcp2);
                // ignore NLog logging error?
            }
            // todo : UnhandledError route, controller
//            Response.RedirectToRoute("
            //return new HttpResponseMessage()
            //{
            //    StatusCode = HttpStatusCode.InternalServerError,
            //    Content = new StringContent(Json.Encode(
            //        new
            //        {
            //            error = "server error: " + oxcp.Message
            //        }))
            //};
        }

        private Exception GetOriginalException(Exception xcp)
        {
            return (xcp.InnerException == null) ? xcp : GetOriginalException(xcp.InnerException);
        }

    }
}