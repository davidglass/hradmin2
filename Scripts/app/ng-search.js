﻿angular.module('search', [])
.controller('SearchController', function ($http, $scope, $timeout) {
    // vm is server-generated vm, may include ValErr, Par, etc.
    $scope.vm = vm; // vm global is populated in .cshtml template
    $scope.sortField = 'LastName'; // default sort field
    $scope.desc = false;
    $scope.sortOn = function (f) {
        //*note* Angular can take array of sort fields, TODO: manage this to enable multi-sort (splice out if found, then push)
        // (currently only single sort field is supported...)
        $scope.desc = $scope.sortField == f ? !$scope.desc : false; // default to ascending unless second click, then reverse.
        $scope.sortField = f;
    }
});