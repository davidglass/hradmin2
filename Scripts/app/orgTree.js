﻿var shiftPressed = false;
//var editMode = 'read'; // unused now, I think.
var cmOpening = false;

//var empVm = ko.observable({
//    //ApptHistory: [],
//    //PrNr: 99999999
//    FirstName: 'John',
//    LastName: 'Doe',
//    HomeAddress: {
//        StreeLine1: 'street line 1'
//    }
//});

if (crumbJson.length > 0) {
    ko.applyBindings(crumbJson, $('#crumbs')[0]);
}

function initUi() {
    /*
    $('#EffectiveDate').datepicker();
    $('#EndDate').datepicker();
    $('#DOB').datepicker();
    $('#MsSince').datepicker();
    $('#AnniversaryDate').datepicker();
    $('#ApptDate').datepicker();
    $('#HireDate').datepicker();
    $('#PersHolEligDate').datepicker();
    $('#PersLvEligDate').datepicker();
    $('#PriorPidDate').datepicker();
    $('#SeniorityDate').datepicker();
    $('#TsrLvEligDate').datepicker();
    $('#UnbrokenSvcDate').datepicker();
    $('#VacLvFrozenDate').datepicker();
*/

    $('#org-accord').accordion({
        active: false,
        collapsible: true,
        heightStyle: "content",
        active: 0
    });

    $('#appt-accordion').accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });
    $('#pos-accord').accordion({
        active: false,
        collapsible: true,
        heightStyle: "content"
    });

    //$('#ToggleAccordion').click(function (evt) {
    //    if ($('#pos-accord').hasClass('ui-accordion')) { // is accordion
    //        $(evt.target).text('collapse');
    //        $('#pos-accord').accordion('destroy');
    //    }
    //    else {
    //        $(evt.target).text('expand all');
    //        $('#pos-accord').accordion({
    //            active: 0, // expand panel 0 by default.
    //            animated: 'slide',
    //            collapsible: true,
    //            heightStyle: 'content'
    //        });
    //    }
    //}).text('expand all');

    //$('#SaveRequest').click(function () { saveActionRequest(); });
    // getActionReasons defined in actionRequest.js
    //$('#ActionType').change(function () { getActionReasons($(this).val()) });
    /* removing tabs...
    $("#tabs").tabs({
        collapsible: false,
        //        selected: -1, // breaking change in UI 1.10, replaced with "active"!!!
        active: -1,
        // NOTE, show event is invalid in UI 1.9 (this is 1.8):
        // NOTE2: this must use show event, *not* select (or accordion height fails)
        //show: function (evt, ui) { UI1.10 replaces 1.8 "show" with "activate"
        activate: function (evt, ui) {
            if (ui.index == 2) { // Employee tab selected; render empty accordion
                //empVm({
                //    FirstName: 'Joe',
                //    LastName: 'Blow'
                //}); // clear employee ViewModel...TODO: pull blank VM (id=0) from server.
                //        ko.applyBindings(empVm, $('#detail-emp')[0]);
                //               $('#appt-accordion').accordion({ active: 0 });
//                $('#appt-accordion').accordion("resize"); // resize broken in 1.10 - now "refresh"
                $('#appt-accordion').accordion("refresh"); // resize broken in 1.10 - now "refresh"
            };
            if (ui.index == 1) { // Position tab selected; render empty accordion
//                $('#pos-accord').accordion("resize");
                $('#pos-accord').accordion("refresh");
            }
        }



        //activate: function (e, ui) {
        //    alert('before activate!');
        //}
    });
   */
    $('#splitter > div:first').resizable({
        handles: 'e',
        minWidth: '10',
        maxWidth: '600',
        resize: function () {
            var remainingSpace = $(this).parent().width() - $(this).outerWidth();
            var divTwo = $(this).next();
            var divTwoWidth = remainingSpace - (divTwo.outerWidth() - divTwo.width());
            divTwo.css('width', divTwoWidth + 'px');
        }
    });
}

$(function () {
    // TODO: move this stuff into initUi func in separate app.js
    initUi();
    buildJsTree(treeJson);
});

function buildJsTree(d) {

    var theTree = $('#jsTree').jstree({
        contextmenu: {
            items: createContextMenuItems,
            select_node: true,
            show_at_node: false
        },
        core: {
            animation: 80 // ms, set to 0 to disable
        },
        json_data: { data: d },
        plugins: [
            //"contextmenu",
            "json_data",
            "themes",
            "ui"
        ],
        ui: {
            initially_select: [rootId]
        }
    });
    setupEventBindings(theTree);
    // shiftPressed is for "super-expand/collapse"...
    $(document).keydown(function (e) {
        shiftPressed = e.shiftKey; // track shift state for expand(all)/collapse(all) 
    });
    $(document).keyup(function (e) {
        shiftPressed = e.shiftKey; // track shift state for expand(all)/collapse(all) 
    });
    // cache selected root:
    $('#jsTree').data('lastSelected', $('#jsTree').jstree('get_selected'));
}

function doAction(actionCode, itemType) {
    if (actionCode == '') return false;
    //    alert('doing action ' + actionCode);
    var n = $('#jsTree').data('lastSelected'); // most recently selected tree node.
    //    if (itemType == 'Pos') {
    //alert('node id=' + n.attr('id'));
    switch (actionCode) {
        //case 'jump': alert('jumping to position #' + $('#jsTree').data('lastSelected').attr('id'));
        //            case 'jump': window.open(appRoot + 'position/' + (emp ? n.data('IncumbentId') : n.attr('id'))
            //case 'jump': window.open(appRoot + (itemType == 'Pos' ? 'position/' : 'employee/')
        //+ (itemType == 'Pos' ? n.attr('id') : n.data('IncumbentId')), '_self');
        case 'jump': window.open(appRoot + (itemType == 'Pos' ? 'position/' : itemType == 'OU' ? 'orgunit/' : 'employee/')
            + (itemType == 'Pos' || itemType == 'OU' ? n.attr('id') : n.data('IncumbentId')), '_self');
            return false;
        case 'createPosition':
            createPar({
                PosOrgUnitId: n.attr('id'),
                ActionCode: 'P0' // Create Position
            });
            break;
        case 'U3-81': createPar({ // transfer out
            PositionId: n.attr('id'),
            EmployeeId: (itemType == 'Emp') ? n.data('IncumbentId') : null,
            ActionCode: 'U3',
            ActionReasonId: 81, // ApptChange_U3 = 58
            IsTransferOut: true
        });
            break;
        case 'U3-2': createPar({ // schedule change 
            PositionId: n.attr('id'),
            EmployeeId: (itemType == 'Emp') ? n.data('IncumbentId') : null,
            ActionCode: 'U3',
            ActionReasonId: 2 // ApptChange_U3 = 08
        });
            break;
        default: createPar({
            PositionId: n.attr('id'),
            EmployeeId: (itemType == 'Emp') ? n.data('IncumbentId') : null,
            ActionCode: actionCode
        });
    }
//    }
}

function performAction(selectorId) {
    var n = $('#jsTree').data('lastSelected'); // most recently selected tree node.
/*    if (selectorId == 'OrgUnitAction') {
        doAction($('#' + selectorId).val(), 'OU');
    }*/
    doAction($('#' + selectorId).val(), (selectorId == 'PosAction') ? 'Pos'
        : (selectorId == 'OrgUnitAction') ? 'OU'
        : 'Emp'
    );
}

function createContextMenuItems(clickedItem) { // this is the actual clicked item (e.g. could be emp or position link)
    var cc = $(clickedItem.context);
    var emp = cc.hasClass('employee');
    if (emp) {
        // employee context
        //        $('#tabs').tabs('option', 'selected', 2); // UI 1.8 style
        // removing tabs -DG
        //$('#tabs').tabs('option', 'active', 2); // UI 1.10 style
    }
    var n = cc.parent('li');
    var nt = n.data('nt'); // NodeType
    var items = {
        ccp: false, // cut, copy, paste
        create: false,
        rename: false,
        remove: false, // delete
        heading: {
            _class: "menu-heading",
            label: (emp ? 'Employee' : n.data('nt')) + ' Actions:',
            separator_after: true
        }
    };

    items['jump'] = {
        //icon: siteRoot + "Images/icons/go-jump.png",
        label: nt == 'OrgUnit' ? " jump here" : " detail page",
//        label: " jump here", // standardizing label
        action: function () {
            //window.open(appRoot + (emp ? 'employee' : nt) + '/' + (emp ? n.data('IncumbentId') : n.attr('id')),
            //    nt == 'OrgUnit' ? '_self' : '_blank')
            window.open(appRoot + (emp ? 'employee' : nt) + '/' + (emp ? n.data('IncumbentId') : n.attr('id')),
                '_self');
        }
    };

    if (nt == 'OrgUnit') {
        items['createUnit'] = {
            label: 'create subunit',
            _disabled: true,
            action: function () {
                alert('create subunit');
            }
        };
        items['createPos'] = {
            label: 'create position',
            action: function () {
                createPar({
                    PosOrgUnitId: n.attr('id'),
            //        ActionType: 'P0' // Create Position
                    ActionCode: 'P0' // Create Position
            });
            }
        };
    }
    if (nt == 'Position' && !emp) {
        if (n.data('vac')) {
            items['abolish'] = {
                label: ' abolish',
                action: function () {
                    createPar({
                        PositionId: n.attr('id'),
                //        ActionType: 'P2' // Abolish
                        ActionCode: 'P2' // Abolish
                });
                }
            };
        }
        items['fill-new'] = {
            label: ' fill: New Hire',
            action: function () {
                //editMode = 'create';
                // hide tabs-DG
                //$('#tabs').tabs('option', 'selected', 2);
                $('#statusmessage').text('creating request...'); // TODO: async spinner in statusbar
                // open panel 0 unless it is already active:
                var activepanel = $('#appt-accordion').accordion('option', 'active');
                if (activepanel !== 0) { // !== needed to exclude match on false
                    $('#appt-accordion').accordion('option', 'active', 0);
                }
                // moved actiontype selector to async callback
                // must explicitly fire change event after programmatic selection:
                // http://stackoverflow.com/questions/4672505/why-does-the-jquery-change-event-not-trigger-when-i-set-the-value-of-a-select-us
                // $('#ActionType').val('U0').change(); // select New Hire action.  TODO: select default ActionReason based on Position Personnel Sub-area
                //getNewHireRequestViewModel(n.attr('id'));
                //                createPar({ PositionId: n.attr('id'), ActionType: 'U0' });
                createPar({ PositionId: n.attr('id'), ActionCode: 'U0' });
            }
        };
        items['fill-current'] = {
            label: ' fill: Current Employee',
            action: function () {
                //editMode = 'create';
                // TODO: move tab selector to node selected event:
                // hide tabs-DG
                //$('#tabs').tabs('option', 'selected', 2); // 1-index.

                // commented this out, seems obsolete?
                // open panel 0 unless it is already active:
                //var activepanel = $('#appt-accordion').accordion('option', 'active');
                //if (activepanel !== 0) { // !== needed to exclude match on false
                //    $('#appt-accordion').accordion('option', 'active', 0);
                //}

                // moved actiontype selector to async callback
                // must explicitly fire change event after programmatic selection:
                // http://stackoverflow.com/questions/4672505/why-does-the-jquery-change-event-not-trigger-when-i-set-the-value-of-a-select-us
                // $('#ActionType').val('U0').change(); // select New Hire action.  TODO: select default ActionReason based on Position Personnel Sub-area
                //getNewHireRequestViewModel(n.attr('id'));
                //                createPar({ PositionId: n.attr('id'), ActionType: 'U3' });
                createPar({ PositionId: n.attr('id'), ActionCode: 'U3' });
//                createPar(n.attr('id'), 'U3');
            }
        };
        items['fill-rehire'] = {
            label: ' fill: Rehire',
            action: function () {
                // TODO: remove EmployeeId prop from this?  seems superfluous.
                //                createPar({ EmployeeId: n.data('IncumbentId'), PositionId: n.attr('id'), ActionType: 'U6' });
                createPar({ EmployeeId: n.data('IncumbentId'), PositionId: n.attr('id'), ActionCode: 'U6' });
            }
        };

        items['modify'] = {
            label: ' modify',
            action: function () {
                createPar({
                    PositionId: n.attr('id'),
            //        ActionType: 'P1'
                    ActionCode: 'P1'
            });
            }
        };
    }

    if (emp) {
        items['change-status'] = {
            label: ' change status',
            action: function () {
                createPar({
                    EmployeeId: n.data('IncumbentId'),
                    PositionId: n.attr('id'),
            //        ActionType: 'UJ' // Status Change
                    ActionCode: 'UJ' // Status Change
            });
            }
        };
        items['change-sched'] = {
            label: ' change work schedule',
            action: function () {
                createPar({
                    EmployeeId: n.data('IncumbentId'),
                    PositionId: n.attr('id'),
//                    ActionType: 'U3', // Appointment Change
                    ActionCode: 'U3', // Appointment Change
                    ActionReasonId: 2 // Change Work Hours
                });
            }
    };
        items['change-appt'] = {
            label: ' modify appointment',
            action: function () {
//                createPar({ EmployeeId: n.data('IncumbentId'), PositionId: n.attr('id'), ActionType: 'U3' });
                createPar({ EmployeeId: n.data('IncumbentId'), PositionId: n.attr('id'), ActionCode: 'U3' });
            }
        };
        items['loa'] = {
            label: ' schedule LOA',
            action: function () {
                createPar({
                    EmployeeId: n.data('IncumbentId'),
                    PositionId: n.attr('id'),
            //        ActionType: 'U8' // Begin LOA - Active
                    ActionCode: 'U8' // Begin LOA - Active
            });
            }
        };
        items['loaRtn'] = {
            label: ' return from LOA',
            action: function () {
                createPar({
                    EmployeeId: n.data('IncumbentId'),
                    PositionId: n.attr('id'),
            //        ActionType: 'UA' // Begin LOA - Active
                    ActionCode: 'UA' // Begin LOA - Active
            });
            }
        };
        items['separate'] = {
            label: ' separate',
            action: function () {
//                createPar({ EmployeeId: n.data('IncumbentId'), PositionId: n.attr('id'), ActionType: 'U5' });
                createPar({ EmployeeId: n.data('IncumbentId'), PositionId: n.attr('id'), ActionCode: 'U5' });
            }
        };
    }

    return items;
}

function createPar(parData) {
    // TODO: test that parData is not null and is an object...
    $('#ApptStatusMessage').text('creating PAR');
    $('#statusbar').fadeTo(0, 1);
    $('#Spinner').show();

    $('#PosStatusMessage').text('creating PAR');
    $('#statusbar2').fadeTo(0, 1);
    $('#Spinner2').show();

    $.ajax({
        type: 'POST',
        // beforeSend to defeat IE login box on unauthorized header:
        // http://stackoverflow.com/questions/361467/is-there-any-way-to-suppress-the-browsers-login-prompt-on-401-response-when-usi
        //beforeSend: function (xhr) {
            //xhr.setRequestHeader("X-MicrosoftAjax", "Delta=true");
        //},
        url: appRoot + 'api/par',
        data: parData,
        //dataType: "json",
        success: function (d) {
            window.location = appRoot + 'par/' + d.Par.Id;
        },
        error: function (xrq, txtStatus, errThrown) {
            //alert(xrq['responseText']);
            alert(xrq.statusText);
            // TODO: test error handling more thoroughly...
            //$('#Spinner').hide();
            //$('#Spinner2').hide();
            //$('#PosStatusMessage').text('ERROR: ' + xrq.statusText);
            $('#PosStatusMessage').text('ERROR');
            //$('#ApptStatusMessage').text('ERROR: ' + xrq.statusText);
            //alert(txtStatus);
        }
    });
}

function setupEventBindings(theTree) {
    theTree.bind("loaded.jstree", enhanceTree);
    theTree.bind("select_node.jstree", selectNode);
    theTree.bind("hover_node.jstree", hoverNode);
    theTree.bind("close_node.jstree", asyncCollapseNode);
    theTree.bind("open_node.jstree", asyncExpandNode);
}

// TODO: unify enhanceTree / enhanceNode
function enhanceTree() {

    enhanceNode($('#jsTree'));
}

function enhanceNode(n) {
    // TODO: consider more efficient filter / split:
    var ouList =
    n.find('li').filter(function () {
        return $(this).data("nt") == "OrgUnit"
    });
    for (var i = 0; i < ouList.length; i++) {
//        $(ouList[i]).children('a').addClass('orgunit').attr('href', '/orgunit/' + $(ouList[i]).attr('id'))
        $(ouList[i]).children('a').addClass('orgunit').attr('href', appRoot + 'orgunit/' + $(ouList[i]).attr('id'))
            // toggle OU nodes on double-click only:
            .dblclick(function (n) { $('#jsTree').jstree('toggle_node', $(n).parents('li')[0]); })
            // append position count to OU nodes:
            .after('<span> [' + $(ouList[i]).data('pcount') + ']</span>');
    }

    n.find('li').filter(function () {
        return $(this).data("sv")
    }).children('a').addClass('supervisor');

    var vacList =
    n.find('li').filter(function () {
        return $(this).data("vac")
    });
    for (var i = 0; i < vacList.length; i++) {
        var v = $(vacList[i]);
        v.children('a').attr('href', '/position/' + v.attr('id'));
        if (v.data('Incumbent') && v.data('Incumbent').match(/:/)) { // new hire, future start:
            v.append(" <span class='coming-soon'>[" + v.data('Incumbent') + "]</span>").children('a').addClass('vacant');
            //v.append(" <span class='coming-soon'>[" + v.data("Incumbent") + "]</span>");
//            v.append(" <span class='coming-soon'>[" + v.data("Incumbent") + "]</span>").children('a').addClass('coming-soon');
        }
        else {
        v.append(" <span class='vacant'>[VACANT" + (v.data('Incumbent') == null ? '' : " " + v.data("Incumbent")) + "]</span>").children('a').addClass('vacant');
    }
    }

    var posList = n.find('li').filter(function () {
        return ($(this).data("nt") == "Position" && !$(this).data("vac"))
        //            }).children('a').addClass('employee');
        //            }).append("<a href='/employee/" + $(this).data("IncumbentId") + "' />").append($(this).data("Incumbent"));
    });
    for (var i = 0; i < posList.length; i++) {
        var p = $(posList[i]);
        p.children('a').attr('href', appRoot+ 'position/' + p.attr('id'));

        // *NOTE*, clicks on this link will be ignored (swallowed by jsTree).  Can still be opened via context menu though.
        var emplink = $(" <a class='employee' href='" + appRoot + "employee/" + p.data("IncumbentId")
            + "'>[" + p.data("Incumbent") + "]</a>")
        emplink.click(function (evt) {
            //            $('#tabs').tabs('option', 'selected', 2);
            // hide tabs-DG
            //$('#tabs').tabs('option', 'active', 2);
            // must cancelBubble here to prevent jstree.select_node firing...
            // unfortunately, node should be selected, but the selectNode handler will choose Position tab on select event.
            // TODO: set employee flag
//            stopPropagation(evt);
            // select node without propagating event:
            $('#jsTree').data('empnode', true);
            //$('#jsTree').jstree('select_node');
            //return false;
        });
        p.append(emplink);
    }
}

// cross-browser stopPropagation to support IE8:
// http://stackoverflow.com/questions/7596364/whats-the-difference-between-cancelbubble-and-stoppropogation
//function stopPropagation(evt) {
//    if (typeof evt.stopPropagation != "undefined") {
//        evt.stopPropagation();
//    } else {
//        evt.cancelBubble = true;
//    }
//}

// TODO: disable this if unused...
function hoverNode(evt, data) {
    if (data.args != undefined) {
        lastHoveredNode = $(data.rslt.obj[0]);
        //                alert(lastHoveredNode.attr('id'));
    }
}

function getOuDetail(id) {
    $.ajax({
        type: 'GET',
        url: appRoot + 'api/orgunit/' + id + '/detail',
        success: function (d) {
            var ouVm = ko.mapping.fromJS(d);
            ko.applyBindings(ouVm, $('#detail-org')[0]);
//            alert('ou detail: ' + JSON.stringify(d));
        },
        error: function (xrq, txtStatus, errThrown) {
            alert(xrq['responseText']);
            alert(txtStatus);
        }
    });
}

function selectNode(evt, data) {
    // https://code.google.com/p/jstree/issues/detail?id=666
    var n = data.rslt.obj;
    // TODO: distinguish between left-click and context-click for selection, only toggle for left click.
    // NOTE, changed to toggle only on OU double-click. (listener attached during enhanceTree)
    //if (lastHoveredNode != undefined && n != lastHoveredNode && !cmOpening) {
    //    $('#jsTree').jstree('toggle_node', n);
    //}
    var empclick = $('#jsTree').data('empnode');
    $('#jsTree').removeData('empnode');
    if (n.data('nt') == 'OrgUnit') {
        //        $('#detail-org').css({ visibility: 'visible' });
        // DG: commented out to speed page load:
        //getOuDetail(n.attr('Id'));

        var ouVm = {
            Id: n.attr('Id'),
            HrmsOrgUnitId: n.attr('Id'),
            Description: n.data('title'),
            ShortName: '',
            Abbreviation: ''
        };
        ko.applyBindings(ouVm, $('#detail-org')[0]);

        //$('#orgUnitId').text(n.attr('Id'));
        
        //$('#orgUnitDesc').text(n.data('title')); // TODO: change field to generic LabelText property

        $('#detail-org').show(); // TODO: consider fadeIn if possible.

        // new tabless approach hides sibling panels manually:
        $('#detail-pos').hide(); // TODO: consider fadeIn if possible.
        $('#detail-appt').hide(); // TODO: consider fadeIn if possible.
        //$('#orgUnitId').text(n.attr('Id'));
        //$('#hrmsOuId').text(n.attr('Id'));
        //$('#orgUnitDesc').text(n.data('title')); // TODO: change field to generic LabelText property
        // show OrgUnit tab, hide others...
        //$('#tabs').tabs("option", "active", false); // collapse all panels
        // apparently "selected = -1" collapses, instead of active??? version issue?
        //        $('#tabs').tabs("option", "selected", -1); // collapse all panels (yes, selected is deprecated in newer jqui)
        //$('#tabs').tabs("option", "disabled", []); // enable all before selecting new tab (UI 1.8 syntax)
        // hide tabs-DG
        /*
        $('#tabs').tabs("enable"); // enable all before selecting new tab (UI 1.8 syntax)
        //$('#tabs').tabs("option", "selected", 0); // collapse all panels (yes, selected is deprecated in newer jqui)
        $('#tabs').tabs("option", "active", 0); // collapse all panels but first (new syntax)
        // disable position + emp tabs until such pos node is selected:
        $('#tabs').tabs("option", "disabled", [1, 2]);
        */
        $('#jsTree').data('lastSelected', n);

        return;
    }
    else {
        // new tabless design:
        $('#detail-org').hide(); // TODO: consider fadeIn if possible.
        if (!empclick) { // position node selected, show that panel:
            $('#detail-appt').hide(); // TODO: consider fadeIn if possible.
            $('#detail-pos').show(); // TODO: consider fadeIn if possible.
        }
        else {
            $('#detail-pos').hide(); // TODO: consider fadeIn if possible.
            $('#detail-appt').show(); // TODO: consider fadeIn if possible.
        }

        /* removing tab logic...
        //        $('#tabs').tabs("option", "active", 0); // open first panel (todo: select appropriate panel for action)
        //$('#tabs').tabs("option", "disabled", []); // enable all before selecting new tab (UI 1.8 syntax)
        $('#tabs').tabs("enable"); // enable all before selecting new tab (UI 1.8 syntax)
        //$('#detail-pos').show(); // TODO: consider fadeIn if possible.
        //$('#detail-appt').show(); // TODO: consider fadeIn if possible. // ** triggers event, accordion redraw
        //        $('#tabs').tabs("option", "selected", empclick ? 2 : 1); // show second panel...
                $('#tabs').tabs("option", "active", empclick ? 2 : 1); // show second panel...
        if (n.data('vac')) { // position is vacant
            $('#tabs').tabs("option", "disabled", [0, 2]); // disable OU and emp node
        }
        else {
            $('#tabs').tabs("option", "disabled", [0]); // disable OU node (not really necessary)
        }
        //$('#detail-pos').show(); // TODO: consider fadeIn if possible.
        //$('#detail-appt').show(); // TODO: consider fadeIn if possible. // ** triggers event, accordion redraw
        // removed?:
        //$('#JobTitle').text(n.data('title'));

        // switched this back to knockout Id...
        //$('#PosId').text(n.attr('id'));

        $('#orgUnitId').text(n.parents('li').attr('Id'));
        $('#orgUnitDesc').text(n.parents('li').data('title')); // TODO: change field to generic LabelText property
*/
//        $('#OuSelection').val(n.parents('li').attr('id')); // for selecting item from dropdown (in edit mode)
        //$('#orgUnit').text(n.parents('li').data('title')); // for selecting item from dropdown (in edit mode)
        //back to Knockout...
//        $('#classCode').text(n.data('classcode'));
        //$('#empLast').text(n.data('Incumbent')); // now using AJAX call, knockout for emp detail.
        //$('#empFirst').text(n.data('IncumbentFirst'));
        //$('#hrmsPositionId').text(n.data('HrmsPositionId'));
        // TODO: get full PositionViewModel (including Incumbent info), not just CoC...

        // TODO: merge PositionDetail and Incumbents into single call, composite Vm
        //$('#statusmessage').text('loading appointment detail...');
        // getIncumbents is in position.js (now called in series on positiondetail success)
        //getIncumbents(n.attr('id'));
        // default to Personal Data accordion panel on employee click
//        if (empclick) {
//            // default Action to Appt Change (U3):
//            //$('#ActionType').val('U3').change();

//            //var activepanel = $('#appt-accordion').accordion('option', 'active');
//            // default to Appointment panel
////            if (activepanel !== 1) {
//////            if (activepanel !== 0) { // !== needed to exclude match on false
        ////                    // changed default to Action Request panel...
        // moved to getPositionDetail since selects PAR panel if open pars exist:
        //if (!$('#appt-accordion').accordion('option', 'active')) {
        //    $('#appt-accordion').accordion('option', 'active', 0);
        //}

        ////                $('#appt-accordion').accordion('option', 'active', 1);
////            }
//            //$('#appt-accordion').accordion('refresh');
//        }
//        else {
//            // default Position Action to New Hire (U0):
//            $('#ActionType').val('U0').change();
//        }
        //var posActivepanel = $('#pos-accord').accordion('option', 'active');
        //if (posActivepanel !== 0) { // !== needed to exclude match on false (N/A in UI 1.10)
        if ($('#jsTree').data('lastSelected') == undefined || $('#jsTree').data('lastSelected').attr('id') != n.attr('id')) {
            //bypass if node is already selected...
            //if ($('#jsTree').data('lastSelected') != undefined) {
            //if (empclick) {
            //    $('#ApptStatusMessage').text('loading appointment detail...');
            //    $('#statusbar').fadeTo(0, 1);
            //    $('#Spinner').show();
            //}
            //}
            getPositionDetail(n.attr('id'), empclick, n.data('IncumbentId'));
        }
        else if (empclick ||
            // OR clicked secondary position node on multi-filled position:
            (n.data('IncumbentId') != $('#jsTree').data('lastSelected').data('IncumbentId'))
            ) {
            // replace Appointment Vm based on IncumbentId: (need to cache appt array on initial getPositionDetail)
            var tvm = $('body').data('tabsVm');
            // find index of matching appointment:
            for (var i = 0; i < tvm.Appointments().length; i++) {
                // TODO: only replace if different from 
                if (n.data('IncumbentId') != $('#jsTree').data('lastSelected').data('IncumbentId')
                    && n.data('IncumbentId') == tvm.Appointments()[i].Appt.EmpId()) {
                    if (tvm.Appointments()[i].OpenPars().length == 0) {
                        // need placeholder OpenPar to prevent destroying template before applyBindings:
                        tvm.Appointments()[i].OpenPars.push(ko.mapping.fromJS({
                            Id: 0,
                            ActionDescription: '',
                            ActionType: '', // not sure why this was removed?
                            ActionCode: '',
                            EffectiveDate: '2013-01-01'
                        }));
                    }
                    ko.applyBindings(tvm.Appointments()[i], $('#detail-appt')[0]); // new binding
                    break;
                }
            }
        }
        //}
        //if (empclick) {
        //    getEmployee(n.data('IncumbentId'));
        //}
        //else {
        //    // load vacant VM
        //    if (n.data('vac')) {
        //        getEmployee(0);
        //    }
        //}
        // this doesn't take effect until re-render?  TODO: fix that.
        $('#jsTree').data('lastSelected', n);
    }
/*    alert('selected node id =' + n.attr('id')
        + '; nodetype = ' + n.data('nt')
        + '; empid = ' + n.data('IncumbentId'));
        */
}

//function getEmployee(id) {
//    $.ajax({
//        type: 'GET',
//        url: appRoot + 'api/employee/' + id,
//        success: function (d) {
//            // TODO: start with empty ko appt history viewmodel and change contents here
//            // TEMPORARY HACK!
//            //          $('#ApptList').remove('li').append('<li>Started: <span data-bind="text: StartDate"></span></li>');
//            empVm(d);
//            // knockout binding is failing, so setting value explicitly:
////            $('#CountyList').val(d.HomeAddress.CountyId);
//            // need to explicitly re-apply bindings broken by accordion:
////            ko.applyBindings(empVm, $('#detail-emp')[0]);
//            $('#appt-accordion').accordion("resize");
////            $('#appt-accordion').accordion({ active: 0 });
//            //            ko.applyBindings(d, $('#detail-emp')[0]);
//        },
//        error: function (xrq, txtStatus, errThrown) {
//            alert(xrq['responseText']);
//            alert(txtStatus);
//        }
//    });
//}

function asyncCollapseNode(event, data) {
    var all = shiftPressed;
    var node = data.args[0]; // args[0] is origin node...
    var atag = node[0].childNodes[1];
    var jtag = $(atag);
    //jtag.toggleClass('jstree-clicked', false); // need to turn this off?
    // *NOTE* the async-op toggle may happen too quickly to be visible locally.
    jtag.toggleClass('async-op');
    $.ajax({
        type: 'POST',
        url: appRoot + 'api/orgunit/' + node.attr("id"),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Http-Method-Override", "PUT");
        },
        data: {
            action: all ? 'collapse_all' : 'collapse'
        },
        success: function () {
            if (all) {
                // remove all subnodes and revert self to lazy...collapsing all subnodes without firing collapse event = too much work.
                node.children('ul').remove();
                // TODO: move this from attr( to data(
                //node.attr('z', 'true'); // reset to lazy (sic 'true' string)
                node.data('lazy', 'true');
            }
            jtag.toggleClass('async-op');
        },
        error: function (xrq, txtStatus, errThrown) {
            alert(xrq['responseText']);
            alert(txtStatus);
        }
    });
}

function asyncExpandNode(event, data) {
    var node = data.args[0];
    var l = node["length"];
    // ignore expand events triggered by drop onto leaf (in which case node arg contains no nodes)
    // TODO: distinguish between drop onto leaf and lazy-load expand.
    if (l == undefined) {
        return;
    }

    /*  TODO: get lazy-load working on metadata property... */
    // node.attr("lazy") is coming back as "false" string, which evaluates to true!  TODO: fix.
    //            var lazy = node.attr("z");
    var lazy = node.data("lazy");
    //alert('lazy ='+lazy);
    if (lazy || shiftPressed) { // always lazy-load all when shiftPressed...
        //if (lazy == "true" || shiftPressed) { // always lazy-load all when shiftPressed...
        // TODO: don't forget to set node.attr("lazy") to undefined after lazy-load completes...
        if (shiftPressed) {
            node.children('ul').remove();
            asyncLazyLoad(node, '/all');
        }
        else {
            asyncLazyLoad(node, '');
        }
        //                node.removeAttr("z");
        node.removeData("lazy");

        return false;
    }

    // return if node has no childnodes.
    // caused by attempted leaf expansion during drag-drop or programmatic node move.
    // *note*, the new parent probably *should* be expanded, but only *after* the child is added.
    //		if (! node.hasChildNodes()) {
    //			return;
    //		}
    //		// first set text orange to indicate async op:
    var atag = node[0].childNodes[1];
    var jtag = $(atag);
    //jtag.toggleClass('jstree-clicked', false); // need to turn this off?
    // *NOTE* the async-op toggle happens too fast to be visible locally.
    jtag.toggleClass('async-op');
    $.ajax({
        type: 'POST',
        //                url: approot + 'node/' + node.attr("id"),
        //        url: '/api/orgunit/' + node.attr("id"),

        url: appRoot + 'api/orgunit/' + node.attr("id"),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Http-Method-Override", "PUT");
        },
        success: function () {
            jtag.toggleClass('async-op');
            //				jtag.toggleClass('jstree-clicked'); // need to turn this off
        },
        data: {
            action: 'expand'
        }
    });
}

function asyncLazyLoad(n, all) {
    var atag = n[0].childNodes[1];
    var jtag = $(atag);
    jtag.toggleClass('async-op');
    //    var path = '/api/orgunit/' + n.attr("id") + '/subnodes' + all;
    var path = appRoot + 'api/orgunit/' + n.attr("id") + '/subnodes' + all;
    $.ajax({
        //type: 'PUT','GET' 'POST' 'DELETE', etc.
        //            url: '/NodeService/node/' + n.attr("id") + '/subnodes',
        //			url: '/nodes/' + n.attr("id") + '/subnodes',
        //		url: '/Node/SubNodes/' + n.attr("id"), // MVC unReSTful path...
        url: path,
        //                url: approot + 'node/' + n.attr("id") + '/subnodes' + all,
        // *NOTE*, d result is coerced into an array.
        success: function (d) {
            //alert("REST call took " + (new Date().getTime() - start) + " ms.");
            //d["data"] = "<input type='checkbox' onclick=alert('hello')></input> " + d["data"];
            // TODO: return JSON array from server (currently returns root node, as for main data)
            jtag.toggleClass('async-op');
            addJsTreeLastStyle(d);
            //var domList = jst._parse_json(d);
            var domList = $.jstree._reference($('#jsTree'))._parse_json(d);
            n.append(domList);
            // TODO: more granular version of this, only gray completed items that were just lazy-loaded.
            //grayCompletedItems(mytree);
            //styleSubItems(n);
            n.removeData("lazy");
            enhanceNode(n);
        },
        // *NOTE*, Method-Override only works with POST.
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Http-Method-Override", "GET");
        }
    });
}

function addJsTreeLastStyle(lazynodes) {
    lazynodes[lazynodes.length - 1].state += ' jstree-last';
    for (var i=0; i < lazynodes.length; i++) {
        if (lazynodes[i].children.length) {
            addJsTreeLastStyle(lazynodes[i].children);
        }
    }
}

function formatJsonDate(d) {
    if (d == null) { return; }
    // *NOTE*: this is a different flavor of formatJsonDate...
    // TODO: check date format before attempting to parse, move this into util.js
    var fdate = parseDate(d); // convert .NET format
    // allow negation prefix for dates before 1970:
//    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    //var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

// inspired by ISO8601 parser found here:
// http://n8v.enteuxis.org/2010/12/parsing-iso-8601-dates-in-javascript/
function parseDate(s) {
    var re = /(\d{4})\-(\d\d)\-(\d\d)/;
    var ymd = s.match(re);
    return new Date(ymd[1], ymd[2] - 1, ymd[3]);
}

function getPositionDetail(id, empclick, empid) {
    if (empclick) {
        $('#ApptStatusMessage').text('loading appointment detail...');
        $('#statusbar').fadeTo(0, 1);
        $('#Spinner').show();
    }
    else {
        $('#PosStatusMessage').text('loading position detail');
        $('#statusbar2').fadeTo(0, 1);
        $('#Spinner2').show();
    }

    $.ajax({
        type: 'GET',
        url: appRoot + 'api/position/' + id,
        success: function (d) {
            var tabsVm = ko.mapping.fromJS(d);
            $('body').data('tabsVm', tabsVm);
            var appt = {};
            if (d.Appointments.length) {
                for (var i = 0; i < d.Appointments.length; i++) {
                    if (d.Appointments[i].Appt.EmpId == empid) {
                        appt = tabsVm.Appointments()[i];
                        break;
                    }
                }
            }

            if (empclick) {
                $('#statusbar').fadeTo(500, 0, function () {
                    $('#Spinner').hide();
                    $('#ApptStatusMessage').text('');
                });
            }
            else {
                $('#statusbar2').fadeTo(500, 0, function () {
                    $('#Spinner2').hide();
                    $('#PosStatusMessage').text('');
                });
            }
            // TODO: bind to tabsVm (mapped vm from d) as root.
            // now remove any rows after the first (template) row.
            // this is to enable repeatedly re-applying bindings without caching and manually updating viewmodel
            $('#PosParList tr:not(:first)').remove();
            $('#EmpParList tr:not(:first)').remove();

            if (!tabsVm.Position.OpenPars().length) {
                // fake Par placeholder (Id 0 = hidden)
                tabsVm.Position.OpenPars.push(ko.mapping.fromJS({
                    Id: 0,
                    ActionDescription: '',
                    ActionType: '',  //sic
                    EffectiveDate: '2013-01-01'
                }));
                if ($('#pos-accord').hasClass('ui-accordion')) { // is accordion
                    $('#pos-accord').accordion('option', 'active', 0);
                }
            }
                //            if (tabsVm.Position.OpenPars().length) {
            else {
                // pos-accord select PAR panel
                if ($('#pos-accord').hasClass('ui-accordion')) { // is accordion
                    $('#pos-accord').accordion('option', 'active', 2);
                }
            }
            ko.applyBindings(tabsVm.Position, $('#detail-pos')[0]); // new binding

            // TODO: create one tab per appointment for multi-fills
            // skip appts binding for vacant:

            // now need to push fake appointment for vacant pos to allow binding Employee pars:
            if (!tabsVm.Appointments().length) {
                appt =
                ko.mapping.fromJS({
                    Appt: {
                        LastName: 'Last',
                        PreferredName: 'First'
                    },
                    OpenPars: [
                        {
                            Id: 0,
                            ActionDescription: '',
                            ActionType: '',
//                            ActionCode: '',
                            EffectiveDate: '2013-01-01'
                        }
                    ]
                });
            }

            //if (tabsVm.Appointments().length) {
            //if (!tabsVm.Appointments()[0].OpenPars().length) {
            if (!appt.OpenPars().length) {
                // fake Par placeholder (Id 0 = hidden)
                //                tabsVm.Appointments()[0].OpenPars.push(ko.mapping.fromJS({
                appt.OpenPars.push(ko.mapping.fromJS({
                    Id: 0,
                    ActionDescription: '',
                    ActionType: '',
//                    ActionCode: '',
                    EffectiveDate: '2013-01-01'
                }));
                $('#appt-accordion').accordion('option', 'active', 0); // show emp appt  info
            }
            else {
                $('#appt-accordion').accordion('option', 'active', 1); // show emp pending PARs
            }
            ko.applyBindings(appt, $('#detail-appt')[0]); // new binding
        },
        error: function (xrq, txtStatus, errThrown) {
            alert(xrq['responseText']);
            alert(txtStatus);
        }
    });
}
// TODO: move non-tree functions (e.g. tabs, forms, etc.) to separate script files