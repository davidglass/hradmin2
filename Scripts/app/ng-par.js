﻿angular.module('par', ['kendo.directives'])
.controller('ParController', function ($http, $scope, $timeout) {
    // TODO: enclose this all in a MainController with routing.  set global vals/fns in $rootScope.
    $scope.appRoot = appRoot;
    // vm is server-generated vm, may include ValErr, Par, etc.
    $scope.vm = vm; // vm global is populated in .cshtml template
    convertJsonDates($scope.vm.Par); // convert MVC "/Date(nnn)/" JSON dates to actual dates...(*NOTE*, WebAPI serializes dates differently than MVC!!!)
    // superfluous with Angular Kendo:
    // TODO: $scope.availableActions() function?
    $scope.refreshActions = function () {
        // TODO: constrain availableActions by role + Par.PositionId
        var xn = {
            expand: 'expandAll()',
            collapse: 'collapseAll()'
        };
        var appstatus = $scope.vm.Par.ApprovalStatusId; // 1=Active, 3=Approved, 5=Created, 4=Rejected, 2=Cancelled
        var r = $scope.vm.CurrentUser.RoleName;
        if ((r == 'Admin' || r == 'HR') && $scope.vm.Par.StatusId != 4) {  // disallow deleting processed PARs
            xn['delete'] = 'del()';
        }

        //if ((r == 'Admin' || r == 'HR') && appstatus == 3) { // allow only approved
        //if (r == 'Admin' || r == 'HR') {
        //    xn['preview'] = 'preview()';
        //    xn['send email'] = 'sendEmail()';
        //}

        if ((r == 'Admin' || r == 'HR')
            && $scope.vm.Par.StatusId != 4 // 4 == Processed
            && $scope.vm.Par.StatusId != 5 // 5 == Pushed
            && appstatus == 3) { // allow only unprocessed + approved
            xn['process'] = 'process()';
        }

        if (r == 'Admin' || r == 'HR') {// create checklist
            if ($scope.vm.Par.ActionCode != 'P0' && $scope.vm.Par.ActionCode != 'P2') { // exclude create/abolish Position PARs
                if (!$scope.vm.Par.HasCheckList) { // TODO: exclude create/abolish position par types here...
                    xn['create checklist'] = 'createchecklist()';
                }
                else {
                    xn['checklist'] = 'getCheckList()';
                }
            }
        }

        if ((r == 'Admin' || r == 'HR')) {
            xn['save'] = 'save()';
            xn['preview'] = 'preview()'; // allowing at any time now, not necessarily Approved status
            xn['printable'] = 'print()';
            // TODO: show 'sent mm/dd' if sent
            if ($scope.vm.Par.ParEmailSentAt) {
                xn['sent ' + formatJsonDate($scope.vm.Par.ParEmailSentAt)] = 'sendEmail()';
            }
            else {
                xn['send email'] = 'sendEmail()';
            }
            //&& $scope.vm.Par.StatusId != 4
            ////&& (appstatus == 1 || appstatus == 5)) { // only save Active or Created approval statuses
            //&& (appstatus == 1 || appstatus == 3)) { // only save Active or Created approval statuses
        }

        $scope.availableActions = [];
        for (var k in xn) {
            $scope.availableActions.push({
                label: k,
                action: xn[k]
            });
        }
    }

    $scope.refreshActions();

    // vm.Par.StatusId: 1=Active, 4=Processed
    // vm.Par.ApprovalStatusId (new field): 1=Active, 2=Canceled, 3=Approved, 4=Rejected, 5=Created.

    // config options for KendoUI widgets:
    $scope.options = {
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            //expandMode: "multiple"
            expandMode: "single",
        }
    }

    // scope-level vars:
    $scope.asyncOp = false;
    $scope.attachmentUrl = appRoot + 'api/par/' + $scope.vm.Par.Id + '/attachment';
    $scope.invokedAction = undefined;

    // TODO: consider separate ApprovalTaskController for approval functions???
    // BEGIN Approval functions:
    $scope.approvalTaskApprove = function (t) {
        if (confirm('Approve?')) {
            // seems to be issue with multiple async calls...commenting out save here...DG
            //$scope.save();//Save PAR before Approving
            $scope.save(function () {
                $scope.StatusMessage = 'Approving...';
                $scope.asyncOp = true;
                //t.Task.FinalApproverPersonLanId = userLanId; // DG: LanId can be determined server-side, should not really be stored/managed client-side.
                t.Task.StatusCode = 'Approved';
                $http({
                    method: 'PUT',
                    url: appRoot + 'api/ApprovalTask/' + t.Task.Id,
                    data: t.Task
                })
                .success(function (d) { // save update returns only validation errors...
                    $timeout(function () {
                        $scope.asyncOp = false;
                        $scope.StatusMessage = "";
                        $scope.refreshApprovalWorkflowVm();
                    }, 300);
                })
                .error(function (data, status, errors, config) {
                    $scope.asyncOp = false;
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            });
        }
    }

    $scope.approvalTaskCanceled = function (t) {
        if (confirm('Skip Approval?')) {
            $scope.StatusMessage = 'Skipping Approval';
            $scope.asyncOp = true;
            t.Task.StatusCode = 'Canceled';
            // moved this to server controller -dg:
            //           item.Task.FinalApproverPersonLanId(userLanId);
            $http({
                method: 'PUT',
                url: appRoot + 'api/ApprovalTask/' + t.Task.Id,
                data: t.Task
            })
            .success(function (d) { // save update returns only validation errors...
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.StatusMessage = "";
                    $scope.refreshApprovalWorkflowVm();
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncOp = false;
                if (data.error) { alert('ERROR: ' + data.error); };
            });
        }
    }

    $scope.approvalTaskReject = function (t) {
        if (confirm('Reject?')) {
            $scope.StatusMessage = 'Rejecting...';
            $scope.asyncOp = true;
            // moved server-side:
            //item.Task.FinalApproverPersonLanId(userLanId);
            t.Task.StatusCode = 'Rejected';
            $http({
                method: 'PUT',
                url: appRoot + 'api/ApprovalTask/' + t.Task.Id,
                data: t.Task
            })
            .success(function (d) { // save update returns only validation errors...
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.StatusMessage = "";
                    $scope.refreshApprovalWorkflowVm();
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncOp = false;
                if (data.error) { alert('ERROR: ' + data.error); };
            });
        }
    }

    $scope.approvalWorkflowActive = function () {
        $scope.StatusMessage = 'Starting Workflow';
        $scope.asyncOp = true;
        $scope.vm.StartingWorkflow = false;
        var taskVm = $scope.vm.ApprovalWorkflowVm.DisplayTasks[0];
        for (var i = 0; i < $scope.vm.ApprovalWorkflowVm.DisplayTasks.length; i++) {

            if ($scope.vm.ApprovalWorkflowVm.DisplayTasks[i].Status == 'Created') {
                taskVm = $scope.vm.ApprovalWorkflowVm.DisplayTasks[i];
                break;
            }
        }
        taskVm.Task.StatusCode = 'Active';

        $http({
            method: 'PUT',
            url: appRoot + 'api/ApprovalTask/' + taskVm.Task.Id,
            data: taskVm.Task
        })
        .success(function (d) { // save update returns only validation errors...
            $timeout(function () {
                $scope.asyncOp = false;
                $scope.StatusMessage = "";
                // TODO: bind Submitted status to Submit button visibility...
                $scope.refreshApprovalWorkflowVm();
            }, 300);
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    }

    $scope.createPdf = function () {
        //alert('TODO: createPdf for ActionCode ' + $scope.vm.Par.ActionCode);
        $scope.StatusMessage = 'Creating PDF';
        $scope.asyncOp = true;

        var posturl = $scope.appRoot + 'api/PositionDescription';
        // TODO: pass appropriate params for Create/Modify Position...
        var postData = {
            LinkedParId: vm.Par.Id,
            WorkingTitle: vm.Par.PosWorkingTitle,
            ProposedJobId: vm.Par.PosJobId,
            PositionTypeCXW: vm.Par.PosPersonnelSubArea == '0002' ? 'W' :
                (vm.Par.PosPersonnelSubArea == '0003' ? 'X' : 'C'),
            xProposedJvacEvalPts: vm.Par.PosPointValue,
            IsSupervisor: vm.Par.PosIsSupervisor
        }
        if (vm.Par.ActionCode != 'P0') { // omit PositionId for unprocessed CreatePosition PARs (Positions do not yet exist for these).
            postData.PositionId = vm.Par.PositionId;
        }
        if (confirm('This will create a new Position Description for this PAR...')) {
            $http({ method: 'POST', url: posturl, data: postData })
            .success(function (data) {
                $scope.vm.Par.PdId = data.Id;
                $timeout(function () {
                    // TODO: refactor all asyncOps to use Kendo asyncNotification...
                    //            $scope.asyncNotification.hide();
                    $scope.asyncOp = false;
                    $scope.StatusMessage = "";
                }, 300);
            }) // TODO: more robust client-side error handling...
            .error(function (data, status, errors, config) {
                $scope.asyncOp = false;
                $scope.StatusMessage = "";
                alert('error:' + JSON.stringify(data));
            });
        }
    }

    $scope.hrOverrideWorkflow = function (status) {
        var callback = function () {
            $scope.StatusMessage = (status == 'Rejected' ? 'Rejecting' : 'Approving') + ' Workflow';
            $scope.asyncOp = true;
            var wf = $scope.vm.ApprovalWorkflowVm.Workflow;

            // MOVED LanId bit TO SERVER-SIDE:
            //vm.ApprovalWorkflowVm.Workflow.HrOverridePersonLanId(userLanId);
            $scope.vm.ApprovalWorkflowVm.Workflow.StatusCode = status;

            $http({
                method: 'PUT',
                url: appRoot + 'api/ApprovalWorkflow/' + wf.Id,
                data: wf
            })
            .success(function (d) { // save update returns only validation errors...
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.StatusMessage = "";
                    // TODO: bind Submitted status to Submit button visibility...
                    $scope.refreshApprovalWorkflowVm();
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncOp = false;
                if (data.error) { alert('ERROR: ' + data.error); };
                //if (status == '400') { alert('ERROR: ' + data.error); };
            });
        }

        if (confirm('Override Approval?')) {
            $scope.save(callback);//Save PAR before doing final approve or ject
        }
    }

    $scope.isAuthorizedAndActive = function (t) {
        return t.Status == 'Active' && t.Authorized;
    }

    $scope.refreshApprovalWorkflowVm = function() {
        $scope.StatusMessage = 'Updating Workflow...';
        $scope.asyncOp = true;
        $http({
            method: 'GET',
            url: appRoot + 'api/ApprovalWorkflow/' + $scope.vm.ApprovalWorkflowVm.Workflow.Id
        })
        .success(function(d) {
            $scope.vm.ApprovalWorkflowVm.Status = d.Status;
            if (d.Status == 'Approved') {
                $scope.vm.Par.ApprovalStatusId = 3;
            }
            $scope.vm.ApprovalWorkflowVm.DisplayTasks = d.DisplayTasks;
            $timeout(function () {
                $scope.asyncOp = false;
                $scope.StatusMessage = "";
                $scope.refreshActions();
            }, 300);
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    }

    $scope.saveAndActivateWorkflow = function () {
        if (confirm('Save PAR and Start Approval Workflow?')) {
            $scope.vm.Par.Validate = true; // validate model before updating db:
            $scope.vm.StartingWorkflow = true;
            $scope.save();
        }
    }
    // END Approval functions...

    $scope.collapseAll = function () {
        $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse($('[id^="panel-"]')); // select all with id starting with "panel-"
    }

    $scope.countIssues = function (panelid) {
        var ec = 0;
        $(panelid + ' .validation-err').each(function (index, element) {
            if ($(element).html().length) {
                ec++;
            }
        });
        return ec;
    }

    $scope.createchecklist = function () {
        var posturl = appRoot + 'api/checklist';
        $scope.asyncOp = true;
        $scope.StatusMessage = 'creating checklist...';
        var newItem = {
            Parid: $scope.vm.Par.Id
        }
        $http({ method: 'POST', url: posturl, data: newItem })
            .success(function (created) {
                $timeout(function (d) {
                    $scope.asyncOp = false;
                    $scope.StatusMessage = '';
                    $scope.$apply(function () {
                        $scope.vm.Par.HasCheckList = true;
                        $scope.refreshActions();
                    });
                }, 400);
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
                //                console.log('post error: ' + angular.toJson(data));
            });
    }

    $scope.del = function () {
        var p = $scope.vm.Par;
        if (!confirm('this will permanently delete PAR # ' + p.Id)) {
            return false;
        }
        $scope.asyncOp = true;
        $scope.StatusMessage = ('deleting PAR #' + p.Id);
        //*NOTE* $http.delete shortcut is only in Angular 1.2 +, this is 1.1.x:
        //$http.delete(appRoot + 'api/par/' + p.Id);
        $http({ method: 'DELETE', url: appRoot + 'api/par/' + p.Id })
        .success(function (deleted) {
            // this seems to succeed silently for IE...
            $timeout(function () {
                $scope.asyncOp = false;
                // redirect to par list:
                window.location = appRoot + 'par';
            }, 500); // 500ms timeout to ensure deletion is visible...
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    };

    $scope.deleteAttachment = function (att) {
        if (!confirm('this will permanently delete the attachment ' + att.FileName)) {
            return false;
        }
        var atts = $scope.vm.Par.Attachments;

        $scope.asyncOp = true;
        $scope.StatusMessage = ('deleting ' + att.FileName);

        $http({
            method: 'DELETE',
            url: appRoot + 'api/attachment/' + att.Id
        })
        .success(function (deleted) {
            atts.splice(atts.indexOf(att), 1);
            // this seems to succeed silently for IE...
            $timeout(function () {
                $scope.asyncOp = false;
                //                $scope.StatusMessage = ('deleted ' + att.FileName);
                $scope.StatusMessage = '';
            }, 500); // 500ms timeout to ensure deletion is visible...
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    }

    $scope.doAction = function (axn) {
        //alert('current action label: ' + $scope.availableActions[$scope.availableActions.indexOf(axn)].label);
        $scope.invokedAction = axn;
        eval('$scope.' + axn.action);
        //alert('doAction index #' + $scope.availableActions.indexOf(axn));
        // TODO: update label of email link on successful email:

        //$scope[axn](); // this works as long as not passing args...
    };

    $scope.expandAll = function () {
        // TODO: just iterate on LI children of PanelBar...
        //        $scope.panelbar.expand($('#panel-par')); // selector example
        $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand($('[id^="panel-"]')); // select all with id starting with "panel-"

        // ...etc.
    }

    $scope.getCheckList = function () {
        window.location = appRoot + 'checklist/' + $scope.vm.Par.Id;
    };

    $scope.getParDetailWithEmployee = function () {
        $scope.asyncOp = true;
        // TODO: getEmpInfoForPar to get only relevant employee info, not full Par:
        $scope.StatusMessage = "Loading Selected Employee Information...";
        var p = $scope.vm.Par;
        if (!p.EmployeeId) { return false; }

        $http({
            method: 'GET',
            url: appRoot + 'api/par/' + p.Id + '/emp/' + p.EmployeeId
        })
        .success(function (newpar) { // save update returns only validation errors...
            $timeout(function () {
                // **NOTE** MVC and WebAPI serialize dates DIFFERENTLY!!!
                //                        var dp = Date.parse(d.EmpDOB); // THIS FAILS in IE8 (returns NaN)!!!
                // convert pseudo-ISO8601 WebAPI date to JavaScript Date before mapping vm and building datepickers:
                try {
                    for (var k in newpar) {
                        if (newpar[k] && k.indexOf('EmpDS') == 0 && k.indexOf('Changed') == -1) { // omit Changed properties from format.  TODO: hide from model mapping
                            vm.Par[k] = parseDate(newpar[k]);
                        }
                    }
                    vm.Par.EmpFirstName = newpar.EmpFirstName;
                    vm.Par.EmpPreferredName = newpar.EmpPreferredName;
                    vm.Par.EmpLastName = newpar.EmpLastName;
                    vm.Par.EmpMiddleName = newpar.EmpMiddleName;
                    vm.Par.EmpSuffix = newpar.EmpSuffix;
                    vm.Par.EmpSuffix = newpar.EmpSuffix;
                }
                catch (xcp) {
                    alert('failed to parse dates...' + JSON.stringify(xcp));
                }
                // moved vm.Par update to try block
                //$scope.vm.Par = newpar; // TODO: this needs to be more granular, replace only Employee info. Or save PAR first...

                $scope.asyncOp = false;
                $scope.StatusMessage = "";
            }, 500); // 500ms timeout to ensure deletion is visible...
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    }

    $scope.getParTypeLabel = function () {
        var p = $scope.vm.Par;
        return p.IsTransferIn ? 'Fill' :
            (p.IsTransferOut && p.ActionReasonId == 81 ? 'Separation' :
                (p.ActionCode == 'U6' ? 'Fill, Prior Service' :
                    p.ActionDescription));
    };

    $scope.monthlySalary = function () {
        return Math.round($scope.vm.Par.ApptPayAnnualSalary / .12) / 100;
    }

    $scope.preview = function () {
        window.open(appRoot + 'par/' + $scope.vm.Par.Id + '/email', "_blank");
    }

    $scope.print = function () {
        // TODO: configurable SSRS server
        window.location = 'http://test-reporting.cts.wa.gov/reportserver?/Human Resources/Print/Par&rs:Command=Render&rs:Format=pdf&Id=' + $scope.vm.Par.Id + '&rs:ClearSession=true'
    }

    $scope.process = function () {
        if (confirm('Process PAR #' + $scope.vm.Par.Id + '?\n\n'
    + 'This will make permanent the proposed changes;\nthe only way to revert them will be through a new PAR.'
    )) {
            $scope.asyncOp = true;
            $scope.StatusMessage = "processing...";
            $scope.vm.StartingWorkflow = false;
            $scope.vm.Par.UpdateAction = "Process";
            $scope.save(function () {
                $scope.vm.Par.StatusId = 4; // set to processed on success
                $scope.refreshActions();
            });
        }
    }

    $scope.save = function (cb) {
        var p = $scope.vm.Par;
        $scope.asyncOp = true;
        // set message unless already set...
        if (!$scope.StatusMessage) { $scope.StatusMessage = "Saving..."; }
        $http({
            method: 'PUT',
            url: appRoot + 'api/par/' + p.Id,
            data: $scope.vm.Par
        })
        .success(function (valerrs) { // save update returns only validation errors...
            $timeout(function () {
                $scope.vm.ValErr = valerrs;
                $scope.asyncOp = false;
                $scope.StatusMessage = "";
                // todo: convert this if to callback...
                if ($scope.vm.StartingWorkflow) {
                    $scope.approvalWorkflowActive();
                }
                // invoke callback if supplied:
                if (cb) {
                    cb();
                }
            }, 300);
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
            if (data.ExceptionMessage) { alert('ERROR: ' + data.ExceptionMessage); };
            $scope.StatusMessage = "ERROR on save";
        });
    }

    $scope.sendEmail = function() {
        // TODO: checkbox for "BCC me", pass val in posted data object...
        if (!confirm('This will send a PAR email to the currently configured recipient(s).')) {
            return false;
        }
        $scope.asyncOp = true;
        $scope.StatusMessage = 'sending email';
        $http({
            method: 'POST',
            url: appRoot + 'api/par/' + $scope.vm.Par.Id + '/email',
        })
        .success(function (d) {
            $scope.StatusMessage = 'sent email.';
            $timeout(function () {
                var now = new Date();
                $scope.asyncOp = false;
                $scope.StatusMessage = '';
                // set email label:
                $scope.availableActions[$scope.availableActions.indexOf($scope.invokedAction)].label = 'sent '
                + (now.getMonth() + 1) + '/' + now.getDate() + '/' + now.getFullYear();
            }, 300);
        })
        .error(function (data, status, errors, config) {
            $scope.asyncOp = false;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    }

    $scope.setContractEnd = function () {
        if ($scope.vm.Par.EffectiveDate) {
            var ctc = $scope.vm.Par.ContractTypeCode;
            var efd = new Date(Date.parse($scope.vm.Par.EffectiveDate));
            if (ctc.match(/(02|03|04|05|10|11|12|21|22)/)) {
                if (efd.getMonth() > 5) {
                    efd.setFullYear(efd.getFullYear() + 1);
                }
                var newMonth = (6 + efd.getMonth()) % 12;
                efd.setMonth(newMonth);
                $scope.vm.Par.ContractEndDate = efd;
            }
            else if (ctc.match(/23/)) {
                // WMS Review: 12 months
                efd.setFullYear(efd.getFullYear() + 1);
                $scope.vm.Par.ContractEndDate = efd;
            }
            else {
                $scope.vm.Par.ContractEndDate = null;
            }
        }
        else {
            alert('set PAR EffectiveDate to auto-calculate contract end date.');
        }
    }

    $scope.setNewHireDates = function () {
        var ac = $scope.vm.Par.ActionCode;
        if (ac == 'U0' || ac == 'U6') {
            // if par type is new hire or rehire, copy EffectiveDate to Appt, CtsHireDates
            $scope.vm.Par.EmpDS02_Appointment = $scope.vm.Par.EmpDS03_CtsHire = $scope.vm.Par.EffectiveDate;
        }
    }

    $scope.uncheck = function (cb) {
        $scope.vm.Par[cb] = false;
    };
})
.controller('ParListController', function ($filter, $http, $scope, $timeout) {
    $scope.vm = vm; // vm global is populated in .cshtml template
    $scope.convertJsonDate = function(d) {
        if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
        return new Date(parseInt(d.match(/-?\d+/)[0]));
    }
    // default to order by Id descending (most recent first)
    $scope.sortField = 'Id'; // default sort field
    $scope.desc = true;
    $scope.sortOn = function (f) {
        //*note* Angular can take array of sort fields, TODO: manage this to enable multi-sort (splice out if found, then push)
        // (currently only single sort field is supported...)
        $scope.desc = $scope.sortField == f ? !$scope.desc : false; // default to ascending unless second click, then reverse.
        $scope.sortField = f;
    }
    $scope.submit = function () {
        alert('called submit.');
    }
});

// indexOf Polyfill for IE8:
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
        if (this === undefined || this === null) {
            throw new TypeError('"this" is null or not defined');
        }

        var length = this.length >>> 0; // Hack to convert object.length to a UInt32

        fromIndex = +fromIndex || 0;

        if (Math.abs(fromIndex) === Infinity) {
            fromIndex = 0;
        }

        if (fromIndex < 0) {
            fromIndex += length;
            if (fromIndex < 0) {
                fromIndex = 0;
            }
        }

        for (; fromIndex < length; fromIndex++) {
            if (this[fromIndex] === searchElement) {
                return fromIndex;
            }
        }

        return -1;
    };
}
// end Polyfill

// start global JS (outside of AngularJS) functions:
function formatJsonDate(d) {
    // allow negation prefix for dates before 1970:
    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

// response handler for file post:
function handleFilePostResponse() {
    var $scope = angular.element('#ng-app').scope();
    if (!$scope) {
        return true;
    } // bypass during DOM init...

    var responseText = $('#hiddenFrame').contents(document).text();
    if (!responseText) return true; //Firefox fires load event without loading frame
    var attachResponse = JSON.parse(responseText);
    $scope.$apply(function () {
        if (attachResponse.error) {
            $scope.StatusMessage =  'ERROR:' + attachResponse.error;
            $scope.asyncOp = false;
            return false;
        }
        $scope.vm.Par.Attachments.push(attachResponse);
        $scope.StatusMessage = "Attached";
        setTimeout(function () {
            $scope.$apply(function () {
                $scope.asyncOp = false;
                $scope.StatusMessage = "";
            });
        }, 800); // TODO: see if possible to use Angular $timeout service externally
    });
    // clear file name: (see "more elegant" answer here):
    // http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
    $('#fileIEframe').wrap('<form>').closest('form').get(0).reset();
    $('#fileIEframe').unwrap();
}

// inspired by ISO8601 parser found here:
// http://n8v.enteuxis.org/2010/12/parsing-iso-8601-dates-in-javascript/
function parseDate(s) {
    var re = /(\d{4})\-(\d\d)\-(\d\d)/;
    var ymd = s.match(re);
    var d = new Date(ymd[1], ymd[2] - 1, ymd[3]);
    //alert('tz offset=' + d.getTimezoneOffset());
    return d;
}

function convertJsonDates(p) {
    // to convert JsonDates to actual dates...
    p.EffectiveDate = formatJsonDate(p.EffectiveDate);
    p.ContractDate = formatJsonDate(p.ContractDate);
    p.ApptPayScaleEffectiveDate = formatJsonDate(p.ApptPayScaleEffectiveDate);
    p.ApptPayNextIncrease = formatJsonDate(p.ApptPayNextIncrease);
    for (var k in p) {
        if (k.indexOf('EmpDS') == 0 && k.indexOf('Changed') == -1) {
            p[k] = formatJsonDate(p[k]);
        }
    }
}



function uploadFile() {
    var $scope = angular.element('#ng-app').scope();
    if (!$scope) {
        return true;
    } // bypass during DOM init...
    $scope.$apply(function () {
        $scope.asyncOp = true;
        $scope.StatusMessage = "Attaching...";
    });
    var f = $('#uploadFormIEframe');
    f.submit();
}