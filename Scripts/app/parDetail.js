﻿$(function () {
    init();
    $(function ($) {
//        $('#ApptPayAnnualSalary').mask("99999?9", { placeholder: "." }); // terminal decimal to allow parsing as Decimal
//        $('#ApptPayAnnualSalary').mask("999999.99", { placeholder: "0" }); // terminal decimal to allow parsing as Decimal
        $('#ApptPayAnnualSalary').maskMoney({ decimal: '.' , thousands: ','});
        $('#ApptPayHourlyRate').mask("99.99");
        $('#EmpAddrHomePhone').mask("(999) 999-9999");
        $('#EmpSSN').mask("999-99-9999");
        $('#PercentFullTime').mask("99?9");
        $('#PosPointValue').mask("?999");
    });

    $('#ApptPayAnnualSalary').change(function (e) {
        $('#ApptPayScaleEffectiveDate').val($('#EffectiveDate').val());
        var vm = $(document).data('viewmodel');
        vm.Par.ApptPayAnnualSalary($('#ApptPayAnnualSalary').maskMoney('unmasked')[0]);
        //alert('PayScale Effective Date was set to the PAR Effective Date');
    });

    $('#ActionReason').change(function () {
        var vm = $(document).data('viewmodel');
        if ($('#ActionReason').val() != 81) {
            vm.Par.IsTransferOut(false);
            vm.Par.IsTransferIn(false);
        }
    });

    $('#XferIn').change(function () {
        var vm = $(document).data('viewmodel');
        if (vm.Par.IsTransferIn()) {
            vm.Par.IsTransferOut(false);
            vm.Par.ActionReasonId(81); // change selection to Transfer if other selected.
        }
    });

    $('#XferOut').change(function () {
        var vm = $(document).data('viewmodel');
        if (vm.Par.IsTransferOut()) {
            vm.Par.IsTransferIn(false);
            vm.Par.ActionReasonId(81); // change selection to Transfer if other selected.
        }
    });

    $('#ShiftCode').change(function () {
        var vm = $(document).data('viewmodel');
        var day = ($('#ShiftCode').val() == 'D');
        if (confirm((day ? 'remove' : 'set') + ' shift differential?')) {
            //    $('#ShiftDiff').prop('checked', !day);
            vm.Par.ApptShiftDifferential(!day);
        }
    });

    // this handles JSON response in hidden frame upon attaching a file to the PAR:
    $('#hiddenFrame').load(function () {
        try {
            var responseText = $('#hiddenFrame').contents(document).text();
            if (!responseText) return true; //Firefox fires load event without loading frame
            var attachResponse = ko.mapping.fromJSON(responseText);
            if (attachResponse.error) {
                $('#StatusMessage').text('ERROR:' + attachResponse.error());
                $('#Spinner').hide();
                return false;
            }
            // fade out status on successful upload...
            $('#statusbar').fadeTo(700, 0, function () {
                $('#StatusMessage').text('');
                $('#Spinner').hide();
                var vm = $(document).data('viewmodel');
                vm.Par.Attachments.push(attachResponse);
                // clear file name: (see "more elegant" answer here):
                // http://stackoverflow.com/questions/1043957/clearing-input-type-file-using-jquery
                $('#fileIEframe').wrap('<form>').closest('form').get(0).reset();
                $('#fileIEframe').unwrap();
            });
        }
        catch (xrq) {
            //alert('xrq=' + JSON.stringify(xrq));
            $('#StatusMessage').text(xrq.message);
            //$('#StatusMessage').text('ERROR: attachment failed.');
            $('#Spinner').hide();
        }
    });

    $('#ToggleAccordion').click(function (evt) {
        if ($('#par-detail').hasClass('ui-accordion')) { // is accordion
            $(evt.target).text('Collapse');
            $('#par-detail').accordion('destroy');
            $('.HeadContainer').css("background-color", "#194775");
            $('.HeadContainer').css("width", "800px");
            $('.HeadContainer').css("color", "#ffdd88");
            //$('.HeadContainer').css("height", "10px");
            $('.HeadContainer').css("padding", "10px");

        }
        else {
            // TODO: make this a class which can be added / removed:
            $('.HeadContainer').css({
                "background-color": "transparent",
                "color": "inherit",
                "height": "inherit",
                "padding": "0px"
            });
            $(evt.target).text('Expand all');
            $('#par-detail').accordion({
                active: 0, // expand panel 0 by default.
                animated: 'slide',
                collapsible: true,
                heightStyle: 'content'
            });
        }
    });
});

function sendEmail() {
    // TODO: checkbox for "BCC me", pass val in posted data object...
    var vm = $(document).data('viewmodel');
    $('#Spinner').show();
    $('#StatusMessage').text('sending email');
    $.ajax({
        type: 'POST',
        url: appRoot + 'api/par/' + vm.Par.Id() + '/email',
        success: function (d) {
            $('#statusbar').fadeTo(700, 0, function () {
                $('#Spinner').hide();
                $('#StatusMessage').text(''); // clear async message
            });
        },
        error: function (xrq, txtStatus, errThrown) {
            $('#Spinner').hide();
            $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
        }
    });
}

function deletePar() {
    var vm = $(document).data('viewmodel');
    if (confirm('deleting PAR #' + vm.Par.Id())) {
        $.ajax({
            type: 'POST',
            url: appRoot + 'api/par/' + vm.Par.Id(),
            beforeSend: function (xhr) {
                xhr.setRequestHeader("X-Http-Method-Override", "DELETE");
            },
            success: function (d) {
                window.location = appRoot + 'par'; // back to list
            },
            error: function (xrq, txtStatus, errThrown) {
                var err = JSON.parse(xrq['responseText']);
                $('#ErrMessage').text(err.Message);
                //alert(xrq['responseText']);
                //alert(txtStatus);
            }
        });
    }
}

function processPar() {
    $('#ErrMessage').text('');
    var vm = $(document).data('viewmodel');
    if (confirm('Process PAR #' + vm.Par.Id() + '?\n\n'
        +'This will make permanent the proposed changes;\nthe only way to revert them will be through a new PAR.'
        )) {
        vm.StartingWorkflow(false);
        vm.Par.UpdateAction("Process");
        savePar();
    }
}

function deleteAttachment(item) {
    var vm = $(document).data('viewmodel');
    $('#StatusMessage').text('deleting attachment: ' + item.FileName());
    $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
    $('#Spinner').show();

    $.ajax({
        type: 'POST',
        url: appRoot + 'api/attachment/' + item.Id(),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Http-Method-Override", "DELETE");
        },
        success: function (d) {
            $('#statusbar').fadeTo(1000, 0, function () {
                $('#Spinner').hide();
                $('#StatusMessage').text(''); // clear async message
            });
            vm.Par.Attachments.remove(item);
        },
        error: function (xrq, txtStatus, errThrown) {
            $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
        }
    });
}

function saveAndActivateWorkflow() {
    var vm = $(document).data('viewmodel');
    vm.Par.Validate(true); // validate model before updating db:
    vm.StartingWorkflow(true);
    savePar();
}


function hrOverrideWorkflow(status){
    if (confirm('Override Approval?')) {

        savePar();//Save PAR before doing final approve or ject

        $('#StatusMessage').text('Rejecting Workflow');
        $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
        $('#Spinner').show();        

        var vm = $(document).data('viewmodel');
        vm.ApprovalWorkflowVm.Workflow.HrOverridePersonLanId(userLanId);
        vm.ApprovalWorkflowVm.Workflow.StatusCode(status);

        $.ajax({
            type: 'PUT',
            //        url: appRoot + 'api/ApprovalTask/' + taskVm.Task,
            url: appRoot + 'api/ApprovalWorkflow/' + vm.ApprovalWorkflowVm.Workflow.Id(), // assuming this was meant here -DG
            data: ko.toJS(vm.ApprovalWorkflowVm.Workflow), // workflow
            success: function (d) {
                refreshApprovalWorkflowVm();
            },
            error: function (xrq, txtStatus, errThrown) {
                $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
            }
        });
    }
}
                                           

function approvalWorkflowActive() {
    $('#StatusMessage').text('Starting Workflow');
    $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
    $('#Spinner').show();

    //Get first task with status of created. 
    var vm = $(document).data('viewmodel');
    vm.StartingWorkflow(false);

    var taskVm = (vm.ApprovalWorkflowVm.DisplayTasks())[0];
    for (var i = 0; i < vm.ApprovalWorkflowVm.DisplayTasks().length; i++) {

        if (vm.ApprovalWorkflowVm.DisplayTasks()[i].Status() == 'Created') {

            taskVm = (vm.ApprovalWorkflowVm.DisplayTasks())[i];
            break;
        }
    }

    taskVm.Task.StatusCode('Active');

    $.ajax({
        type: 'PUT',
//        url: appRoot + 'api/ApprovalTask/' + taskVm.Task,
        url: appRoot + 'api/ApprovalTask/' + taskVm.Task.Id(), // assuming this was meant here -DG
        data: ko.toJS(taskVm.Task), // Task
        success: function (d) {
            $('#SubmitLink').css('visibility', 'hidden');//added to remove submit link from header.
            refreshApprovalWorkflowVm();
        },
        error: function (xrq, txtStatus, errThrown) {
            $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
        }
    });
}

function approvalTaskCanceled(item) {
    if (confirm('Skip Task?')) {
        $('#StatusMessage').text('Skipping Task');
        $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
        $('#Spinner').show();
        item.Task.FinalApproverPersonLanId(userLanId);
        item.Task.StatusCode('Canceled');

        $.ajax({
            type: 'PUT',
            //        url: appRoot + 'api/ApprovalTask/' + item.Task,
            url: appRoot + 'api/ApprovalTask/' + item.Task.Id(),
            data: ko.toJS(item.Task), // Task
            success: function (d) {

                refreshApprovalWorkflowVm();

            },
            error: function (xrq, txtStatus, errThrown) {
                $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
            }
        });
    }
}

function approvalTaskReset(item) {
    $('#StatusMessage').text('Reseting Task');
    $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
    $('#Spinner').show();
    item.Task.StatusCode('Created');

    $.ajax({
        type: 'PUT',
//        url: appRoot + 'api/ApprovalTask/' + item.Task,
        url: appRoot + 'api/ApprovalTask/' + item.Task.Id(),
        data: ko.toJS(item.Task), // Task
        success: function (d) {

            refreshApprovalWorkflowVm();

        },
        error: function (xrq, txtStatus, errThrown) {
            $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
        }
    });
}

function approvalTaskApprove(item) {
    if (confirm('Approve?')) {

        savePar();//Save PAR before Approving

        $('#StatusMessage').text('Approving');
        $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
        $('#Spinner').show();        

        item.Task.FinalApproverPersonLanId(userLanId);
        //item.Task.FinalApproverPersonId(6058);
        item.Task.StatusCode('Approved');

        $.ajax({
            type: 'PUT',
            //        url: appRoot + 'api/ApprovalTask/' + item.Task,
            url: appRoot + 'api/ApprovalTask/' + item.Task.Id(),
            data: ko.toJS(item.Task), // Task
            success: function (d) {

                refreshApprovalWorkflowVm();

            },
            error: function (xrq, txtStatus, errThrown) {
                $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
            }
        });
    }
}

function approvalTaskReject(item) {
    if (confirm('Reject?')) {
        $('#StatusMessage').text('Rejecting');
        $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
        $('#Spinner').show();
        item.Task.FinalApproverPersonLanId(userLanId);
        item.Task.StatusCode('Rejected');

        $.ajax({
            type: 'PUT',
            //        url: appRoot + 'api/ApprovalTask/' + item.Task,
            url: appRoot + 'api/ApprovalTask/' + item.Task.Id(),
            data: ko.toJS(item.Task), // Task
            success: function (d) {

                refreshApprovalWorkflowVm();
            },
            error: function (xrq, txtStatus, errThrown) {
                $('#StatusMessage').text(xrq['responseText'] + ':' + txtStatus);
            }
        });
    }
}

function refreshApprovalWorkflowVm() {
    var vm = $(document).data('viewmodel');
    var workflowId = vm.ApprovalWorkflowVm.Workflow.Id();
    $.ajax({
        type: 'GET',
        url: appRoot + 'api/ApprovalWorkflow/' + workflowId,
        success: function (d) {
            
            vm.ApprovalWorkflowVm.Status(d.Status);
            //debugger;
            for (var i = 0; i < d.DisplayTasks.length; i++) {
                //check to see if returned task list has grown.
                if (vm.ApprovalWorkflowVm.DisplayTasks().length == i) {

                    var newTask = d.DisplayTasks[i];
                    vm.ApprovalWorkflowVm.DisplayTasks.push(ko.mapping.fromJS(newTask));
                }
                else {
                    vm.ApprovalWorkflowVm.DisplayTasks()[i].Status(d.DisplayTasks[i].Status);
                    vm.ApprovalWorkflowVm.DisplayTasks()[i].Task.StatusCode(d.DisplayTasks[i].Task.StatusCode);
                    vm.ApprovalWorkflowVm.DisplayTasks()[i].CompletedDateString(d.DisplayTasks[i].CompletedDateString);
                    vm.ApprovalWorkflowVm.DisplayTasks()[i].Task.Comment(d.DisplayTasks[i].Task.Comment);
                    vm.ApprovalWorkflowVm.DisplayTasks()[i].FinalApproverPersonName(d.DisplayTasks[i].FinalApproverPersonName);
                }

            }
            $('#statusbar').fadeTo(1000, 0, function () {
                $('#Spinner').hide();
                $('#StatusMessage').text(''); // clear async message
            });
        },
        error: function (xrq, txtStatus, errThrown) {
            alert(xrq['responseText']);
            alert(txtStatus);
            $('#statusbar').fadeTo(1000, 0, function () {
                $('#Spinner').hide();
                $('#StatusMessage').text(''); // clear async message
            });
        }
    });
}

function formatJsonDate(d) {
    // allow negation prefix for dates before 1970:
    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

function savePar() {
    // set RaceCodes to comma-concatenated string:
    //alert('selected race: ' + $('#RaceCode').val());
    var vm = $(document).data('viewmodel');
    //vm.Par.EmpRaceCodes($('#RaceCode').val());
    var jobclass = $('#JobClassList option:selected').text();
    var contract = $('#ContractTypeList option:selected').text();
    var actiontype = $('#ActionReason option:selected').text();
    // to update UI for individual multi-bound property, call valueHasMutated() on it.
    // explicitly setting since no datepicker knockout binding.
    // this is a plain int:
    if ($('#EffectiveDate').val()) { // ignore blanks here.
        vm.Par.EffectiveDate(formatJsonDate('' + $('#EffectiveDate').datepicker('getDate').getTime()));
    }
    if (vm.Par.ActionCode() == 'U0' && $('#DOB').val()) { // DOB is only available for New Hire PARs
        vm.Par.EmpDOB(formatJsonDate('' + $('#DOB').datepicker('getDate').getTime()));
    }
    if (vm.Par.ActionCode() != 'U5') { // no date edits for separation type
        if ($('#ContractEndDate').val()) { // ignore blanks here.
            vm.Par.ContractEndDate(formatJsonDate('' + $('#ContractEndDate').datepicker('getDate').getTime()));
        }
        else {
            vm.Par.ContractEndDate(null);
        }
        if ($('#ApptPayScaleEffectiveDate').val()) { // ignore blanks here.
            vm.Par.ApptPayScaleEffectiveDate(formatJsonDate('' + $('#ApptPayScaleEffectiveDate').datepicker('getDate').getTime()));
        }
        if ($('#ApptPayNextIncrease').val()) { // ignore blanks here.
            vm.Par.ApptPayNextIncrease(formatJsonDate('' + $('#ApptPayNextIncrease').datepicker('getDate').getTime()));
        }
        if ($('#ApptStepMEligibleDate').val()) { // ignore blanks here.
            vm.Par.ApptStepMEligibleDate(formatJsonDate('' + $('#ApptStepMEligibleDate').datepicker('getDate').getTime()));
        }
        if ($('#AnniversaryDate').val()) { // ignore blanks here.
            vm.Par.EmpDS01_Anniversary(formatJsonDate('' + $('#AnniversaryDate').datepicker('getDate').getTime()));
        }
        if ($('#AppointmentDate').val()) { // ignore blanks here.
            vm.Par.EmpDS02_Appointment(formatJsonDate('' + $('#AppointmentDate').datepicker('getDate').getTime()));
        }
        if ($('#CtsHireDate').val()) { // ignore blanks here.
            vm.Par.EmpDS03_CtsHire(formatJsonDate('' + $('#CtsHireDate').datepicker('getDate').getTime()));
        }
        if ($('#PriorPidDate').val()) { // ignore blanks here.
            vm.Par.EmpDS04_PriorPid(formatJsonDate('' + $('#PriorPidDate').datepicker('getDate').getTime()));
        }
        if ($('#SeniorityDate').val()) { // ignore blanks here.
            vm.Par.EmpDS05_Seniority(formatJsonDate('' + $('#SeniorityDate').datepicker('getDate').getTime()));
        }
        if ($('#UnbrokenServiceDate').val()) { // ignore blanks here.
            vm.Par.EmpDS07_UnbrokenService(formatJsonDate('' + $('#UnbrokenServiceDate').datepicker('getDate').getTime()));
        }
        if ($('#VacLeaveFrozenDate').val()) { // ignore blanks here.
            vm.Par.EmpDS09_VacLeaveFrozen(formatJsonDate('' + $('#VacLeaveFrozenDate').datepicker('getDate').getTime()));
        }
        if ($('#PersonalHolidayElgDate').val()) { // ignore blanks here.
            vm.Par.EmpDS18_PersonalHolidayElg(formatJsonDate('' + $('#PersonalHolidayElgDate').datepicker('getDate').getTime()));
        }
        if ($('#PersonalLeaveElgDate').val()) { // ignore blanks here.
            vm.Par.EmpDS26_PersonalLeaveElg(formatJsonDate('' + $('#PersonalLeaveElgDate').datepicker('getDate').getTime()));
        }
    }
    $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
    $('#Spinner').show();
    $('#StatusMessage').text(vm.Par.UpdateAction() == 'Process' ? '(processing)' : '(saving)');
    if ($('#par-detail').hasClass('ui-accordion')) { // skip if in print/html mode
        var activePanel = $('#par-detail').accordion('option', 'active');
    }
    var eid = vm.Par.EmployeeId();
    $.ajax({
        type: 'POST',
        url: appRoot + 'api/par/' + vm.Par.Id(),
        beforeSend: function (xhr) {
            xhr.setRequestHeader("X-Http-Method-Override", "PUT");
        },
        data: ko.toJS(vm.Par), // ParDetail model
        success: function (modelstate) { // now returning modelstate even on success...
            if (vm.Par.UpdateAction() == 'Process') {
                vm.Par.StatusId(4); // explicitly setting to "Processed".
                $('#ProcessLink').hide();
            }
            vm.Par.UpdateAction(''); // default update
            // clear any error messages from (now hidden) form elements:
            var modelerrs = {};
            for (var k in modelstate) {
                vek = k.substring(k.lastIndexOf('.') + 1); // strip off object qualifiers ("pd" in this case)
                modelerrs[vek] = modelstate[k];
            }

            for (var i in vm.ValErr) {
                if (ko.isObservable(vm.ValErr[i])) {
                    vm.ValErr[i](null);
                }
            }

            for (var k in modelerrs) {
                vm.ValErr[k] = ko.observable(modelerrs[k]);  // problem rebinding.
//                vm.ValErr[k](modelerrs[k]); // set newly-returned error message
            }

            vm.Par.FillTitle(jobclass);
            vm.Par.ContractType(contract);
            vm.Par.ActionType(actiontype);
            if ($('#par-detail').hasClass('ui-accordion')) { // skip if in print/html mode
                var activePanel = $('#par-detail').accordion('option', 'active');
                $('#par-detail').accordion('destroy').accordion({
                    active: activePanel,
                    animated: 'swing',
                    collapsible: true,
                    heightStyle: 'content'
                });

            }
            // LEAVING always in edit mode now:
            $('#statusbar').fadeTo(700, 0, function () {
                $('#Spinner').hide();
                $('#StatusMessage').text(''); // clear async message
            });
            // trying re-bind to pick up new validations, seems to work OK.
            // TODO: test more thoroughly.
            $('#TaskList').empty();
            ko.cleanNode($('#TaskList')[0]);
            ko.applyBindings(vm, $('#par-detail')[0]);
            updateStatusBars(vm);
            //check to see if workflow is starting.
            if (vm.StartingWorkflow() == true) {
                approvalWorkflowActive();
            }
        },
        error: function (xrq, txtStatus, errThrown) {
            // nullify status, only set during process of approved par.
            vm.Par.UpdateAction(''); // default update
            $('#StatusMessage').text('ERROR'); // clear async message
            $('#Spinner').hide(); // no need to keep spinning
            // general error handler...
            // TODO: handle non-validation errors separately...
            //alert(xrq['responseText']);
            var err = JSON.parse(xrq['responseText']);

            // TO TEMPORARILY VIEW COMPLETE ModelState, uncomment next line:
            //alert(JSON.stringify(err.ModelState));
            //// TODO: logging subsystem...

            // trying re-bind:

            // first clear any pre-existing error messages:
            for (var i in vm.ValErr) {
                if (ko.isObservable(vm.ValErr[i])) {
                    vm.ValErr[i](null);
                }
            }

            var ek = [];
            for (var k in err.ModelState) {
                vek = k.substring(k.lastIndexOf('.') + 1); // strip off object qualifiers ("pd" in this case)
                if (typeof vm.ValErr[vek] == "undefined") {
                    vm.ValErr[vek] = ko.observable(err.ModelState[k]);
                }
                else {
                    vm.ValErr[vek](err.ModelState[k]); // set newly-returned error message
                }
            }
            $('#ErrMessage').text(err.ExceptionMessage);
            // re-apply bindings to pick up new errors:
            $('#TaskList').empty();
            ko.cleanNode($('#TaskList')[0]);
            ko.applyBindings(vm, $('#par-detail')[0]);

            updateStatusBars(vm);

            //check to see if workflow was attempting to start. Reset to false if so.
            if (vm.StartingWorkflow() == true) {
                vm.StartingWorkflow(false);
            }
        }
    });
}

function updateStatusBars(vm) {
    var sections = ['par-info', 'appt-info', 'diversity-info', 'emp-info', 'emp-address', 'pay-info', 'pos-info', 'emp-dates'];
    for (var i in sections) {
        var ec = 0;
        $('#' + sections[i] + ' .validation-err').each(function (index, element) {
            if ($(element).html().length) {
                ec++;
            }
        });
        if (ec) {
            $('#' + sections[i] + '-status').html('' + ec + ' input issue(s)');
            $('#' + sections[i] + '-status').css('visibility', 'visible');
        }
        else {
            $('#' + sections[i] + '-status').html('');
            $('#' + sections[i] + '-status').css('visibility', 'hidden');
        }
    }
}

// TODO: prevent calling this during initial Incumbent pre-selection...
function getParDetailWithEmployee() {
    var vm = $(document).data('viewmodel');
    if (!$('#EmpList').val()) { return false; }
    $('#Spinner').show();
    $('#StatusMessage').text('loading employee information');
    $('#statusbar').fadeTo(0, 1); // fade back in (instantly, not really fade).
    $.ajax({
        type: 'GET',
        url: appRoot + 'api/par/' + vm.Par.Id() + '/emp/' + vm.Par.EmployeeId(),
        success: function (d) {
            $('#statusbar').fadeTo(700, 0, function () {
                $('#Spinner').hide();
                $('#StatusMessage').text(''); // clear async message
            });

            // **NOTE** MVC and WebAPI serialize dates DIFFERENTLY!!!
            //                        var dp = Date.parse(d.EmpDOB); // THIS FAILS in IE8 (returns NaN)!!!
            // convert pseudo-ISO8601 WebAPI date to JavaScript Date before mapping vm and building datepickers:
            if (d.EmpDOB) { // will be blank when filling from yet-unselected existing emp.
                d.EmpDOB = formatJsonDate('' + parseDate(d.EmpDOB).getTime());
            }

            // TODO: test all dates exist before attempting parse:
            if (d.ContractEndDate) { // ignore blanks here.
                d.ContractEndDate = formatJsonDate('' + parseDate(d.ContractEndDate).getTime());
            }

            // convert non-null EmpDateSpecs for datepickers
            for (var k in d) {
                if (d[k] && k.indexOf('EmpDS') == 0 && k.indexOf('Changed') == -1) { // omit Changed properties from format.  TODO: hide from model mapping
                    d[k] = formatJsonDate('' + parseDate(d[k]).getTime());
                }
            }

            //DG: changed this since it was breaking on lookup employee:
            if (d.ApptPayScaleEffectiveDate) { // ignore blanks here.
                d.ApptPayScaleEffectiveDate = formatJsonDate('' + parseDate(d.ApptPayScaleEffectiveDate).getTime());
            }

            vm.Par.EmpFirstName(d.EmpFirstName);
            vm.Par.EmpPreferredName(d.EmpPreferredName);
            vm.Par.EmpMiddleName(d.EmpMiddleName);
            vm.Par.EmpLastName(d.EmpLastName);
            vm.Par.EmpSuffix(d.EmpSuffix);
            vm.Par.EmpDOB(d.EmpDOB);
            vm.Par.EmpDS01_Anniversary(d.EmpDS01_Anniversary);
            vm.Par.EmpDS02_Appointment(d.EmpDS02_Appointment);
            vm.Par.EmpDS03_CtsHire(d.EmpDS03_CtsHire);
            vm.Par.EmpDS04_PriorPid(d.EmpDS04_PriorPid);
            vm.Par.EmpDS05_Seniority(d.EmpDS05_Seniority);
            vm.Par.EmpDS07_UnbrokenService(d.EmpDS07_UnbrokenService);
            vm.Par.EmpDS09_VacLeaveFrozen(d.EmpDS09_VacLeaveFrozen);
            vm.Par.EmpDS18_PersonalHolidayElg(d.EmpDS18_PersonalHolidayElg);
            vm.Par.EmpDS26_PersonalLeaveElg(d.EmpDS26_PersonalLeaveElg);

            //vm.Par.EmpRaceCodes = ko.observableArray(vm.Par.EmpRaceCodes().split(',')); // convert string to array

            destroyDatePickers();
            createDatePickers();
        },
        error: function (xrq, txtStatus, errThrown) {
            $('#ErrMessage').text(xrq.statusText);
            //$('#StatusMessage').text('ERROR: ' + xrq['responseText']);
            $('#Spinner').hide();
            //alert(xrq['responseText']);
            //alert(txtStatus);
        }
    });
} // end fn

function createDatePickers() {
    $('#DOB').datepicker(
        {
            changeMonth: true,
            changeYear: true,
            yearRange: '-80:-18' // assumes employee is between 18 and 80
            //onClose: function (dateText, inst) { alert('closed ' + JSON.stringify(inst) + ' with ' + dateText); }
        }
    );
    var dates = [
        'AnniversaryDate',
        'AppointmentDate',
        'ApptPayScaleEffectiveDate',
        'ApptPayNextIncrease',
        'ApptStepMEligibleDate',
        'ContractEndDate',
        'CtsHireDate',
        'PriorPidDate',
        'SeniorityDate',
        'UnbrokenServiceDate',
        'VacLeaveFrozenDate',
        'PersonalHolidayElgDate',
        'PersonalLeaveElgDate'
    ];
    for (var i in dates) {
        $('#' + dates[i]).datepicker({
            changeMonth: true,
            changeYear: true
            //, yearRange: '-40' // this breaks datepicker when binding to invalid date
        });
    }
}

function destroyDatePickers() {
    var dates = [
        'DOB',
        'AnniversaryDate',
        'AppointmentDate',
        'ApptPayScaleEffectiveDate',
        'ApptPayNextIncrease',
        'CtsHireDate',
        'PriorPidDate',
        'SeniorityDate',
        'UnbrokenServiceDate',
        'VacLeaveFrozenDate',
        'PersonalHolidayElgDate',
        'PersonalLeaveElgDate'
    ];
    for (var i in dates) {
        $('#' + dates[i]).datepicker("destroy");
    }
}

function editMode() {
    var vm = $(document).data('viewmodel');
    vm.Par.Notes(vm.Par.Notes().replace(/<br\/>/g, '\n')); // convert HTML breaks back to newlines
    vm.ViewMode('edit');
    $('#EffectiveDate').datepicker(
        {
            onClose: function (dateText, inst) {
                var ac = vm.Par.ActionCode();
                if (ac == 'U0' || ac == 'U6') {
                    // if par type is new hire or rehire, copy EffectiveDate to Appt, CtsHireDates
                    $('#AppointmentDate').datepicker("setDate", dateText);
                    $('#CtsHireDate').datepicker("setDate", dateText);
                }
            }
        }
    );
    createDatePickers();
    if ($('#par-detail').hasClass('ui-accordion')) { // skip if in print/html mode
        var activePanel = $('#par-detail').accordion('option', 'active');
        $('#par-detail').accordion('destroy').accordion({
            active: activePanel,
            animated: 'swing',
            collapsible: true,
            heightStyle: 'content'
        });
    }
}

function init() {
    // this line must be in server template:
    //var vm = ko.mapping.fromJS(@Html.Raw(Json.Encode(Model)));

    // ValErr should now be initialized server-side
    // initialze validation object:
    //var ve = {};
    //for (var k in vm.Par) {
    //    if (ko.isObservable(vm.Par[k])) {
    //        ve[k] = '';
    //    }
    //}
    //vm.ValErr = ko.mapping.fromJS(ve);
    //var ve = vm.ValErr(); // this is not being mapped, for some reason.
    // format dates...
    formatDates();
    function formatDates() {
        // null Date is returned as "/Date(-62135568000000)/" (1/1/1); changed to Date?
        vm.Par.EffectiveDate(formatJsonDate(vm.Par.EffectiveDate()));
        vm.Par.ApptPayScaleEffectiveDate(formatJsonDate(vm.Par.ApptPayScaleEffectiveDate()));
        vm.Par.ApptPayNextIncrease(formatJsonDate(vm.Par.ApptPayNextIncrease()));
        vm.Par.ApptStepMEligibleDate(formatJsonDate(vm.Par.ApptStepMEligibleDate()));
        vm.Par.ContractEndDate(formatJsonDate(vm.Par.ContractEndDate()));
        vm.Par.EmpDOB(formatJsonDate(vm.Par.EmpDOB()));
        vm.Par.EmpDS01_Anniversary(formatJsonDate(vm.Par.EmpDS01_Anniversary()));
        vm.Par.EmpDS02_Appointment(formatJsonDate(vm.Par.EmpDS02_Appointment()));
        vm.Par.EmpDS03_CtsHire(formatJsonDate(vm.Par.EmpDS03_CtsHire()));
        vm.Par.EmpDS04_PriorPid(formatJsonDate(vm.Par.EmpDS04_PriorPid()));
        vm.Par.EmpDS05_Seniority(formatJsonDate(vm.Par.EmpDS05_Seniority()));
        vm.Par.EmpDS07_UnbrokenService(formatJsonDate(vm.Par.EmpDS07_UnbrokenService()));
        vm.Par.EmpDS09_VacLeaveFrozen(formatJsonDate(vm.Par.EmpDS09_VacLeaveFrozen()));
        vm.Par.EmpDS18_PersonalHolidayElg(formatJsonDate(vm.Par.EmpDS18_PersonalHolidayElg()));
        vm.Par.EmpDS26_PersonalLeaveElg(formatJsonDate(vm.Par.EmpDS26_PersonalLeaveElg()));
    }
    //vm.Par.Notes(vm.Par.Notes().replace(/\n/g, "<br/>")); // convert newlines to HTML breaks for read-mode
    $(document).data('viewmodel', vm); // attach vm to document as quasi-global
    //alert('applying...');
    ko.applyBindings(vm, $('#par-detail')[0]);
    //editMode(); // default to edit mode.
    $('#par-detail').accordion({
        active: 0, // expand panel 0 by default.
        animated: 'swing',
        collapsible: true,
        heightStyle: 'content'
    });
    $('#ContractTypeList').change(function () {
        setContractEnd($(this).val());
    });
    // attach this *after* initial selection.
    $('#EmpList').change(function () {
        getParDetailWithEmployee()
    });
    editMode();
    updateStatusBars();
    $('#par-detail').show();
}

function setContractEnd(contractType) {
    var vm = $(document).data('viewmodel');
    var efd = $('#EffectiveDate').datepicker('getDate');
    if (efd) {
        if (contractType.match(/(02|03|04|05|10|11|12|21|22)/)) {
            if (efd.getMonth() > 5) {
                efd.setFullYear(efd.getFullYear() + 1);
            }
            var newMonth = (6 + efd.getMonth()) % 12;
            efd.setMonth(newMonth);
            vm.Par.ContractEndDate(formatJsonDate('' + efd.getTime()));
        }
        else if (contractType.match(/23/)) {
            // WMS Review: 12 months
            efd.setFullYear(efd.getFullYear() + 1);
            vm.Par.ContractEndDate(formatJsonDate('' + efd.getTime()));
        }
        else {
            vm.Par.ContractEndDate(null);
        }
    }
    else {
        alert('set PAR EffectiveDate to auto-calculate contract end date.');
    }
}

// inspired by ISO8601 parser found here:
// http://n8v.enteuxis.org/2010/12/parsing-iso-8601-dates-in-javascript/
function parseDate(s) {
    var re = /(\d{4})\-(\d\d)\-(\d\d)/;
    var ymd = s.match(re);
    return new Date(ymd[1], ymd[2] - 1, ymd[3]);
}