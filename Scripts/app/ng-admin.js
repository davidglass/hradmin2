﻿// Angular Modules:

// bulkupdate module:
//var bum = angular.module('bulkupdate', []); // routing module is separate from angular.min.js...
// admin module:
var am = angular.module('admin', []); // routing module is separate from angular.min.js...
am.controller('BulkUpdateController', function ($scope, $http, $timeout) {
    $scope.appRoot = appRoot;
    $scope.SelectedPosIds = [];
    $scope.UpdateVals = new PosUpdateVals();
    $scope.update = function () {
        //        alert('$scope.update() called.');
        // *NOTE*, getElementById is being used since jQuery is omitted (jqLite only):
        var s = document.getElementById('SupervisorEmpId');
        var p = document.getElementById('Position');
        var posturl = $scope.appRoot + 'api/position/' + $scope.SelectedPosIds.join(',');
        $http({ method: 'PUT', url: posturl, data: $scope.UpdateVals })
            .success(function (updated) {
                $timeout(function () {
                    // *now set dropdown labels to new super: * //
                    for (var i = 0; i < p.length; i++) {
                        if (p[i].selected) {
                            var pt = p[i].text;
                            var se = pt.lastIndexOf(', SE:');
                            if (se > -1) {
                                p[i].text = pt.substring(0, se) + (s.selectedIndex > 0 ? ', SE:' + s.options[s.selectedIndex].text : '');
//                                p[i].style.backgroundColor = (s.selectedIndex > 0 ? "yellow" : undefined);
                            }
                            else if (s.selectedIndex > 0) {
//                                p[i].style.backgroundColor = "yellow";
                                p[i].text += ', SE:' + s.options[s.selectedIndex].text;
                            }
                        }
                    }
                    // just reloading for now...TODO (maybe): modify viewmodel and rebind.  Currently selection lists are generated server-side for best performance.
    //                window.open($scope.appRoot + 'bulkupdate?' + Math.random(), '_self'); // Math.random() added as cache-buster.
                    //                item.IsUpdating = false;
                    // TODO: time call only add delay if < 100ms:
                }, 200); // 300ms timeout to ensure update is noticed...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });
    }
});

// CostCenter module:
//var m = angular.module('admin', []); // routing module is separate from angular.min.js...
//var m = angular.module('costcenter', []); // routing module is separate from angular.min.js...
am.controller('CostCenterController', function ($scope, $filter, $http, $timeout) {

    // TODO: move shared properties to base scope...

    $scope.appRoot = appRoot;
    $scope.reverseOrder = true;
    $scope.sortField = "Positions";

    $scope.load = function () {
        $scope.costcenters = rows;
        $scope.newItem = new CostCenter();
    }

    $scope.load();

    $scope.isActive = function (item) {
        return item.IsActive ? true : false; // return false for undefined, etc.
    }

    $scope.isAdding = function (item) {
        return item.IsAdding;
    }

    $scope.isDeleting = function (item) {
        return item.IsDeleting ? true : false; // return false for undefined, etc.
    }

    $scope.isUpdating = function (item) {
        return item.IsUpdating;
    }

    $scope.wasAdded = function (item) {
        return item.WasAdded;
    }

    $scope.addItem = function (newItem) {
        newItem.CharCode = newItem.Id; // char(4) version of CostCenter...
        var ms = document.getElementById('ManagerEmpId');
        newItem.ManagerName = ms.options[ms.selectedIndex].text; //ignored at server
        var posturl = $scope.appRoot + 'api/costcenter';
//        var addedItem = angular.copy(newItem);
        newItem.IsAdding = true;
        $http({ method: 'POST', url: posturl, data: newItem })
            .success(function (created) {
                created.ManagerName = newItem.ManagerName;
                created.Positions = 0;
                created.Vacancies = 0;
                newItem.IsAdding = false;
                $scope.costcenters.unshift(created);
                // sets CSS class...animated fade out requires CSS3 so not in IE8.
                created.WasAdded = true;
                $timeout(function () {
                    created.WasAdded = false; // removes bound CSS class for new add
                    $scope.newItem = new CostCenter();
                }, 400);
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
                newItem.IsAdding = false;
                //                console.log('post error: ' + angular.toJson(data));
            });
    }

    $scope.deleteItem = function (item) {
        if (!confirm('this will permanently delete cost center # ' + item.Id)) {
            return false;
        }
        var posturl = $scope.appRoot + 'api/costcenter/' + item.Id
        item.IsDeleting = true;
        $http({ method: 'DELETE', url: posturl })
            .success(function (deleted) {
                // this seems to succeed silently for IE...
                $timeout(function () {
                    $scope.costcenters.splice($scope.costcenters.indexOf(item), 1); // remove (delete) item
                }, 300); // 300ms timeout to ensure deletion is visible...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });
    }

    // TODO: set $scope.sortField instead of passing as arg (now using for sort toggle logic...)
    $scope.sort = function (colname) {
        if (colname == $scope.sortField) { // only reverse direction on second click
            $scope.reverseOrder = !$scope.reverseOrder;
        }
        else {
            // default to ascending order unless Positions or Vacancies, then descending...
            $scope.reverseOrder = (colname == "Positions" || colname == "Vacancies");
        }
        //        alert('sorting by ' + colname);
        // TODO: sort function for non-simple string sorts:
        var sorted = $filter('orderBy')($scope.costcenters, colname, $scope.reverseOrder); // simple string sort, reversing
        $scope.costcenters = sorted;
        $scope.sortField = colname;
    }

    $scope.updateItem = function (item, $event) {
        $event.stopPropagation(); // prevent row selection from click propagation to <tr>
        item.IsActive = !item.IsActive; // need actual checkstate if using this for more complete update.
        var posturl = $scope.appRoot + 'api/costcenter/' + item.Id;
        item.IsUpdating = true;
        $http({ method: 'PUT', url: posturl, data: item })
            .success(function (updated) {
                $timeout(function () {
                    item.IsUpdating = false;
                }, 300); // 300ms timeout to ensure update is noticed...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });
    }

});

am.controller('ExpirationController', function ($scope, $http, $timeout, $location, $rootScope) {
    $scope.path = '';
    $rootScope.$on('$locationChangeSuccess', function (event) {
        // TODO: single (flat) hash of all nodes by id
        $scope.path = $location.url(); // e.g. "/Contracts, /Delegations"
        switch ($scope.path) {
            case '/ExpiringContract':
                // *NOTE*, this expects by convention to find ExpiringContractController ApiController route on server:
                // TODO: client-side caching layer for data that changes only daily
                $http.get(appRoot + 'api' + $scope.path).success(function (data) {
                    $scope.ExpiringContracts = data || [];
                });
                return;
            case '/ExpiringDelegate':
                // *NOTE*, this expects by convention to find ExpiringContractController ApiController route on server:
                // TODO: client-side caching layer for data that changes only daily
                $http.get(appRoot + 'api' + $scope.path).success(function (data) {
                    $scope.ExpiringDelegates = data || [];
                });
                return;
            default: return;
        }
    });
});

// jobclass module:
//var m = angular.module('main', ['ngRoute']); // routing module is separate from angular.min.js...
//var m = angular.module('jobclass', []); // routing module is separate from angular.min.js...
//m.controller('MainController', function ($scope, $timeout, $http) {
am.controller('JobClassController', function ($scope, $http, $timeout, $filter) {
    // properties:
    $scope.appRoot = appRoot;
    //$scope.classcodes = rows; // embedded server-side by MVC
    $scope.reverseOrder = true;
    $scope.sortField = "Positions";

    $scope.hasPositions = function (item) {
        return item.TotalPositions;
    }

    $scope.isActive = function (item) {
        return item.IsActive;
    }

    $scope.isAdding = function (item) {
        return item.IsAdding;
    }

    $scope.isDeleting = function (item) {
        return item.IsDeleting;
    }

    $scope.isUpdating = function (item) {
        return item.IsUpdating;
    }

    $scope.isSelected = function (item) {
        return item.IsSelected;
    }

    $scope.isEven = function (index) {
        return (index % 2);
    }

    $scope.wasAdded = function (item) {
        return item.WasAdded;
    }

    // methods / event handlers:
    $scope.addItem = function (newItem) {
        // could pre-validate that newItem.ClassCode and JobTitle don't match any existing,
        // or do it in the db...
        var posturl = $scope.appRoot + 'api/jobclass';
        //var addedItem = angular.copy(newItem);
        newItem.IsAdding = true;

        // FAKE POST:
        //$timeout(function () {
        //    //            $scope.classcodes[0].IsAdding = false;
        //    newItem.IsAdding = false;
        //    $scope.classcodes.unshift(addedItem);
        //    $scope.newItem = new ClassCode();
        //    console.log('addedItem = ' + angular.toJson(addedItem));
        //}, 500);
        // TODO persist to server..., copy generated Id to addedItem.

        // explicitly POSTing since $post implicitly triggers OPTIONS, fails.
        $http({ method: 'POST', url: posturl, data: newItem })
            .success(function (created) {
                newItem.IsAdding = false;
                $scope.classcodes.unshift(created); // need Id if immediately deleting / updating.
                // sets CSS class...animated fade out requires CSS3 so not in IE8.
                created.WasAdded = true;
                $timeout(function () {
                    created.WasAdded = false; // removes bound CSS class for new add
                }, 500);
                $scope.newItem = new ClassCode();
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
                newItem.IsAdding = false;
            });

    }

    //    $scope.checkChanged = function (item) {
    // *NOTE*, this handles click event, *before* change actually occurs.  So IsActive should be opposite.
    // TODO: handle update click here on item edit.
    $scope.updateItem = function (item, $event) {
        $event.stopPropagation(); // prevent row selection from click propagation to <tr>
        item.IsActive = !item.IsActive; // need actual checkstate if using this for more complete update.
        var posturl = $scope.appRoot + 'api/jobclass/' + item.Id;
        item.IsUpdating = true;
        $http({ method: 'PUT', url: posturl, data: item })
            .success(function (updated) {
                $timeout(function () {
                    item.IsUpdating = false;
                }, 200); // 300ms timeout to ensure update is noticed...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });
    }

    //$scope.checkChanged = function (item, $event) {
    //    //        $event.stopPropagation(); // prevent row selection (changes yellow async-updating to blue selection)
    //    // TODO: async op, persistence...this just changes client-side model.
    //    $event.stopPropagation(); // prevent row selection from click propagation to <tr>
    //    item.IsUpdating = true; // TODO: prefix transient properties like this with $ to strip from persistence
    //    // simulate 400ms async persistence call:

    //    // fake update:
    //    $timeout(function () {
    //        item.IsActive = !item.IsActive;
    //        item.IsUpdating = false;
    //    }, 500);

    //    //   console.log("changed " + item.JobTitle + " IsActive to " + item.IsActive);
    //}

    $scope.deleteItem = function (item) {
        if (!confirm('this will permanently delete job class ' + item.LegacyCode )) {
            return false;
        }
        var posturl = $scope.appRoot + 'api/jobclass/' + item.Id;
        // *NOTE*, remove all console writes before running in IE or check for console existence first...
        //console.log('posturl: ' + posturl);
        item.IsDeleting = true;
        $http({ method: 'DELETE', url: posturl })
            .success(function (deleted) {
                $timeout(function () {
                    $scope.classcodes.splice($scope.classcodes.indexOf(item), 1); // remove (delete) item
                }, 300); // 300ms timeout to ensure deletion is visible...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });

        // fake delete:
        //$timeout(function () {
        //    $scope.classcodes.splice($scope.classcodes.indexOf(item), 1); // remove (delete) item
        //}, 500);
    }

    // TODO: set $scope.sortField instead of passing as arg:
    $scope.sort = function (colname) {
        if (colname == $scope.sortField) { // only reverse direction on second click
            $scope.reverseOrder = !$scope.reverseOrder;
        }
        else {
            // default to ascending order unless Positions or Vacancies, then descending...
            $scope.reverseOrder = (colname == "TotalPositions" || colname == "Vacancies");
        }
        //        alert('sorting by ' + colname);
        // TODO: sort function for non-simple string sorts:
        var sorted = $filter('orderBy')($scope.classcodes, colname, $scope.reverseOrder); // simple string sort, reversing
        $scope.classcodes = sorted;
        $scope.sortField = colname;
    }

    // *NOTE*, ng-click on <tr> apparently fails in IE... (v9, at least), so selection only works in Chrome.
    $scope.toggleSelection = function (item) {
        item.IsSelected = !item.IsSelected;
    }

    $scope.openPositionList = function (item, q) {
        var qparams = ['ClassCode=' + item.LegacyCode];
        // add additional query params beside those in jobclass item (e.g. IsVacant)
        for (var k in q) {
            qparams.push(k + '=' + q[k]);
        }
        window.open($scope.appRoot + 'position?' + qparams.join('&'), '_self');
    }

    $scope.load = function () {
        $scope.newItem = new ClassCode();
        // rows JSON is currently rendered server-side with rest of page, stored in global rows var.
        // in future, could be loaded client-side if necessary (adds latency).
        $scope.classcodes = rows;
    }
    $scope.load();
});

am.controller('PositionController', function ($scope, $filter, $http, $timeout) {
    $scope.reverseOrder = false;
    $scope.sortField = "OuName";

    $scope.isEven = function (index) {
        return (index % 2);
    }

    $scope.sort = function (colname) {
        if (colname == $scope.sortField) { // only reverse direction on second click
            $scope.reverseOrder = !$scope.reverseOrder;
        }
        else {
            // default to ascending:
            $scope.reverseOrder = false;
        }
        //        alert('sorting by ' + colname);
        // TODO: sort function for non-simple string sorts:
        var sorted = $filter('orderBy')($scope.positions, colname, $scope.reverseOrder); // simple string sort, reversing
        $scope.positions = sorted;
        $scope.sortField = colname;
    }
    $scope.positions = rows; // embedded server-side by MVC

});

am.controller('UserMgtController', function ($scope, $http, $timeout) {
    $scope.appRoot = appRoot;
    $scope.status = "";
    $scope.updating = false;
    $scope.isUpdating = function () {
        return $scope.updating;
    }
    $scope.emp = {}; // Id bound to selected emp...
    $scope.user = new AppUser();
    $scope.changeUser = function () {
        if (empusers[$scope.emp.Id]) {
            $scope.user = empusers[$scope.emp.Id];
        }
        else {
            // TODO: attempt AD insert automatically after running ADExtract...this error should only occur for special cases
            // (e.g. no match on Employee ID nor First+Last)
            alert('no user account found for selected employee...contact ATS for help.');
        }
    }
    $scope.updateUserRole = function () {
        //alert('updating selected user: ' + angular.toJson($scope.user));
        var posturl = $scope.appRoot + 'api/appuser/' + $scope.user.Id;
        $scope.status = "updating...";
        $scope.updating = true;
        $http({ method: 'PUT', url: posturl, data: $scope.user })
            .success(function (updated) {
                $timeout(function () {
                    $scope.status = ""; // TODO: set error if error...
                    $scope.updating = false;
//                    alert('user role was updated.');
//                    item.IsUpdating = false;
                }, 250); // 250ms timeout to ensure update is noticed...
            })
            .error(function (data, status, errors, config) {
                if (data.error) {
                    $scope.status = "update failed."; // TODO: set error if error...
                    alert('ERROR: ' + data.error);
                };
            });
    }
});

// TODO: remove "var m"...it is superfluous and overwritten throughout this script anyway.
// just call angular.module() and ignore return val, call .controller directly,
// or only have a single module with multiple controllers.
am.controller('ImpersonationController', function ($scope, $http, $timeout) {
    $scope.appRoot = appRoot;
    $scope.status = "";
    $scope.imp = {
        'ImpersonatingUserId': impid
    };

    $scope.changeUser = function () {
        var posturl = $scope.appRoot + 'api/impersonation';
        $scope.status = "updating...";
        $scope.updating = true;
        $http({ method: 'PUT', url: posturl, data: $scope.imp })
            .success(function (updated) {
                $timeout(function () {
                    $scope.status = ""; // TODO: set error if error...
                    $scope.updating = false;
                }, 250); // 250ms timeout to ensure update is noticed...
                location.reload(true);
            })
            .error(function (data, status, errors, config) {
                if (data.error || data.Message) {
                    $scope.status = "update failed."; // TODO: set error if error...
                    // *NOTE*, .error is set explicitly in ApiController, .Message may be set by AuthFilter.
                    alert('ERROR: ' + (data.error || data.Message));
                };
            });
    }
});

// Viewmodels:
function AppUser() {
    this.Id = undefined;
    this.EmployeeId = undefined;
    this.RoleId = 0;
}

function ClassCode() {
    this.Id = 0;
    this.HrmsJobId = "";
    this.JobTitle = "";
    this.LegacyCode = "";
//    this.FilledPositions = 0;
    this.TotalPositions = 0;
    this.Vacancies = 0;
    this.IsActive = true;
    this.WasAdded = false;
}

function CostCenter() {
    this.Id = undefined;
    this.IsActive = true;
    this.DescShort = "";
    this.DescLong = "";
    this.ManagerEmpId = undefined;
    this.ManagerName = "";
    this.WasAdded = false;
    this.IsDeleting = false;
    //this.Positions = 0;
    //this.Vacancies = 0;
}

function PosUpdateVals() {
    this.SupervisorEmpId = 0;
}
