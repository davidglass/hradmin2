﻿angular.module('hr', ['kendo.directives', 'ngRoute'])
.controller('SpaController', function ($scope, $filter, $http, $location, $rootScope, $route, $routeParams, $timeout) {
    document.title = 'HR SPA';
    $scope.formatJsonDate = function (d) { return formatJsonDate(d) }; // inherited by child controllers.
    $scope.startTime = new Date().getTime();
    // hack for IE8 missing window.location.origin:
    // http://tosbourn.com/a-fix-for-window-location-origin-in-internet-explorer/
    if (!window.location.origin) {
        window.location.origin = window.location.protocol + "//" + window.location.hostname + (window.location.port ? ':' + window.location.port : '');
    }
    var path = window.location.pathname;
    // find second slash index if exists:
    var second = path.indexOf('/', path.indexOf('/') + 1) + 1;
    $rootScope.appRoot = $scope.appRoot = window.location.origin + (path.substring(0, second || path.indexOf('/') + 1));
    //$scope.appRoot = appRoot;
    //$scope.appRoot = $location.protocol() + '://' + $location.host() + ($location.port() != 80 ? ':' + $location.port() : '') + appRoot;
    //$scope.showLoading = function () {
    //$scope.startTime = new Date().getTime();
    //    // TODO: use Kendo Alert here...
    ////    alert('loading view...');
    //};
})
.controller('EmployeeController', function ($scope, $rootScope, fetch) {
    $scope.vm = fetch.data;
    document.title = $scope.vm.EmpInfo.LastName + ", " + $scope.vm.EmpInfo.FirstName;
})
.controller('PdController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    $scope.vm = fetch.data; // *NOTE*, using separate PdListController.
    // TODO: Angular directive to set CurrentJobName via JobClasses...???
    // this all comes from using EF to pull PD directly from table.
    // other option is additional server-side lookup.
    // TODO: create SQL Views for simple read-only joins like this:
    // global HACK!!!:  TODO: pull JobClasses from API, not MVC-embedded
    var jobclasses = pdfDetailVm.JobClasses; //
    $scope.currentJobName = '';
    for (var c in jobclasses) {
        if (jobclasses[c].Value && jobclasses[c].Value == $scope.vm.Description.xCurrentJobId) {
            $scope.currentJobName = jobclasses[c].Text;
            break;
        }
    }
    document.title = "PDF #" + $scope.vm.Description.Id + " Detail";
    //alert('vm: ' + JSON.stringify($scope.vm.Duties));
    //    $scope.$routeParams = $routeParams; // pdfId is passed in $routeParams...

    // TODO: look up Pdf Detail from $routeParams.pdfId... for now just copying from parent scope.
    //alert('root scope working title: ' + $scope.$parent.vm.Pos.WorkingTitle);

    // TODO: this is stupid little demo...replace with actual Pdf detail lookup in RESOLVE of route.
    //var pdfs = $scope.$parent.vm.Pdfs;
    //for (var i = 0; i < pdfs.length; i++) {
    //    //if (pdfs[i].Id == $routeParams.pdfId) {
    //    if (pdfs[i].Id == id) {
    //        $scope.vm = pdfs[i]; // set vm to matching pdf record.
    //    }
    //}

    //alert('dvm:' + JSON.stringify(pdfDetailVm));
    $scope.options = {
        AsyncNotification: {
            show: onShowAsyncNote,
            stacking: "down",
            templates: [
                {
                    type: 'async-op',
                    template: $("#asyncOpTemplate").html()
                }
            ]
        },
        PositionType: {
            dataSource: [
                { Id: "C", Name: "Classified" },
                { Id: "X", Name: "Exempt" },
                { Id: "W", Name: "WMS" }
            ],
            dataValueField: "Id",
            dataTextField: "Name",
            optionLabel: " - select - "
        },
        JobClass: {
            dataSource: pdfDetailVm.JobClasses,
            dataValueField: "Value",
            dataTextField: "Text",
            optionLabel: " - select - ",
            template: '<span class="options">#: Text#</span>'
        },
        // *note*, these are options for Kendo-UI widgets, not <option>s:
        // also note, DutyEditor should be data-bound to CurrentlyEditingDuty or similar in VM.
        DutyEditor: {
            encoded: false, // else Angular double-encodes
            tools: [
                "bold", "italic", "underline",
                "insertUnorderedList",
                "createLink", "unlink",
                // see SiteBasic.css, .k-save for example of setting icon from sprite.
                {
                    name: "save",
                    tooltip: "save changes",
                    exec: function (e) { // e is button click event...not sure whether we need it...
                        $scope.saveDuty();
                        //alert('saving selected duty info [TODO]...' + $scope.selectedDutyId);
                        //var ed = $(this).data("kendoEditor");
                    },
                    // templates can also be specified with variable substitution as text/x-kendo-template...
                    //template: "<button class='k-button'><span class='k-icon k-update'>&nbsp;</span>Save</button>"
                }
            ],
            // trying to fix bug where initially editing newitem, then selecting existing item disables editor:
            // seems to work, even though it is theoretically re-focusing an item that just lost focus.
            change: function () {
                this.focus();
            }
        },
        DutyPanelBar: {
            animation: {
                collapse: {
                    duration: 300
                    //effects: 'fadeOut'
                },
                expand: {
                    duration: 300,
                    //                effects: 'expandVertical fadeIn'
                    effects: 'expandVertical'
                }
            },
            collapse: function (pb) {
                // exit edit mode on collapse...
                if ($scope.editingDutyId == $(pb.item).attr('id')) {
                    $scope.$apply(function () {
                        $scope.editingDutyId = 0;
                    });
                }
            },
            //expand: function (pb) {
            //    $scope.$apply(function () {
            //        $scope.editingDutyId = $(pb.item).attr('id');
            //    });
            //},
            expandMode: "multiple",
            //activate: function () {
            //    $scope.hideTasks(); // hide any floating duty task "tooltips"
            //},
            select: function (pb) {
                $scope.$apply(function () {
                    $scope.editingDutyId = $(pb.item).attr('id');
                    $scope.selectedDutyId = $(pb.item).attr('id');
                    $scope.selectedDutyIndex = $(pb.item).index();
                });
            }
        },
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            expand: function (e) {
                $('#AsyncMessage').hide(); // yes, it's DOM manipulation in the controller.  mea culpa!
                $('#PdForm').css('display', 'block');
            },
            expandMode: "single"
        },
        PdEditor: { // save button calls savePD(), not saveDuty().  *NOTE*, removed ng-change event handler trigger that was also calling savePD().
            encoded: false, // else Angular double-encodes
            tools: [
                "bold", "italic", "underline",
                "insertUnorderedList",
                "createLink", "unlink",
                // see SiteBasic.css, .k-save for example of setting icon from sprite.
                {
                    name: "save",
                    tooltip: "save changes",
                    exec: function (e) {
                        // e is button click event...not sure whether we need it...
                        // *YES*, must cancel propagation to prevent save on blur...
                        $scope.savePD();
                    },
                    // templates can also be specified with variable substitution as text/x-kendo-template...
                    //template: "<button class='k-button'><span class='k-icon k-update'>&nbsp;</span>Save</button>"
                }
            ],
            // trying to fix bug where initially editing newitem, then selecting existing item disables editor:
            // seems to work, even though it is theoretically re-focusing an item that just lost focus.
            change: function () {
                this.focus();
            }
        }
    };

    $scope.asPdf = function () {
        var pdForm = {
            C: 'WGS',
            W: 'WMS',
            X: 'Exempt'
        };
        // TODO: configurable SSRS server
        var pdfloc = 'http://test-reporting.cts.wa.gov/reportserver?/Human Resources/Print/' + pdForm[$scope.vm.Description.PositionTypeCXW] + '&rs:Command=Render&rs:Format=pdf&Id=' + $scope.vm.Description.Id + '&rs:ClearSession=true';
        //alert('pdfloc:' + pdfloc);
        window.location = pdfloc;
    };

    $scope.collapseAll = function () {
        $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse('li');
    };

    $scope.collapseDuties = function () {
        $scope.dutiesPanelBar.options.expandMode = "single";
        $scope.dutiesPanelBar.collapse('li');
    };

    $scope.collapseQuals = function () {
        $scope.qualsPanelBar.options.expandMode = "single";
        $scope.qualsPanelBar.collapse('li');
    };

    $scope.collapseWx = function () {
        $scope.wxPanelBar.options.expandMode = "single";
        $scope.wxPanelBar.collapse('li');
    };

    $scope.createDuty = function () {
        // TODO: get Id or complete Duty object from server insert or use client-side GUID generator...
        // faking with random() for now.
        var newDuty = {
            //            Id: Math.floor(Math.random() * (2000000000)),
            PositionDescriptionId: $scope.vm.Description.Id,
            Duty: 'New Duty',
            TasksHtml: '<ul><li>Sample Task 1</li><li>Sample Task 2</li></ul>',
            TimePercent: 0
        };

        //        var posturl = appRoot + 'api/PositionDuty/';
        var posturl = $scope.appRoot + 'api/PositionDuty/';
        $scope.asyncNotification.show({ message: 'Creating Duty...' }, 'async-op'); // info, success, warning, error...
        try {
            $scope.asyncOp = true;
            $scope.asyncMessage = "creating duty";
            $http({ method: 'POST', url: posturl, data: newDuty })
            .success(function (data) {
                // $timeout should automatically call $apply:
                $timeout(function () {
                    $scope.asyncNotification.hide();
                    $scope.vm.Duties.push(data);
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncNotification.hide();
                alert('ERROR:\n' + data.Message);
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    $scope.createPar = function () {
        var pd = $scope.vm.Description;
        //alert('Creating CreatePosition PAR for PD# ' + $scope.vm.Description.Id);
        // TODO: test that parData is not null and is an object...
        var posturl = $scope.appRoot + 'api/par';
        var parData = {
            ActionCode: pd.PositionId ? 'P1' : 'P0', // Update or Create Position...
            PosWorkingTitle: pd.WorkingTitle,
            PositionId: pd.PositionId, // undefined for CreatePosition PARs...
            PosJobId: pd.ProposedJobId,
            PosPersonnelSubArea: (pd.PositionTypeCXW == 'W' ? '0002' :
                (pd.PositionTypeCXW == 'X' ? '0003' : null)), // Classified PSA is indeterminate, so null here.
            PosIsSupervisor: pd.IsSupervisor,
            PosPointValue: pd.xProposedJvacEvalPts,
            PdfId: pd.Id
        };
        $timeout(function () {
            $scope.asyncNotification.show({ message: 'Creating PAR...' }, 'async-op'); // info, success, warning, error...
        }); // *this* timeout is to ensure Angular $apply.  Next one is for delay to ensure visibility before redirect.
        $timeout(function () {
            try {
                $http({ method: 'POST', url: posturl, data: parData })
                .success(function (d) {
                    $scope.asyncNotification.hide();
                    // TODO: set Pd link automatically via extra param to ParController...
                    $scope.vm.Description.LinkedParId = d.Par.Id
//                    alert('created Par # ' + d.Par.Id);
                })
                .error(function (d, status, errors, config) {
                    $scope.asyncNotification.hide();
                    alert('ERROR\n' + d.Status || d.error);
                });
            }
            catch (xcp) {
                var err = xcp;
                alert('ERROR: ' + JSON.stringify(xcp));
            }
        }, 300);
    }

    // this passes the index of the clicked item, more reliable than selectedDutyIndex...
    $scope.deleteDuty = function (index, evt) {
        var d = $scope.vm.Duties[index];
        evt.stopPropagation(); // prevent toggling expand/collapse...
        if (confirm('this will delete duty #' + d.Id + '\n(' + d.Duty + ')')) {
            $scope.asyncNotification.show({ message: 'Deleting Duty...' }, 'async-op'); // info, success, warning, error...
            //            var posturl = appRoot + 'api/PositionDuty/' + d.Id;
            var posturl = $scope.appRoot + 'api/PositionDuty/' + d.Id;
            try {
                $http({ method: 'DELETE', url: posturl })
                    .success(function (deleted) {
                        $timeout(function () {
                            $scope.asyncNotification.hide();
                            $scope.vm.Duties.splice(index, 1);
                        }, 300); // 300ms timeout to ensure deletion is visible...
                    })
                    .error(function (data, status, errors, config) {
                        $scope.asyncNotification.hide();
                        alert('ERROR:\n' + data.Message);
                    });
            }
            catch (xcp) {
                alert('$http delete failed.');
            }
        }
    };

    $scope.deletePD = function () {
        //        var posturl = appRoot + 'api/PositionDescription/' + $scope.vm.Description.Id;
        var posturl = $scope.appRoot + 'api/PositionDescription/' + $scope.vm.Description.Id;
        // TODO: allow deleting only PDs in "created" approval state...
        if (!confirm('This will permanently delete Position Description #' + $scope.vm.Description.Id
            + ', along with any associated Duty allocations.  Upon success, you will be redirected to the list of Position Descriptions in your perspective.')) { return false; };
        // TODO: add Kendo Notification of async op status
        $scope.asyncNotification.show({ message: 'Deleting PD...' }, 'async-op'); // info, success, warning, error...
        $http({ method: 'DELETE', url: posturl })
        .success(function (deleted) {
            // this seems to succeed silently for IE...
            $timeout(function () {
                $scope.asyncNotification.hide();
//                alert("Deleted PD #" + $scope.vm.Description.Id + ".");
                // redirect to PD list for position, use Angular $location to keep routing happy:
                $location.path('/pd');
//                $location.path('/position/' + $scope.vm.Description.PositionId + '/pd');
            }, 300); // timeout to ensure deletion is visible...
        })
        .error(function (data, status, errors, config) {
            $scope.asyncNotification.hide();
            alert('ERROR:\n' + data.Message);
        });
    };

    $scope.dutyCheckKey = function ($event) {
        if ($event.which == 13) { // pressed return
            $scope.editingDutyId = 0;
        }
    };

    $scope.dutyPctSum = function () {
        duties = $scope.vm.Duties;
        var sum = 5; // 5% reserved for "Other"
        for (var i = 0; i < duties.length; i++) {
            if (duties[i].TimePercent > 0) {
                sum += new Number(duties[i].TimePercent);
            }
        }
        return sum;
    };

    //$scope.editDuty = function (index, evt) {
    //    $scope.editingDuty = $scope.vm.Duties[index];
    //    if (evt.stopPropagation) {
    //        evt.stopPropagation();
    //    }
    //    else {
    //        evt.stopImmediatePropagation();
    //    }
    //};

    $scope.editingDutyId = 0;
    //= null;

    $scope.expandAll = function () {
        // *NOTE, panelbar gets replaced on re-render, thus caching as separate property here.
        $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand('li');
    };

    $scope.expandDuties = function () {
        $scope.dutiesPanelBar.options.expandMode = "multiple";
        $scope.dutiesPanelBar.expand('li');
    };

    $scope.expandQuals = function () {
        $scope.qualsPanelBar.options.expandMode = "multiple";
        $scope.qualsPanelBar.expand('li');
    };

    $scope.expandWx = function () {
        $scope.wxPanelBar.options.expandMode = "multiple";
        $scope.wxPanelBar.expand('li');
    };

    $scope.hideTasks = function () {
        $('#DutyTasksPreview').hide();
    };

    $scope.isDutySelected = function (item) {
        return ($scope.selectedDutyId && $scope.selectedDutyId == item.Id);
    };

    $scope.pctkeydown = function ($event) {
        // allow only digits (48-57, 96-105 keypad), backspace (8), delete(46), cursor left (37), right (39), tab (9), enter(13)
        if (!(
            ($event.which > 47 && $event.which < 58)
            || ($event.which > 95 && $event.which < 106)
            || $event.which == 8
            || $event.which == 9
            || $event.which == 13
            || $event.which == 37
            || $event.which == 39
            || $event.which == 46)
            || ($event.shiftKey && $event.which != 9) // allow shift+tab nav
            || $event.ctrlKey
            || $event.metaKey
            ) {
            $event.preventDefault();
        }
    };

    $scope.saveDuty = function () {
        // *NOTE*, with multi-expand accordion, save button may be clicked in non-selected panel...
        // TODO: handle that better, perhaps cause tool buttons to auto-select parent panel?
        // alternatively, could save changes to all Duties at once?...
        var d = $scope.vm.Duties[$scope.selectedDutyIndex];
        //        var posturl = appRoot + 'api/PositionDuty/' + d.Id;
        var posturl = $scope.appRoot + 'api/PositionDuty/' + d.Id;
        try {
            $scope.asyncNotification.show({ message: 'Saving Duty...' }, 'async-op'); // info, success, warning, error...
            $http({ method: 'PUT', url: posturl, data: d })
            .success(function (data) {
                $timeout(function () {
                    $scope.asyncNotification.hide();
                }, 600);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncNotification.hide();
                alert('ERROR:\n' + data.Message);
                //if (data.error) { alert('ERROR: ' + data.error); };
                //$scope.asyncOp = false;
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    $scope.savePD = function (cb) {
        // *NOTE*, with multi-expand accordion, save button may be clicked in non-selected panel...
        // TODO: handle that better, perhaps cause tool buttons to auto-select parent panel?
        // alternatively, could save changes to all Duties at once?...
        var pd = $scope.vm.Description;
        //        var posturl = appRoot + 'api/PositionDescription/' + pd.Id;
        var posturl = $scope.appRoot + 'api/PositionDescription/' + pd.Id;
        try {
            //$scope.asyncOp = true;  (not used now...)
            $scope.asyncNotification.show({ message: 'Saving PD...' }, 'async-op'); // info, success, warning, error...
            $http({ method: 'PUT', url: posturl, data: pd })
            .success(function (data) {
                $timeout(function () {
                    $scope.asyncNotification.hide();
                    if (cb) { cb(); }
                }, 300);
            })
            .error(function (data, status, errors, config) {
                $scope.asyncNotification.hide();
                alert('ERROR:\n' + data.Message);
                //alert('ERROR:\n' + (data.Message || data.error)); // Controller sets error, AuthFilter returns Exception directly with Message...(see BaseApiAuthAttribute)
//                if (data.error) { alert('ERROR: ' + data.error); };
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    //$scope.selectDuty = function (item, $event) {
    //    $scope.hideTasks();
    //    $scope.selectedDuty = item;
    //    // this seems necessary in IE8 for some reason, else new editor is opened disabled.
    //    $("#dutyEdit").data("kendoEditor").focus();
    //};

    //$scope.selectedDutyId = 1234;
    $scope.showTasks = function (item, $event) {
        if (item.TasksHtml) { // skip if blank
            // TODO: calculate correct display position, accounting for scroll positions, viewport space to left or right, etc.
            $('#DutyTasksPreview').html(item.TasksHtml);
            $('#DutyTasksPreview').css('left', $event.pageX + 16); // 16px right of cursor
            $('#DutyTasksPreview').css('top', $event.pageY + 16); // show 16px below cursor
            $('#DutyTasksPreview').fadeIn(200);
        }
    };
    // this is too early; template is not recompiled.  Just hardcoded Kendo widget classes into template, works.
    //$scope.$watchCollection('vm.Duties', function (newval, oldval) {
    //    // if new count > old count, rebuild panelbar...
    //    // *NOTE*, this is a hack since Kendo PanelBar ignores collection changes...
    //    if (newval.length > oldval.length) {
    //        //alert('item added...');
    //        //$scope.dutiesPanelBar = $('#DutiesPanelBar').kendoPanelBar($scope.options.DutyPanelBar);
    //    }
    //});

    $scope.updateApprovalTask = function (idx, status) {
        if (idx == 0 && status == 'Active') {
            // block / return false if form invalid...
            var valErrs = $scope.validate();
            if (valErrs.length) {
                alert('INPUT ERRORS:\n' + valErrs.join('\n'));
                return false;
            }
            else if (!confirm('Position Descriptions for current employees should be discussed with the employees prior to submission for HR review.  Click OK to continue submission.')) {
                return false;
            }
        }
        var statusAction = {
            'Active': 'Initiate',
            'Canceled': 'Skip',
            'Rejected': 'Reject',
            'Approved': 'Approve'
        };
        var cb = function () { // callback
            var t = $scope.vm.ApprovalTasks[idx]; // to avoid indexOf polyfill for IE8...
            //if (confirm('Approve?')) {
            // TODO: show asyncNotification here...
            // TODO: make save take callback
            //            $scope.save(function () {
            // TODO: bind Workflow status to completion of final step if displaying.
            //                t.ApprovalStatus = 'Approved';
            var updateTask = {};
            angular.copy(t, updateTask);
            updateTask.ApprovalStatus = status;
            $scope.asyncNotification.show({ message: 'updating Approval...' }, 'async-op'); // info, success, warning, error...
            $http({
                method: 'PUT',
                url: appRoot + 'api/ApprovalTaskNew/' + t.Id,
                data: updateTask
            })
            .success(function (d) { // save update returns only validation errors...
                $timeout(function () {
                    $scope.asyncNotification.hide();
                    t.CompletedDate = formatJsonDate(d.CompletedDate);
                    t.ApproverFirstName = d.FirstName;
                    t.ApproverLastName = d.LastName;
                    t.ApprovalStatus = status;
                    //$scope.refreshApprovalWorkflowVm();
                    if (idx < $scope.vm.ApprovalTasks.length && (status == 'Approved' || status == 'Canceled')) {
                        // set next task to active:
                        $scope.vm.ApprovalTasks[idx + 1].ApprovalStatus = 'Active';
                    }
                }, 300);
            })
            .error(function (data, status, headers, config, statusText) {
                $scope.asyncNotification.hide();
                //t.ApprovalStatus = origStatus;
                if (status == 500) {
                    alert('Error updating task status:\n' + data.Message);
                }
                else {
                    alert('Error on API call, status code ' + status);
                }
            });
        };
        if (confirm(statusAction[status] + '?')) {
            if (idx == 0 && status == 'Active') {
                $scope.savePD(cb);
            }
            else {
                cb();
            }
        }
    }

    $scope.validate = function () {
        var errs = [];
        // TODO: complete validation here, could call server, etc...
        if ($scope.dutyPctSum() != 100) {
            errs.push("Duty time allocation must sum to 100%");
        }
        return errs;
    }
})
//.controller('PdListController', function ($scope) {
//    document.title = "PD List";
//    //$scope.$on('rendered', function () {
//    //    $timeout(function () { // You might need this timeout to be sure its run after DOM render.
//    //        var now = new Date().getTime();
//    //        alert('rendered in ' + (now - $scope.startTime) + ' ms.');
//    //    }, 0, false);
//    //});
//    $scope.vm = vm;

//    $scope.options = {
//        Grid: {
//            columns: [
//                {
//                    field: "PositionId",
//                    template: function(d) {
//                        return "<a href='{{ appRoot }}/position/" + d.PositionId + "'>" + d.PositionId + "</a>"
//                    },
//                    title: "Position",
//                    width: "70px"
//                },
//                {
//                    field: "PositionType",
//                    title: "Type",
//                    width: "70px"
//                },
//                {
//                    field: "WorkingTitle",
//                    template: function (d) {
//                        return d.WorkingTitle + ' (' + (d.IncumbentLast ? d.IncumbentLast + ', ' + d.IncumbentFirst : 'VACANT') + ')';
//                    },
//                    title: "Current Title (Incumbent)",
//                    width: "350px"
//                },
//                //{
//                //    field: "PdId",
//                //    title: "Pos Desc",
//                //    width: "80px"
//                //},
//                {
//                    field: "EffectiveDate",
//                    //format: "{0:M/d/yyyy}",
//                    template: function(d) {
////                        return "<a href='#/pd/" + d.PdId + "'>" + kendo.toString($scope.formatJsonDate(d.EffectiveDate), 'd') + "</a>";
//                        return d.PdId ? "<a href='#/pd/" + d.PdId + "'>" + kendo.toString(d.EffectiveDateString, 'd') + "</a>"
//                            : '[NONE]';
//                    },
//                    title: "Last PD",
//                    //type: "date",
//                    width: '80px'
//                }
//            ],
//            dataSource: {
//                data: $scope.vm.PositionRows,
//                pageSize: 18
//                //                type: "json"
//            },
//            height: 550,
//            //pageable: true,
//            resizable: true,
//            scrollable: {
//                virtual: true
//            },
//            schema: {
//                model: {
//                    id: "PositionId"
//                    //,fields: {
//                    //    EffectiveDateString: {
//                    //        type: "date"
//                    //    }
//                    //}
//                }
//                // see schema.parse for custom pre-parsing of vm
//            },
//            sortable: true
//        }
//    };
//})
.controller('NewPdListController', function ($scope, $compile, $filter, $http, $location, $timeout, fetch) {
    //if (rq) { alert('runQuery = true'); }
    // TODO: include additional info (e.g. current user) in vm.
    // currently vm is a raw list.  result list should be a *property* of the vm.
    //$scope.timeout(function () {
    document.title = "Position Description Search";
    $scope.vm = fetch.data;
    // refresh data on browser history forward/back or search.
    $scope.$on('$locationChangeSuccess', function (event) {
        var q = $location.search();
        var qs = [];
        for (var k in q) {
            qs.push(k + '=' + q[k]);
        }
        $http({ // promise
            method: 'GET',
            url: appRoot + 'api/PositionDescription' + (qs.length ? '?' + qs.join('&') : '')
        }).success(function (d) {
            $timeout(function () {
                $scope.asyncNotification.hide();
            }, 200);
            $scope.vm = d;
            // not really using this link, using data() method now instead.
            // http://stackoverflow.com/questions/18399805/reloading-refreshing-kendo-grid
            // data() wraps Results array in kendo.data.ObservableObject...
            $scope.pdfGrid.dataSource.data(d.Results);
            //http://www.telerik.com/forums/howto-reset-virtual-scrolling-on-datasource-query
            $timeout(function () {
                if (d.Query.UpdatedSince) {
                    d.Query.UpdatedSince = parseDate(d.Query.UpdatedSince);
                    //d.Query.UpdatedSince = $filter('date')(parseDate(d.Query.UpdatedSince), 'M/d/yy');
                }
            });
        })
        .error(function (data, status, errors, config) {
            $scope.asyncNotification.hide();
            if (data.error) { alert('ERROR: ' + data.error); }
        });
    });
    //});
    // hide notification if exists...
    //console.log(Object.keys($location.search()).length);
    $scope.options = {
        AsyncNotification: {
            show: onShowAsyncNote,
            templates: [
                {
                    type: 'async-op',
                    template: $("#asyncOpTemplate").html()
                }
            ]
        },
        Grid: {
            columns: [
                {
                    field: "Id",
                    template: function (d) {
                        return "<a href='{{ appRoot }}spa#/pd/" + d.Id + "'>" + d.Id + "</a>"
                    },
                    title: "PDF #",
                    width: "40px"
                },
                {
                    field: "PositionId",
                    template: function (d) {
                        return d.PositionId ? "<a href='{{ appRoot }}spa#/position/" + d.PositionId + "'>" + d.PositionId + "</a>" : '';
                    },
                    title: "Pos #",
                    width: "40px"
                },
                //{
                //    field: "LegacyStatus",
                //    template: function (d) {
                //        return d.LegacyStatus || '';
                //    },
                //    title: "Status",
                //    width: "120px"
                //},
                {
                    field: "WorkingTitle",
                    template: function (d) {
//                        return d.WorkingTitle + ' [' + d.Incumbent + ']';
                        return d.WorkingTitle;
                    },
                    title: "Working Title",
                    width: "180px"
                },
                {
                    field: "IncumbentLast",
                    sortable: {
                        compare: function (a, b) {
                            // || '' to prevent null compare:
                            return (a.IncumbentLastName || '') < (b.IncumbentLastName || '') ?
                                -1 : (a.IncumbentLastName || '') > (b.IncumbentLastName || '') ? 1 : 0;
                        }
                    },
                    template: function (d) {
//                        return d.IsVacant ? '[VACANT]' : d.IncumbentLastName;
                        return (d.IncumbentLastName || '') + (d.IsVacant ? ' [VACANT]' : (d.EndDate ?
                            ' [' + $filter('date')(parseDate(d.EndDate), 'M/d/yy') + ']' :
                            (', ' + d.IncumbentFirstName)));
                    },
                    title: "Incumbent [thru]",
                    width: "120px"
                },
                //{
                //    field: "EndDate",
                //    template: function (d) {
                //        return d.EndDate ? $filter('date')(parseDate(d.EndDate), 'M/d/yy') : '';
                //    },
                //    title: "Thru",
                //    width: "60px"
                //},
                {
                    field: "ProposedClassCode",
                    template: function (d) {
                        return d.ProposedClassCode;
                    },
                    title: "Class Code",
                    width: "60px"
                },
                {
                    field: "LastUpdatedAt",
                    template: function (d) {
                        // TODO: parse all dates on load, consider moment.js date lib
                        var ds = parseDate(d.LastUpdatedAt);
                        // can't seem to get inline Angular date filter working here,
                        // so using script-style $filter:
                        //return kendo.toString(ds, 'd');
                        // https://docs.angularjs.org/api/ng/filter/date
                        return $filter('date')(ds, 'M/d/yy');
                    },
                    title: "Updated",
                    width: "70px"
                },
                {
                    field: "ApprovalStatusDesc",
                    template: function (d) {
                        return d.ApprovalStatusDesc || '';
                    },
                    title: "Approval Stage",
                    width: "120px"
                }
                //{
                //    field: "PdId",
                //    title: "Pos Desc",
                //    width: "80px"
                //},
                //{
                //    field: "EffectiveDate",
                //    //format: "{0:M/d/yyyy}",
                //    template: function (d) {
                //        //                        return "<a href='#/pd/" + d.PdId + "'>" + kendo.toString($scope.formatJsonDate(d.EffectiveDate), 'd') + "</a>";
                //        return d.PdId ? "<a href='#/pd/" + d.PdId + "'>" + kendo.toString(d.EffectiveDateString, 'd') + "</a>"
                //            : '[NONE]';
                //    },
                //    title: "Last PD",
                //    //type: "date",
                //    width: '80px'
                //}
            ],
            dataSource: {
                data: $scope.vm.Results,
                pageSize: 18
                //                type: "json"
            },
            height: 550,
            //pageable: true,
            //rebind: $scope.vm.Results, doesn't seem to work...
            resizable: true,
            scrollable: {
                virtual: true
            },
            schema: {
                model: {
                    id: "Id"
                    //,fields: {
                    //    EffectiveDateString: {
                    //        type: "date"
                    //    }
                    //}
                }
                // see schema.parse for custom pre-parsing of vm
            },
            sortable: true
        }
    };

    $scope.createBlankPd = function () {
//        var posturl = appRoot + 'api/PositionDescription';
        var posturl = $scope.appRoot + 'api/PositionDescription';
        if (confirm('This will create a new, blank Position Description.')) {
            $http({ method: 'POST', url: posturl, data: {} })
            .success(function (data) {
//                data.LastUpdatedAt = formatJsonDate(data.LastUpdatedAt); // embedded list doesn't need this, only generated items.
                $timeout(function () {
                    alert('created PD # ' + data.Id + '; redirecting there now...');
                    // now redirect to new PD:
                    $location.path('/pd/' + data.Id);
//                    alert('redirecting to ' + $scope.appRoot + '#/pd/' + data.Id);
                    //$scope.vm.History.unshift(data);
                    //$scope.vm.History.push(data);
                }, 300);
            }) // TODO: more robust client-side error handling...
            .error(function (data, status, errors, config) {
                alert('error:' + JSON.stringify(data));
                //console.log(data);
                //if (data.error) { alert('ERROR: ' + data.error); };
            });
        }
    }
    $scope.runQuery = function () {
        // create query string from query object...
        //        alert('dataSource.data len:' + $scope.pdfGrid.dataSource.data().length);
        $timeout(function () {
            $scope.asyncNotification.show({ message: 'searching Position Descriptions...' }, 'async-op'); // info, success, warning, error...
            $('#pdfGrid .k-scrollbar-vertical').scrollTop(0); // need to scrollTop *BEFORE* updating data source...looks like bug in Grid virtual scrolling
        });

//        $('#pdfGrid .k-scrollbar-vertical').scrollTop(0); // need to scrollTop *BEFORE* updating data source...looks like 

        var q = $scope.vm.Query;
        var qp = []; // Query Params
        if (q.IncludeOld) qp.push('IncludeOld=true');
        if (q.DirectReportsOnly) qp.push('DirectReportsOnly=true');
        if (typeof q.UpdatedSince == 'object') { // Date, not string. (first submit goes as string.)
            q.UpdatedSince = $filter('date')(q.UpdatedSince, 'M/d/yy');
        }
        if (q.UpdatedSince) qp.push('UpdatedSince=' + q.UpdatedSince);
        if (q.QueryText) qp.push('QueryText=' + escape(q.QueryText));

        // TODO: handle when '/pd?[query string]'...
        if (qp.length) {
            //            $location.path('/pd').search( + (qp.length ? '?' + qp.join('&') : ''));
            //            $location.path('/pd').search(qp.join('&'));
            $location.search(qp.join('&')); // reloadOnSearch = false.
            //            $location.path('/pd/q/' + qp.join('&'));
        }
        else {
            $location.search('');
        //    //// per http://www.angularjshub.com/examples/routing/locationservice/
        //    //for (var k in $location.search()) {
        //    //    //$location.search(k, null); // only documented way to clear search...
        //    //}
        }
    }
})
.controller('PositionController', function ($scope, $filter, $http, $location, $rootScope, $route, $routeParams, $timeout, fetch) {
    //    $scope.cpath = $fscope.path = ''; // initial path (appRoot/Position/{id})
    //$rootScope.$on('$locationChangeSuccess', function (event) {
    //    // TODO: single (flat) hash of all nodes by id
    //    $scope.path = $location.url(); // e.g. "/Contracts, /Delegations"
    //    $scope.cpath = $scope.path.substr(0, $scope.path.lastIndexOf('/'));
    //});
    // bypass init if vm already set:
    //alert('VM:' + $scope.vm); // this gets triggered TWICE on first load, is undefined initially.
    if (!$scope.vm) {
//        $scope.vm = vm; // vm global is populated in .cshtml template
        $scope.vm = fetch.data;
        $scope.appRoot = appRoot; // set in _Layout_ng.cshtml
        // for remembering expansion state during ngRoute navigation...
        // TODO: store this in a cookie.
        $scope.expandedItems = {'PosInfo': true}; // store LI ids...default first panel to open.
        $scope.pbopts = { 'selectedItemId': null };
        $scope.selectedPdfId = null; // for holding PDF selection for actions like cloning, etc...
        $scope.selectedPdfAction = "none";
        $scope.asyncMessage = ""; // e.g. CLONING, DELETING etc.

//        $scope.selectedItemId = []; // this is a hack since changed scalar props are reverted during ngRoute follow...
        //TODO: convert CRUD ops to use ngResource and/or custom resource factory
        $scope.cloneLastPdf = function () {
            var clone = angular.copy($scope.vm.PositionDescriptions[0]);
            clone.ApprovalDate = undefined;
            clone.Status = 'Cloning...';
            $scope.vm.PositionDescriptions.unshift(clone);
            clone.isAdding = true;

            // TODO: connect this to back end Web API...this is just UI demo
            // (should be deep clone server-side...)
            // return new Id from back end asynchronously...
            $timeout(function () {
                $scope.asyncOp = false;
                clone.Status = 'New';
                clone.Id = Math.floor(Math.random() * 2000000000);
                clone.isAdding = false;
            }, 500);
        };

        $scope.clonePD = function (i) {
            var posturl = appRoot + 'api/PositionDescription/' + $scope.vm.PositionDescriptions[i].Id + '/clone'
            try {
                $scope.asyncOp = true;
                $scope.asyncMessage = "CLONING"
                $http({ method: 'POST', url: posturl })
                .success(function (clone) {
                    $timeout(function () {
                        $scope.vm.PositionDescriptions.unshift(clone);
                        $scope.selectedPdfId = clone.Id;
                        $scope.asyncOp = false;
                    }, 500);
                })
                .error(function (data, status, errors, config) {
                    if (data.error) { alert('ERROR: ' + data.error); };
                    $scope.asyncOp = false;
                });
            }
            catch (xcp) {
                var err = xcp;
            }
        };
// SEE PositionPdController for sample createPD, possible share in $rootScope?
//        $scope.createPD = function () {
//            alert('creating PD for position# ' + $scope.vm.Id);
////            var posturl = appRoot + 'api/PositionDescription';
//        };
        $scope.createPD = function () {
            // TODO: add kendo Notification of async op status...
            var posturl = appRoot + 'api/PositionDescription';
            if (confirm('This will create a new Description for Position #' + $scope.vm.Pos.Id
                + '.\nWhere possible, fields will be pre-populated from existing data.')) {
                //            var newPd = new PositionDescription($scope.vm.PositionId);
                var newPd = new PositionDescription($scope.vm.Pos.Id);
                $http({ method: 'POST', url: posturl, data: newPd })
                .success(function (data) {
                    data.LastUpdatedAt = formatJsonDate(data.LastUpdatedAt); // embedded list doesn't need this, only generated items.
                    $timeout(function () {
                        // unshift this to PD list here instead...
                        $scope.vm.PositionDescriptions.unshift(data);
                    }, 300);
                }) // TODO: more robust client-side error handling...
                .error(function (data, status, errors, config) {
                    alert('error:' + JSON.stringify(data));
                    //console.log(data);
                    //if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        };



        $scope.deletePD = function (i) {
            var posturl = appRoot + 'api/PositionDescription/' + $scope.vm.PositionDescriptions[i].Id
            // TODO: allow deleting only PDs in "created" approval state...
            if (!confirm('This will permanently delete PDF #' + $scope.vm.PositionDescriptions[i].Id
                + ', along with any associated Duty allocations.')) { return false; };
            // TODO: put this into AsyncOpIndicator...
            //$scope.vm.PositionDescriptions[i].Status = "Deleting...";
            $scope.vm.PositionDescriptions[i].isDeleting = true;
            $scope.asyncOp = true;
            $scope.asyncMessage = "DELETING"

            $http({ method: 'DELETE', url: posturl })
            .success(function (deleted) {
                // this seems to succeed silently for IE...
                $timeout(function () {
                    $scope.asyncOp = false;
                    $scope.vm.PositionDescriptions.splice(i, 1);
                }, 300); // 300ms timeout to ensure deletion is visible...
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
            });

            //$timeout(function () {
            //    $scope.asyncOp = false;
            //    $scope.vm.PositionDescriptions.splice(i, 1);
            //}, 500);
        };

        $scope.doParAction = function(actionCode, itemType) {
            if (actionCode == '') return false;
            switch (actionCode) {
                case 'jump': window.open(appRoot + 'position/' + $scope.vm.Pos.Id, '_self');
                    return false;
                case 'P0':
                    $scope.createPar({
                        //                    PosSupervisorPosId: posVm.Id(),
                        PosSupervisorPosId: $scope.vm.Pos.Id,
                        ActionCode: actionCode
                    });
                    break;
                case 'U3-81-in':
                    $scope.createPar({
                        //                    PositionId: posVm.Id(),
                        PositionId: $scope.vm.Pos.Id,
                        ActionCode: 'U3',
                        ActionReasonId: 81,
                        IsTransferIn: true
                    });
                    break;
                default: $scope.createPar({
                    //                PositionId: posVm.Id(),
                    PositionId: $scope.vm.Pos.Id,
                    // PositionId: n.attr('id'),
                    // EmployeeId is irrelevant here:
                    //                EmployeeId: (itemType == 'Emp') ? n.data('IncumbentId') : null,
                    //ActionType: actionCode
                    ActionCode: actionCode
                });
            }
        }

        $scope.createPar = function(parData) {
            // TODO: test that parData is not null and is an object...
            var posturl = appRoot + 'api/par';
            $timeout(function () {
                $scope.asyncNotification.show({ message: 'Creating PAR...' }, 'async-op'); // info, success, warning, error...
            }); // *this* timeout is to ensure Angular $apply.  Next one is for delay to ensure visibility before redirect.
            $timeout(function() {
                try {
                    $http({ method: 'POST', url: posturl, data: parData })
                    .success(function (d) {
                        window.location = appRoot + 'par/' + d.Par.Id;
                    })
                    .error(function (d, status, errors, config) {
                        $scope.asyncNotification.hide();
                        alert('ERROR! ' + d);
                        if (d.error) { alert('ERROR: ' + d.error); };
                    });
                }
                catch (xcp) {
                    var err = xcp;
                    alert('ERROR: ' + JSON.stringify(xcp));
                }
            }, 300);
        }

        $scope.doPdAction = function (newscope, idx) {
            switch (newscope.selectedPdAction) {
                case 'clone': {
                    $scope.clonePD(idx);
                    return;
                }
                case 'delete': {
                    $scope.deletePD(idx);
                    return;
                }
                default: return;
            }
        }

        // config options for KendoUI widgets:
        // TODO: move this out of if block, dynamically set expandMode to multiple only if > 1 currently expanded?
        //$scope.options = {
        //    PanelBar: {
        //        animation: {
        //            collapse: {
        //                duration: 300
        //            },
        //            expand: {
        //                duration: 300,
        //                effects: 'expandVertical'
        //            }
        //        },
        //        expandMode: ($scope.expandedItems.length) ? "multiple" : "single",
        //        //            expandMode: (kc == 2) ? "multiple" : "single",
        //        collapse: function (pb) {
        //            delete $scope.expandedItems[$(pb.item).attr('id')];
        //        },
        //        expand: function (pb) {
        //            $scope.expandedItems[$(pb.item).attr('id')] = true;
        //        },
        //        select: function (pb) {
        //            $scope.selectedItemId = $(pb.item).attr('id');
        //        }
        //    }
        //};

        $scope.performAction = function (selectorId) {
            var action = $('#' + selectorId).val();
            if (action != '') { // skip if no selection
//                doAction(action, 'Pos'); // always Pos in this context, can vary in treeview context
                $scope.doParAction(action, 'Pos'); // always Pos in this context, can vary in treeview context
            }
        }
        $scope.formatJsonDate = function (d) { return formatJsonDate(d) };

        $scope.rowClicked = function (pdr) {
            $scope.selectedPdfId = pdr.Id;
        }
    } // pre-init check, irrelevant now...

    document.title = 'Position #' + $scope.vm.Pos.Id + ' Detail';

    $scope.options = {
        AsyncNotification: {
            show: onShowAsyncNote,
            templates: [
                {
                    type: 'async-op',
                    template: $("#asyncOpTemplate").html()
                }
            ]
        },
        PanelBar: {
            animation: {
                collapse: {
                    duration: 300
                },
                expand: {
                    duration: 300,
                    effects: 'expandVertical'
                }
            },
            expandMode: $scope.pbopts.expandMode || 'single',
            collapse: function (pb) {
                delete $scope.expandedItems[$(pb.item).attr('id')];
            },
            expand: function (pb) {
                $scope.expandedItems[$(pb.item).attr('id')] = true;
            },
            select: function (pb) {
                $scope.pbopts.selectedItemId = $(pb.item).attr('id');
            }
        }
    };

    // these always needs initialization due to panelbar directive:
    $scope.expandAll = function () {
        // *NOTE, panelbar gets replaced on re-render, thus caching as separate property here.
        $scope.pbopts.expandMode = $scope.panelbar.options.expandMode = "multiple";
        $scope.panelbar.expand('li');
    };

    $scope.collapseAll = function () {
        $scope.pbopts.expandMode = $scope.panelbar.options.expandMode = "single";
        $scope.panelbar.collapse('li');
    };
})
.controller('PositionPdController', function ($http, $scope, $timeout, fetch) {
    $scope.vm = fetch.data; // TODO: include additional position data, currently raw PdList.

    document.title = 'PD History';
    $scope.clonePD = function (i) {
        var posturl = appRoot + 'api/PositionDescription/' + $scope.vm.History[i].Id + '/clone'
        try {
            $scope.cloningPd = $scope.vm.History[i];
            $http({ method: 'POST', url: posturl })
            .success(function (clone) {
                clone.LastUpdatedAt = formatJsonDate(clone.LastUpdatedAt); // embedded list doesn't need this, only generated items.
                $timeout(function () {
                    $scope.vm.History.unshift(clone);
                    $scope.cloningPd = null;
                }, 500);
            })
            .error(function (data, status, errors, config) {
                if (data.error) { alert('ERROR: ' + data.error); };
                $scope.cloningPd = null;
            });
        }
        catch (xcp) {
            var err = xcp;
        }
    };

    // NOTE, this is currently separate from PositionController createPD...
    $scope.createPD = function () {
        // TODO: add kendo Notification of async op status...
        var posturl = appRoot + 'api/PositionDescription';
        if (confirm('This will create a new Description for Position #' + $scope.vm.Pos.Id
            + '.\nWhere possible, fields will be pre-populated from existing data.')) {
//            var newPd = new PositionDescription($scope.vm.PositionId);
            var newPd = new PositionDescription($scope.vm.Pos.Id);
            $http({ method: 'POST', url: posturl, data: newPd })
            .success(function (data) {
                data.LastUpdatedAt = formatJsonDate(data.LastUpdatedAt); // embedded list doesn't need this, only generated items.
                $timeout(function () {
                    $scope.vm.History.unshift(data);
                    //$scope.vm.History.push(data);
                }, 500);
            }) // TODO: more robust client-side error handling...
            .error(function (data, status, errors, config) {
                alert('error:' + JSON.stringify(data));
                //console.log(data);
                //if (data.error) { alert('ERROR: ' + data.error); };
            });
        }
    };

    $scope.deletePD = function (ix) {
        var pd = $scope.vm.History[ix];
        var posturl = appRoot + 'api/PositionDescription/' + pd.Id;
        // TODO: allow deleting only PDs in "created" approval state...
        if (!confirm('This will permanently delete Position Description #' + pd.Id
            + ', along with any associated Duty allocations.')) { return false; };
        // TODO: add Kendo Notification of async op status
        $scope.deletingPd = pd;
        $http({ method: 'DELETE', url: posturl })
        .success(function (deleted) {
            // this seems to succeed silently for IE...
            $timeout(function () {
                //alert("Deleted PD #" + pd.Id + ".");
                $scope.deletingPd = null;
                $scope.vm.History.splice(ix, 1)
            }, 500); // 300ms timeout to ensure deletion is visible...
        })
        .error(function (data, status, errors, config) {
            $scope.deletingPd = null;
            if (data.error) { alert('ERROR: ' + data.error); };
        });
    };
})
.controller('SearchController', function ($http, $scope, $timeout) {
    // vm is server-generated vm, may include ValErr, Par, etc.
    // TODO: move this util to $rootScope with ng-route...
    $scope.convertJsonDate = function (d) {
        if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
        return new Date(parseInt(d.match(/-?\d+/)[0]));
    }
    $scope.appRoot = appRoot; // TODO: move into $rootScope with ng-routing...
    $scope.vm = vm; // vm global is populated in .cshtml template
    for (var ix in $scope.vm.Results)
    {
        var r = $scope.vm.Results[ix];
        //r.EndDate = formatJsonDate(r.EndDate);
        //r.StartDate = formatJsonDate(r.StartDate);
        r.EndDate = $scope.convertJsonDate(r.EndDate);
        r.StartDate = $scope.convertJsonDate(r.StartDate);
    }
    $scope.sortField = 'LastName'; // default sort field
    $scope.desc = false;
    $scope.sortOn = function (f) {
        //*note* Angular can take array of sort fields, TODO: manage this to enable multi-sort (splice out if found, then push)
        // (currently only single sort field is supported...)
        $scope.desc = $scope.sortField == f ? !$scope.desc : false; // default to ascending unless second click, then reverse.
        $scope.sortField = f;
    }
})
.config(function ($routeProvider, $locationProvider) {
    // TODO: consider replacing ngRoute with ui-router once it stabilizes...
    // https://github.com/angular-ui/ui-router
    $routeProvider
    .when('/', {
        templateUrl: appRoot + 'ngpartial/template/EmployeeInfo',
        controller: 'EmployeeController', // client controller
        caseInsensitiveMatch: true,
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //                 var id = $route.current.params.pdfId;
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/employee/current' // implies id lookup from CurrentUser.  TODO: consider embedding current Emp ID to improve logging context.
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    alert('get data failed...!');
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        }
    })
    .when('/OLDPositionRoot', {
        // (this is not used any more, just here for reference... '/' now maps to myinfo.
        //templateUrl: 'SpaRoot.html',
        //controller: 'SpaController'
        //         TODO: add client /Position route...this relies on server-side PositionController:
        //        templateUrl: 'positionDetail.html',
        controller: 'PositionController', // client controller (already existed, trying to convert NgPosDetail.cshtml to Partial)
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //var id = $route.current.params.id;
                // since this is at root, need to extract id from higher in URL...
                // *NOTE*, this is a total hack to keep /Position/[id] working while migrating to SPA:         //var p = window.location.href;
                var id = p.substring(p.lastIndexOf('Position') + 9, p.length - 2);

                //alert(window.location.href); /Position/[id]#/

                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/position/' + id
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    alert('get data failed...!');
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        },
        templateUrl: appRoot + 'ngpartial/template/Position'
    })
    .when('/employee/:id', {
        templateUrl: appRoot + 'ngpartial/template/EmployeeInfo',
        controller: 'EmployeeController', // client controller
        caseInsensitiveMatch: true,
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //                 var id = $route.current.params.pdfId;
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/employee/' + id
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    alert('get data failed...!');
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        }
        //controller: 'SpaController',
        //templateUrl: 'SpaRoot.html',
        //controller: 'SpaController'
    })
    .when('/myinfo', {
        templateUrl: appRoot + 'ngpartial/template/EmployeeInfo',
        controller: 'EmployeeController', // client controller
        caseInsensitiveMatch: true,
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //                 var id = $route.current.params.pdfId;
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/employee/current' // implies id lookup from CurrentUser.  TODO: consider embedding current Emp ID to improve logging context.
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    alert('get data failed...!');
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        }
    })
    .when('/pd', {
        templateUrl: appRoot + 'Views/NgPartial/PdList.html',
        caseInsensitiveMatch: true,
        controller: 'NewPdListController',
        reloadOnSearch: false,
        resolve: {
            fetch: function ($route, $http, $location) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                var qs = [];
                //if (Object.keys($location.search()).length) {
                for (var k in $location.search()) {
                    qs.push(k + '=' + $location.search()[k]);
                }
                //}
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/PositionDescription' + (qs.length ? '?' + qs.join('&') : '')
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    if (data.error) { alert('ERROR: ' + data.error); }
                });
            }
        }
    })
    .when('/pd/:id', {
        templateUrl: appRoot + 'ngpartial/template/PositionDescription', // TODO: rename this to PdDetail for consistency
        caseInsensitiveMatch: true,
        controller: 'PdController',
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/PositionDescription/' + id
                }).success(function (data) {
                    //                    return data.Description;
                    return data;
                })
                .error(function (data, status, errors, config) {
                    if (data.error) { alert('ERROR: ' + data.error); }
                });
            }
        }
    })
    .when('/position/:id', {
        caseInsensitiveMatch: true,
        controller: 'PositionController', // client controller (already existed, trying to convert NgPosDetail.cshtml to Partial)
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //                 var id = $route.current.params.pdfId;
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/position/' + id
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    alert('get data failed in PositionController...!');
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        },
        // this returns pure HTML NG Partial directly (no server-side controller):
        templateUrl: appRoot + 'Views/NgPartial/Position.html',
        // this calls server-side NgPartialController, renders NgPartial/Position.cshtml:
        //templateUrl: appRoot + 'ngpartial/template/Position'
    })
    .when('/position/:id/pd', {
        caseInsensitiveMatch: true,
        controller: 'PositionPdController', // client controller (already existed, trying to convert NgPosDetail.cshtml to Partial)
        resolve: {
            fetch: function ($route, $http) { // TODO: add Pdfs $resource here. should return full async pdfDetail (promise), not just id.
                //                 var id = $route.current.params.pdfId;
                var id = $route.current.params.id;
                return $http({ // promise
                    method: 'GET',
                    url: appRoot + 'api/position/' + id + '/pd'
                }).success(function (data) {
                    return data;
                })
                .error(function (data, status, errors, config) {
                    alert('get data failed...!');
                    if (data.error) { alert('ERROR: ' + data.error); };
                });
            }
        },
        //        templateUrl: appRoot + 'ngpartial/template/PositionPdList'
        templateUrl: appRoot + 'Views/NgPartial/PositionPdList.html'
    });
    //.when('/positions', { // DOES THIS BELONG HERE???
    //    templateUrl: appRoot + 'ngpartial/template/PdList',
    //    controller: 'PdListController', // client controller
    //    caseInsensitiveMatch: true
    //});
    // configure html5 to get links working on jsfiddle
    $locationProvider.html5Mode(false);
})
//.directive('pbitem', function() {
//    alert('pbitem');
//})
; // end Angular hr module

// TODO: hardcode this anonymously into options config??
function onShowAsyncNote(e) {
    // calculate correct position (top center) for async note:
    if (!$("." + e.sender._guid)[1]) {
        var element = e.element.parent(),
            eWidth = element.width(),
            eHeight = element.height(),
            wWidth = $(window).width(),
            wHeight = $(window).height(),
            newTop, newLeft;

        newLeft = Math.floor(wWidth / 2 - eWidth / 2);
        newTop = Math.floor(wHeight / 2 - eHeight / 2);

        // CENTER CENTER:
        //e.element.parent().css({ top: newTop, left: newLeft });
        // TOP CENTER:
        e.element.parent().css({ top: 10, left: newLeft });
    }
}

// start global JS (outside of AngularJS) functions: (TODO: move these into global.js or root controller)
function formatJsonDate(d) {
    // allow negation prefix for dates before 1970:
    if (d == '/Date(-62135568000000)/' || d == null) { return ''; } // non-nullable .NET DateTime is converted to 1/1/1...
    var fdate = new Date(parseInt(d.match(/-?\d+/)[0]));
    // use getFullYear for Chrome:
    var mdy = (fdate.getMonth() + 1) + '/' + fdate.getDate() + '/' + fdate.getFullYear();
    return mdy;
}

function parseDate(s) {
    var re = /(\d{4})\-(\d\d)\-(\d\d)/;
    var ymd = s.match(re);
    var d = new Date(ymd[1], ymd[2] - 1, ymd[3]);
    //alert('tz offset=' + d.getTimezoneOffset());
    return d;
}

// indexOf Polyfill for IE8:
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
if (!Array.prototype.indexOf) {
    Array.prototype.indexOf = function (searchElement, fromIndex) {
        if (this === undefined || this === null) {
            throw new TypeError('"this" is null or not defined');
        }

        var length = this.length >>> 0; // Hack to convert object.length to a UInt32

        fromIndex = +fromIndex || 0;

        if (Math.abs(fromIndex) === Infinity) {
            fromIndex = 0;
        }

        if (fromIndex < 0) {
            fromIndex += length;
            if (fromIndex < 0) {
                fromIndex = 0;
            }
        }

        for (; fromIndex < length; fromIndex++) {
            if (this[fromIndex] === searchElement) {
                return fromIndex;
            }
        }

        return -1;
    };
}
// end Polyfill

// client-side Models...// TODO: eliminate this, was a bad idea...
function PositionDescription(posId) {
    this.PositionId = posId;
}