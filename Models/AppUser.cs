﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class AppUser
    {
        public int Id { get; set; }
        public string LanId { get; set; }
        public int EmployeeId { get; set; }
        public int RoleId { get; set; }
    }
}