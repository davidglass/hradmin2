﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HrAdmin2.Models
{
    public class CostCenter
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }
        public bool IsActive { get; set; }
        public string CharCode { get; set; }
        public string DescShort { get; set; }
        public string DescLong { get; set; }
        public int ManagerEmpId { get; set; }
    }
}