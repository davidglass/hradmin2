﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ActionReason : ModelBase
    {
        public string Reason { get; set; }
    }
}