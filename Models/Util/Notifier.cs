﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using RazorEngine;

namespace HrAdmin2.Models.Util
{
    // NOTE, this is currently specific to Position Description Approval notifications.
    // TODO: generalize to handle multiple approval types.
    public static class Notifier
    {
        public static void NotifyNextApprover(ApprovalTaskUpdate atu, HrAdminDbContext db)
        {
            // TODO: get appropriate Email template and ViewModel from wf.AppId + active task.TaskCodeId (Standard / SignOff / MoveFwdRegardless).
            // (first look up workflow from atu)
            // *NOTE*, Active approval status should mean Sequence Initiation here:
            // (i.e. First Task, e.g. HR Screen for PD Approval)
            // TODO: could ignore approvalstatus and simply send notification to next active task in workflow...
            //if (atu.ApprovalStatus == "Active")
            //{
            // TaskApprover list (potentially multiple if multiple active delegates):
            var ta = db.GetByProc<Approver>("GetApprovalEmailVmNew", new { TaskId = atu.Id });
            CreateEmails(ta, db, atu.Id);
            //}
            //// "Canceled" == "Skipped":
            //else if (atu.ApprovalStatus == "Approved" || atu.ApprovalStatus == "Canceled") {
            //    // notify incumbent / delegate of next active task...
                
            //}
        }

        public static void CreateEmails(List<Approver> tal, HrAdminDbContext db, int taskid)
        {
            // *NOTE*, tal (Task Approver List) will typically only have one row, but could have more if multiple active Delegates for a single approver position.
            var approot = HttpContext.Current.Server.MapPath("~");
            // TODO: switch on tal[0].AppName (="PdApproval")... and/or other params
            var template = File.ReadAllText(String.Format("{0}/Views/EmailTemplates/PdApproval.cshtml", approot));
            //set Url path
            var fullUrl = HttpContext.Current.Request.Url.ToString();
            var urlPath = fullUrl.Substring(0, fullUrl.LastIndexOf("/api/") + 1);

            foreach (var a in tal)
            {
                a.AppBaseUrl = urlPath;
                try
                {
                    string body = Razor.Parse(template, a);
                    string subject = String.Format("Processing needed for Position Description #{0}: {1}", a.PdId, a.PositionId.HasValue ? "Position #" + a.PositionId : "No Position"); // TODO: add incumbent if not vacant
                    string fromAddress = "no-reply@cts.wa.gov";
                    string fromDisplayName = "CTS HR Apps";

                    db.Exec("CreateEmail", parameters: new
                    {
                        fromAddress = fromAddress,
                        fromDisplayName = fromDisplayName,
                        toAddress = a.ApproverEmail,
                        toDisplayName = (a.IsHR ? "HR Office" : a.ApproverFirstName + " " + a.ApproverLastName),
                        body = body,
                        subject = subject,
                        isBodyHtml = true,
                        source = "ApprovalTask",
                        sourceId = taskid
                    });

                }
                catch (Exception xcp) {
                    throw xcp;
                };        
            }
        }

    }

    public class Approver
    {
        public int? PdId { get; set; } // Position Description Id
        public int? PositionId { get; set; } // of PD
        public string WorkingTitle { get; set; }
        public bool IsHR { get; set; }
        public string FilledByName { get; set; }
        public string TaskCodeName { get; set; }
        public string AppName { get; set; }
        public string CreatorFirstName { get; set; }
        public string CreatorLastName { get; set; }
        public int? DelegatedToPersonId { get; set; } // just for "Delegated" indicator.
        public string ApproverEmail { get; set; }
        public string ApproverFirstName { get; set; }
        public string ApproverLastName { get; set; }
        public string AppBaseUrl { get; set; }
    }
}