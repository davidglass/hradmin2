﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class Employee : ModelBase
    {
        public string LanId { get; set; }
        public int HrmsPersonId { get; set; }
        public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PreferredName { get; set; }
        public string Suffix { get; set; }
        public DateTime LastUpdated { get; set; }
        public DateTime DOB { get; set; }
        public string Gender { get; set; } // "1" = male, "2" = female
        public string Email { get; set; }

        public List<EmployeeAddress> Address { get; set; }
        public List<EmergencyContacts> EmerContacts { get; set; }
        public List<EmployeeLeave> Leave { get; set; }

        
        
    }
}