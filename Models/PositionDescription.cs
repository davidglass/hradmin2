﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
//    public class PositionDescription : ModelBase
    // removing ModelBase to remove Validate / LastUpdatedByUser properties...
    // TODO: move them out of ModelBase.
    public class PositionDescription
    {
        public int Id { get; set; }
        public int? PositionId { get; set; }
        public string PositionObjective { get; set; }
        public int? ProposedJobId { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public DateTime? EffectiveDate { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
        public bool IsLead { get; set; }
        public decimal? DirectFteSupervised { get; set; }
        public decimal? TotalFteSupervised { get; set; }
        public int? TopJobClassSupervised { get; set; }
        public bool IsSupervisor { get; set; }
        public bool AssignsWork { get; set; }
        // TODO: finish this...possibly add separate tables for Wms, Exempt??? or prefix column names
        public bool Instructs { get; set; }
        public bool ChecksWork { get; set; }
        public bool PlansWork { get; set; }
        public bool EvalsPerformance { get; set; }
        public bool Disciplines { get; set; }
        public bool Hires { get; set; }
        public bool Terminates { get; set; }
        public string SupervisorLeadAddInfo { get; set; }
        public int SupervisionLevel { get; set; } // TODO: enum for this...
        public string SupervisionAddInfo { get; set; }
        public bool IsCoopCritical { get; set; }
        public string CoopSupportFunctions { get; set; }
        public string WorkSetting { get; set; }
        public string WorkSchedule { get; set; }
        public string TravelReqs { get; set; }
        public string ToolsEquipment { get; set; }
        public string CustomerInteraction { get; set; }
        public string WorkingConditionsOther { get; set; }
        public string QualificationsRequired { get; set; }
        public string QualificationsPreferred { get; set; }
        public string SpecialRequirements { get; set; }
        public string InTrainingPlan { get; set; }
        public string PositionTypeCXW { get; set; }

        // w prefix is WMS-only:
        //public string wAccountabilityControlInfluence { get; set; } // removed this as redundant with supervision and budget info.
        public string wAccountabilityScope { get; set; }
        public string wDecisionExpertise { get; set; }
        public string wDecisionsRisk { get; set; }
        public string wDecisionsTacticalStrategic { get; set; }

        // wx prefix applies to WMS and eXempt, x to eXempt only
        public string wxDecisionPolicyImpact { get; set; }
        public string wxDecisionResponsibility { get; set; }
        public string wxDecisionsUnauthorized { get; set; }
        public string wxFinancialBudget { get; set; }
        public string wxFinancialImpact { get; set; }
        // wxScopeOfControl was replaced with wAccountabilityControlInfluence.
        // (field is unused by Exempt, *also redundant* with other fields)
        //public string wxScopeOfControl { get; set; }
        //public string xPolicyImpact { get; set; }
        public string xCurrentBand { get; set; }
        public int? xCurrentJobId { get; set; } // this could easily be set for all (not just eXempt)
        // xMgmtCode is actually a char? in db:
        public string xMgmtCodePMC { get; set; }
        public string xProposedBand { get; set; }
        public int? xProposedJvacEvalPts { get; set; }
        public string xExemptCitationHeading { get; set; }
        public int? LinkedParId { get; set; }
    }

    public class PositionDescriptionRow
    {
        public int Id { get; set; }
        public int PositionId { get; set; }
        public int ProposedJobId { get; set; }
        public string ProposedClassCode { get; set; }
        public string ProposedClassTitle { get; set; }
        public string Incumbent { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
    }

//    public class PositionDuty : ModelBase
    public class PositionDuty
    {
        [System.ComponentModel.DataAnnotations.Schema.DatabaseGenerated(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.None)]
        public int Id { get; set; } // sequence returns long, being cast to int.
        public int PositionDescriptionId { get; set; }
        public string Duty { get; set; }
        public string TasksHtml { get; set; }
        public int TimePercent { get; set; }
    }
}

namespace HrAdmin2.Models.ViewModels
{
    public class PositionDescriptionVm : BaseVm
    {
        public PositionDescription Description { get; set; }
        public List<PositionDuty> Duties { get; set; }
        public List<ApprovalTaskPd> ApprovalTasks { get; set; }
        // TODO: make this List<Employee> or such?, show Incumbent(s) from PD active period?
        public string IncumbentName { get; set; }
    }
}