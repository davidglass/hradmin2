﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ApptActionRequest : ModelBase
    {
        public int PositionId { get; set; }
        // EmployeeId should be populated during CreateNewHireRequest
        public int? EmployeeId { get; set; }
        public int ActionReasonId { get; set; }
        public string ActionCode { get; set; }
        public DateTime EffectiveDate { get; set; }
        public DateTime? EndDate { get; set; }
        // TODO: real user id, status codes.
        // example status codes (will likely need more):
        // 1 = Initiated
        // 2 = Pending Approval
        // 3 = Approved
        // 4 = Submitted to DES
        // 5 = Confirmed in HRMS
        public int RequestedByUserId { get; set; } // currently defaulting to 1
        // removed this...Mark's Approval workflow should replace this...
        //public int RequestStatus { get; set; } // currently defaulting to 1
        public DateTime? CreateDate { get; set; }
        public int? ApptId { get; set; } // use for modifying pre-existing appts
        // TODO: make FillLevel non-null.
        public int? FillLevel { get; set; }
        public string ContractTypeCode { get; set; }
        public string WorkScheduleCode { get; set; }
        public char TimeMgtStatus { get; set; }
        public string LeaveAccrual_ATID { get; set; }
        public decimal EmploymentPercent { get; set; }
        public bool IsPartTime { get; set; }
        public string Notes { get; set; }
        public int ContractMonths { get; set; }
        //public Apptmt Appt { get; set; }
    }

    // this doesn't work for fake/non-existent Appt (e.g. in New Hire action)
    public class Apptmt {
        public int Id { get; set; }
        public int EmpId { get; set; }
        public int PosId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int FilledAsJobId { get; set; }
    }

}