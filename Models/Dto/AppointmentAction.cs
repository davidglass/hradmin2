﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.Dto
{
    public class AppointmentAction
    {
        public int RequestId { get; set; }
        public string ActionCode { get; set; }
        public AppointmentDto Appointment { get; set; }
    }

    public class EmployeeDto {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }

    public class PositionDto {
        public int Id { get; set; }
        public int JobClassId { get; set; }
    }

    public class AppointmentDto
    {
        public int Id { get; set; }
        public EmployeeDto Employee { get; set; }
        public PositionDto Position { get; set; }
    }
}