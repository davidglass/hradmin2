﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ApprovalTaskPd : ModelBase
    {
        public string WorkingTitle { get; set; }
        public string FirstName { get; set; } // currently appointed approver
        public string LastName { get; set; }
        public string Comment { get; set; }
        public string ApprovalStatus { get; set; }
        public string ApproverFirstName { get; set; }
        public string ApproverLastName { get; set; }
        public int? ApproverPositionId { get; set; }
        public DateTime? CompletedDate { get; set; }
    }

    public class ApprovalTaskUpdate // no ModelBase to omit extra props...
    {
        public int Id { get; set; }
        public string Comment { get; set; }
        public string ApprovalStatus { get; set; }
        public string UpdaterLanId { get; set; } // used only for updates
    }

    public class TaskApprover
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime CompletedDate { get; set; }
    }
}