﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class EmployeeLeave : ModelBase
    {

       public DateTime EffectiveDate {get; set;}
       public Decimal LeaveType { get; set; }
       public string LeaveTypeDesc { get; set; }
       public DateTime BeginDate  {get; set;}
       public DateTime   EndDate  {get; set;}
       public Decimal BeginningBalance  {get; set;}
       public Decimal LeaveEarned { get; set; }
       public Decimal LeaveTaken { get; set; }
       public Decimal LeavePaid { get; set; }
       public Decimal LeaveAdj { get; set; }
       public Decimal LeaveDonated { get; set; }
       public Decimal LeaveReturned { get; set; }
       public Decimal LeaveReceived { get; set; }
       public Decimal EndBalance { get; set; }
    }
}