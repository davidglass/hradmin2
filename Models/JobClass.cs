﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Web.Script.Serialization;

namespace HrAdmin2.Models
{
//    public class JobClass : ModelBase
    // stopped subclassing ModelBase to avoid inheriting Validate property (interferes with EF insert)
    public class JobClass
    {
        public int? Id { get; set; }
        public int? HrmsJobId { get; set; }
        public string LegacyCode { get; set; }
        public string JobTitle { get; set; }
        public bool IsActive { get; set; }
        //[ScriptIgnore]
    }
}