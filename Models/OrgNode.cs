﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class OrgNode : ModelBase
    {
        public string Name { get; set; } // TODO: add IsMultifilled property instead of setting this to MULTI.
        public int PositionCount { get; set; } // only used by OrgUnit nodes
        public int SubUnitCount { get; set; } // only used by OrgUnit nodes
        public int Depth { get; set; }
        public string NodeType { get; set; } // TODO: consider enum with db lookup table
        public bool Expanded { get; set; }
        public bool IsSupervisor { get; set; }
        public string Incumbent { get; set; }
        public string IncumbentFirst { get; set; }
        public int HrmsJobId { get; set; }
        public string ClassCode { get; set; }
        public int IncumbentId { get; set; }
        public bool IsVacant { get; set; }
    }
}