﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class EmployeeApptVm
    {
        private EmployeeAppt _appt;
        private HrAdminDbContext db;

        public EmployeeApptVm(EmployeeAppt ea, HrAdminDbContext ctx)
        {
            _appt = ea;
            db = ctx;
            Init();
        }

        private void Init()
        {
            OpenPars = db.GetByProc<Par>("GetOpenPars", parameters: new { EmployeeId = Appt.EmpId });
        }

        public EmployeeAppt Appt
        {
            get
            {
                return _appt;
            }
        }

        public List<Par> OpenPars { get; set; }
    }
}