﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class EmployeeDetailVm
    {
        private HrAdminDbContext db;
        private EmployeeDetail model;

        public EmployeeDetailVm(EmployeeDetail ed, HrAdminDbContext ctx)
        {
            db = ctx;
            model = ed;
            Init();
        }

        private void Init()
        {
            var ed = db.GetByProc<EmployeeDetail>("GetEmployeeDetail", parameters: new { EmpId = model.Id });
            model.Address = db.GetByProc<EmployeeAddress>("GetEmpHomeAddress", new { EmpId = model.Id });
            model.EmerContacts = db.GetByProc<EmergencyContacts>("GetEmpEmerContacts", new { EmpId = model.Id });
        }

        public int Id
        {
            get
            {
                return model.Id;
                
            }
        }

        public string ViewMode { get; set; }
    }
}