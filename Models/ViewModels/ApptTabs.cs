﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ApptTabs
    {
        private HrAdminDbContext db;
        private int posId;

        public ApptTabs(int p, HrAdminDbContext ctx)
        {
            posId = p;
            db = ctx;
            Init();
        }

        private void Init() {
            // default appt should be blank employee with pre-filled defaults (e.g. fill level):
            // (no, this should be available in Position info already).
            //var defaultAppt = 
            var emps = db.GetByProc<EmployeeAppt>("GetIncumbentAppts", parameters: new { PosId = posId });
            foreach (var e in emps)
            {
                Tabs.Add(new ApptTab(e));
            }
        }
        public List<ApptTab> Tabs = new List<ApptTab>();
    }

    public class ApptTab
    {
        private EmployeeAppt model;
        private ActionRequestVm arvm;

        public ApptTab(EmployeeAppt apt)
        {
            model = apt;
            //arvm = new ActionRequestVm(new ApptActionRequest() {
            //    ActionCode = "U0",
            //    Notes = "Appt Tab Default ActionRequest notes..."
            //});
        }

        public ActionRequestVm ActionRequest
        {
            get
            {
                return arvm;
            }
            set
            {
                arvm = value;
            }
        }

        // Appointment info:
        public int AppointmentId
        {
            get
            {
                return model.Id;
            }
        }
        public int FillLevel { get { return model.FillLevel; } }
        public string ContractTypeCode { get { return model.ContractTypeCode; } }
        public string WorkScheduleCode { get { return model.WorkScheduleCode; } }
        public string WorkContractTypeDesc { get { return model.WorkContractTypeDesc; } }
        public string AppointmentDate
        {
            get { return NullableDateToString(model.AppointmentDate); }
        }

        // Employee info:
        public int EmpId { get { return model.EmpId; } }
        public int? PersonnelNumber { get { return model.PersonnelNumber; } }
        public string FirstName { get { return model.FirstName; } }
        public string MiddleName { get { return model.MiddleName; } }
        public string LastName { get { return model.LastName; } }
        public string PreferredName { get { return model.PreferredName; } }
        public string Suffix { get { return model.Suffix; } }
        public string Gender { get { return model.Gender; } }
        public string MaritalStatus { get { return model.MaritalStatus; } }
        public string DOB
        {
            get { return NullableDateToString(model.DOB); }
        }
        public string StartDate
        {
            get { return NullableDateToString(model.StartDate); }
        }
        public string EndDate {
            get { return NullableDateToString(model.EndDate); }
        }
        public string StreetLine1 { get { return model.StreetLine1; } }
        public string CountyId { get { return model.CountyId; } }
        public string City { get { return model.City; } }
        public string State { get { return model.State; } }
        public string ZipCode { get { return model.ZipCode; } }
        public string HomePhone { get { return model.HomePhone; } }
        public Decimal EmploymentPercent { get { return model.EmploymentPercent; } }
        public string AnniversaryDate {
            get { return NullableDateToString(model.AnniversaryDate); }
        }
        public string CtsHireDate
        {
            get { return NullableDateToString(model.CtsHireDate); }
        }
        public string PriorPidDate
        {
            get { return NullableDateToString(model.PriorPidDate); }
        }
        public string SeniorityDate
        {
            get { return NullableDateToString(model.SeniorityDate); }
        }
        public string UnbrokenServiceDate
        {
            get { return NullableDateToString(model.UnbrokenServiceDate); }
        }
        public string VacLeaveFrozenDate
        {
            get { return NullableDateToString(model.VacLeaveFrozenDate); }
        }
        public string PersonalHolidayElgDate
        {
            get { return NullableDateToString(model.PersonalHolidayElgDate); }
        }
        public string PersonalLeaveElgDate
        {
            get { return NullableDateToString(model.PersonalLeaveElgDate); }
        }
        public string TsrLeaveElgDate
        {
            get { return NullableDateToString(model.TsrLeaveElgDate); }
        }

        private string NullableDateToString(DateTime? ndt) {
            //return ndt.HasValue ? ndt.Value.ToString("yyyy-MM-dd") : "";
            return ndt.HasValue ? ndt.Value.ToString("MM/dd/yy") : "";
        }
    }
}