﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalDelegateEmailVm : ApprovalDelegateVm
    {
        //private HrAdminDbContext db;

        public string UrlPath { get; set; }
        public string CreatedOrDeleted { get; set; }

        public ApprovalDelegateEmailVm() { }

        public ApprovalDelegateEmailVm(ApprovalDelegateVm vm) 
        {
            this.Delegate = vm.Delegate;
            this.DelegatedToLastName = vm.DelegatedToLastName;
            this.DelegatedToPreferredName = vm.DelegatedToPreferredName;
            this.DelegatedPositionTitle = vm.DelegatedPositionTitle;
            this.DelegatedPositionPersonLastName = vm.DelegatedPositionPersonLastName;
            this.DelegatedPositionPersonPreferredName = vm.DelegatedPositionPersonPreferredName;
            this.DelegatedToId = vm.DelegatedToId;
            this.DelegatedToEmail = vm.DelegatedToEmail;
            this.DelegatedPositionEmail = vm.DelegatedPositionEmail;
            this.DelegatedPositionPersonId = vm.DelegatedPositionPersonId;
		    this.CreationDate = vm.CreationDate;
		    this.DeleteDate = vm.DeleteDate;
		    this.CreateEmpLastName = vm.CreateEmpLastName;
		    this.CreateEmpPreferredName = vm.CreateEmpPreferredName;
		    this.DeleteEmpLastName = vm.DeleteEmpLastName;
            this.DeleteEmpPreferredName = vm.DeleteEmpPreferredName;
        }

    }
}