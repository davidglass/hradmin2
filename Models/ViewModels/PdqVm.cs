﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    /// <summary>
    /// Position Description Query View Model
    /// holds current query parameters and Results...
    /// </summary>
    public class PdqVm : BaseVm
    {
        public PdqVm(HrAdminDbContext db, CurrentUserInfo ui, PdQuery q)
        {
            CurrentUser = ui;
            q.LanId = ui.LanId;
            Query = q;
            Results = db.GetByProc<PdqResult>("GetPdList", q);
        }
        public PdQuery Query { get; set; }
        public List<PdqResult> Results { get; set; }
    }

    public class PdQuery
    {
        public string LanId { get; set; }
        public bool DirectReportsOnly { get; set; }
        public bool IncludeOld { get; set; }
        public string QueryText { get; set; }
        public DateTime? UpdatedSince { get; set; }
    }

    public class PdqResult
    {
        public int Id { get; set; }
        public int? PositionId { get; set; }
        public bool IsVacant { get; set; }
        public int? ProposedJobId { get; set; }
        public string ProposedClassCode { get; set; }
        public string ProposedClassTitle { get; set; }
        //public string Incumbent { get; set; }
        public string IncumbentFirstName { get; set; }
        public string IncumbentLastName { get; set; }
        public DateTime? ApprovalDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime LastUpdatedAt { get; set; }
        public int LastUpdatedByUserId { get; set; }
        public string WorkingTitle { get; set; }
        public string LegacyStatus { get; set; }
        public string ApprovalStatusDesc { get; set; }
    }
}