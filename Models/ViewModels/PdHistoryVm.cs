﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class PdHistoryVm : BaseVm
    {
        public List<PositionDescriptionRow> History { get; set; }
//        public int PositionId { get; set; }
        public Position Pos { get; set; }
    }
}