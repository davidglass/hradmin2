﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HrAdmin2.Models;

namespace HrAdmin2.Models.ViewModels
{
    public class PositionVm : BaseVm
    {
        private HrAdminDbContext db;
        private Position model;
        public PositionVm(Position p, HrAdminDbContext dbctx)
        {
            db = dbctx;
            model = p;
            Init();
        }

        private void Init() {
            var chain = db.GetByProc<ChainLink>("GetChainOfCommandWithDirectReports", parameters: new { Id = this.Pos.Id });
            ChainOfCommand = new ChainLinkVm(chain);
            OpenPars = db.GetByProc<Par>("GetOpenPars", parameters: new { PositionId = Pos.Id });
            ApptHistory = db.GetByProc<PositionAppt>("GetPositionApptHistory", parameters: new { PositionId = Pos.Id });
            try
            {
                PositionDescriptions = db.GetByProc<PositionDescriptionRow>("GetPositionDescriptions", parameters: new { PositionId = Pos.Id });
            }
            catch (Exception xcp)
            {
            }
        }

        public Position Pos
        {
            get
            {
                return model;
            }
        }

        public int Id
        {
            get
            {
                return model.Id;
            }
        }

        public ChainLinkVm ChainOfCommand;
        public List<Par> OpenPars;
        public List<PositionAppt> ApptHistory;
        public List<PositionDescriptionRow> PositionDescriptions;
    }

    // TODO: nix property wrapper, just have Model property or the like:
    public class ChainLinkVm
    {
        private ChainLink model;
        public ChainLinkVm(List<ChainLink> cl)
        {
            model = cl[0];
            AddSubNodes(this, cl, 1);
        }

        private void AddSubNodes(ChainLinkVm root, List<ChainLink> cl, int startIndex)
        {
            for (int i = startIndex; i < cl.Count; i++)
            {
                if (cl[i].Level == model.Level - 1)
                {
                    var sp = new ChainLinkVm(cl[i]);
                    SubPositions.Add(sp);
                    sp.AddSubNodes(sp, cl, i);
                }
            }
        }

        public ChainLinkVm(ChainLink cl)
        {
            model = cl;
        }
        public List<ChainLinkVm> SubPositions = new List<ChainLinkVm>();

        public int Id
        {
            get { return model.Id; }
        }

        public int? EmployeeId
        {
            get { return model.EmployeeId; }
        }

        public int? PdfId
        {
            get { return model.PdfId; }
        }

        public int? KeyCompId
        {
            get { return model.KeyCompId; }
        }

        public int? WmsId
        {
            get { return model.WmsId; }
        }

        public string JobTitle
        {
            get { return model.JobTitle; }
        }

        public string WorkingTitle
        {
            get
            {
                return "Working Title (hardcoded)";
            }
        }

        public string IncumbentLastName
        {
            get
            {
                return model.IncumbentLastName;
            }
        }

        public string IncumbentFirstName {
            get
            {
                return model.IncumbentFirstName;
            }
        }

        public bool IsVacant
        {
            get { return model.IsVacant; }
        }

        public string LegacyPositionNum
        {
            get { return model.LegacyPositionNum; }
        }
    }

    public class ChainLink : ModelBase
    {
        public string JobTitle { get; set; }
        public bool IsVacant { get; set; }
        public string IncumbentLastName { get; set; }
        public string IncumbentFirstName { get; set; }
        public int Level { get; set; }
        public int? EmployeeId { get; set; }
        public int? PdfId { get; set; }
        public int? KeyCompId { get; set; }
        public int? WmsId { get; set; }
        public string LegacyPositionNum { get; set; }
    }
}