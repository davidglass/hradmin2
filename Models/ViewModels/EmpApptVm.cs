﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class EmpApptVm
    {
        private Appointment model;
        public EmpApptVm(Appointment a)
        {
            model = a;
        }

        public static List<EmpApptVm> GetHistory(List<Appointment> ah)
        {
            var vmh = new List<EmpApptVm>();
            foreach (var a in ah)
            {
                vmh.Add(new EmpApptVm(a));
            }
            return vmh;
        }

        public int Id
        {
            get { return model.Id; }
        }

        public int PositionId
        {
            get
            {
                return model.PositionId;
            }
        }

        public string StartDate {
            get
            {
                return model.StartDate.ToString("yyyy-MM-dd");
            }
        }

        public string EndDate
        {
            get
            {
                return model.EndDate.HasValue ? model.EndDate.Value.ToString("yyyy-MM-dd")
                    : String.Empty;
            }
        }
    }
}