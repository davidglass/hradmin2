﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class PositionHierarchyVm : BaseVm
    {
        private PositionHierarchy model;
        public PositionHierarchyVm(List<PositionHierarchy> poslist)
        {
            Init(poslist[0]);
            BuildTree(poslist);
        }

        // private individual node constructor, used by BuildTree
        private PositionHierarchyVm(PositionHierarchy ph)
        {
            Init(ph);
        }

        private void Init(PositionHierarchy ph)
        {
            model = ph;
            DR = new List<PositionHierarchyVm>(); // Direct Reports
        }

        private void BuildTree(List<PositionHierarchy> nodes)
        {
            //var nt = new List<PositionHierarchyVm>(); // nesting tracker
            //nt.Add(this);
            var nt = new List<PositionHierarchyVm> { this };
            int rootlevel = model.Depth;
            int level = rootlevel;
            for (int i = 1; i < nodes.Count; i++)
            {
                var n = new PositionHierarchyVm(nodes[i]);
                var depth = n.model.Depth;
                if (depth > level)
                {
                    if (nt.Count >= depth - rootlevel + 1)
                    {
                        nt[depth - rootlevel] = n;
                    }
                    else
                    {
                        nt.Add(n);
                    }
                    nt[level - rootlevel].DR.Add(nt[depth - rootlevel]);
                }
                else if (depth <= level && depth > rootlevel)
                {
                    nt[depth - rootlevel] = n;
                    nt[depth - rootlevel - 1].DR.Add(n);
                }
                // this should only happen if multiple roots are allowed.
                else if (depth == rootlevel)
                {
                    //tl.Add(n);
                    nt[0] = n;
                }
                level = depth;
            }
        }

        public PositionHierarchy P {
            get
            {
                return model;
            }
        }

        public List<PositionHierarchyVm> DR { get; set; }
    }
}