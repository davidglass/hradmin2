﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalDelegateVm
    {

        private HrAdminDbContext db;

        public ApprovalDelegate Delegate { get; set; }
        public string DelegatedToLastName {get; set;}
        public string DelegatedToPreferredName { get; set; }
        public string DelegatedPositionTitle { get; set; }
        public string DelegatedPositionPersonLastName { get; set; }
        public string DelegatedPositionPersonPreferredName { get; set; }
        public int DelegatedToId { get; set; }
        public string DelegatedToEmail { get; set; }
        public string DelegatedPositionEmail { get; set; }
        public int? DelegatedPositionPersonId { get; set; }
		public DateTime CreationDate { get; set; }
		public DateTime DeleteDate { get; set; }
		public string CreateEmpLastName { get; set; }
		public string CreateEmpPreferredName { get; set; }
		public string DeleteEmpLastName { get; set; }
		public string DeleteEmpPreferredName { get; set; }

        public DateTime StartDate
        {
            get
            {
                return Delegate.StartDate;
            }
        }

        public DateTime EndDate
        {
            get
            {
                return Delegate.EndDate;
            }
        }

        public String DelegatedPositionIdString
        {
            get
            {
                return Delegate.DelegatedPositionId.ToString();
            }
        }

        public String StartDateString
        {
            get
            {
                return Delegate.StartDate.ToString("MM/dd/yy");
            }
        }

        public String EndDateString
        {
            get
            {
                if (Delegate.Indefinite) return "Indefinite";
                return Delegate.EndDate.ToString("MM/dd/yy");
            }
        }

        public ApprovalDelegateVm() { Delegate = new ApprovalDelegate(); }

        public ApprovalDelegateVm(ApprovalDelegate d, HrAdminDbContext db)
        {
            this.db = db;
            Delegate = d;
            Init(d.Id);
            
        }

        private void Init(int id)
        {
            ApprovalDelegateVm tempDelegate = db.GetSingleByProc<ApprovalDelegateVm>("GetApprovalDelegateVm",
                parameters: new { Id = id });

            DelegatedToLastName = tempDelegate.DelegatedToLastName;
            DelegatedToPreferredName = tempDelegate.DelegatedToPreferredName;
            DelegatedPositionTitle = tempDelegate.DelegatedPositionTitle;
            DelegatedPositionPersonLastName = tempDelegate.DelegatedPositionPersonLastName;
            DelegatedPositionPersonPreferredName = tempDelegate.DelegatedPositionPersonPreferredName;
            DelegatedToId = tempDelegate.DelegatedToId;
            DelegatedToEmail = tempDelegate.DelegatedToEmail;
            DelegatedPositionEmail = tempDelegate.DelegatedPositionEmail;
            DelegatedPositionPersonId = tempDelegate.DelegatedPositionPersonId;
		    CreationDate = tempDelegate.CreationDate;
		    DeleteDate = tempDelegate.DeleteDate;
		    CreateEmpLastName = tempDelegate.CreateEmpLastName;
		    CreateEmpPreferredName = tempDelegate.CreateEmpPreferredName;
		    DeleteEmpLastName = tempDelegate.DeleteEmpLastName;
		    DeleteEmpPreferredName = tempDelegate.DeleteEmpPreferredName;
            
        }
    }
}