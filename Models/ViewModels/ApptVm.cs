﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using HrAdmin2.Models;

namespace HrAdmin2.Models.ViewModels
{
    public class ApptVm : ModelBase
    {
        public int EmpId { get; set; } // vacant position should use 0 for EmpId
        public int? PersonnelNumber { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string StreetLine1 { get; set; }
        public string CountyId { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string HomePhone { get; set; }
        public DateTime? AnniversaryDate { get; set; }
    }
}