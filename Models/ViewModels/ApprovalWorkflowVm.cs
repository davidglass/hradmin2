﻿using HrAdmin2.Controllers;
using HrAdmin2.Controllers.Api;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalWorkflowVm
    {
        private ApprovalWorkflow workflow;
        private HrAdminDbContext db;

        #region Properties

        public ApprovalWorkflow Workflow {
            get { return workflow; }            
        }

        public List<ApprovalTaskVm> Tasks { get; set; }
        
        /// <summary>
        /// Stops task list after first appointing authority that is not vacant. 
        /// </summary>
        public List<ApprovalTaskVm> DisplayTasks {
            get
            {
                //generate list by going through Task list
                List<ApprovalTaskVm> displayTasks = new List<ApprovalTaskVm>();                
                int i = 0;
                foreach (var task in Tasks)
                {
                    displayTasks.Add(task);
                    
                    i++; //i keeps track of spot in list

                    //show complete list if one of the remaining tasks is active or completed
                    //check to see if at potental end of list. Looking for task assigned to appointing authority,
                    //who is not vacant, and the task has not been canceled in advance.
                    if (task.ApproverPositionIsAppointingAuth && task.ApproverPositionPersonPreferredName != "" 
                        && task.Task.StatusCode != ApprovalWorkflow.statusCode.Canceled)
                    {
                        //Done if at end of list
                        if(i == Tasks.Count) break;

                        //create sublist of remaining tasks
                        List<ApprovalTaskVm> RemainingTasks = Tasks.GetRange(i, Tasks.Count - i);
                        
                        int activeOrCompleted = RemainingTasks.Count(
                            n => (n.Task.StatusCode != ApprovalWorkflow.statusCode.Canceled 
                                && n.Task.StatusCode != ApprovalWorkflow.statusCode.Created));
                        
                        if(activeOrCompleted < 1)
                            break;
                    }
                    
                }
                //go through remaining tasks and grab any hr tasks attached at end
                for (int j = i; j < Tasks.Count; j++)
                {
                    if (Tasks[j].ApproveGroupName == "HR") displayTasks.Add(Tasks[j]);
                }

                return displayTasks;
            }
            set
            {
                //save list back to Task
                int i = 0;
                foreach (var task in value)
                {
                    Tasks[i++] = task;                    
                }
            }
        }


        /// <summary>
        /// First task that isn't canceled.
        /// TODO: should be able to delete
        /// </summary>
        public ApprovalTaskVm FirstTask
        {
            get
            {
                if (Tasks.Count > 0)
                {
                    foreach (var task in Tasks)
                    {
                        if (task.Task.StatusCode != ApprovalWorkflow.statusCode.Canceled)
                            return task;
                    }
                }
                
                return new ApprovalTaskVm();
            }
        }

        public String Status
        {
            get
            {
                return Workflow.StatusCode.ToString();
            }
        }

        #endregion

        #region Construction

        public ApprovalWorkflowVm(int? id)
        {
            Tasks = new List<ApprovalTaskVm>();

            if (!id.HasValue || id <= 0 )
            {
                SetupDefaultValues();

            }
            else
            {
                db = new HrAdminDbContext();
                Init((int)id);
            }
        }

        private void Init(int id)
        {

            try
            {
                //get workflow data
                workflow = db.GetSingleByProc<ApprovalWorkflow>("GetApprovalWorkflow",
                    parameters: new { Id = id });

                //get task list
                workflow.ApprovalTasks = db.GetByProc<ApprovalTask>("GetApprovalWorkflowTasks",
                    parameters: new { WorkflowId = id });

                foreach (ApprovalTask task in workflow.ApprovalTasks)
                {
                    Tasks.Add(new ApprovalTaskVm(task, db));
                }
                //for PAR make last task required
                Tasks.Last().Required = true;
            }
            catch
            {
                SetupDefaultValues();
            }
        }

        private void SetupDefaultValues()
        {
            workflow = new ApprovalWorkflow();
        }

        #endregion

        public void SetAuthorization(string lanId){
            try
            {
                //TODO: optomize performance: currently a lot of sql server calls.                        
                foreach (var task in Tasks)
                {
                    var ac = db.GetSingleByProc<AccessCheck>("EmpIsInSpanOfControl", new { EmployeeId = task.ApproverPositionEmployeeId, LanId = lanId });
                    if (ac.IsSub)
                    {
                        task.Authorized = true;
                    }
                    else
                    {
                        //check delegates
                        foreach (var del in task.ActiveDelegates)
                        {
                            if (db.GetSingleByProc<AccessCheck>("EmpIsInSpanOfControl", new { EmployeeId = del.Id, LanId = lanId }).IsSub)
                            {
                                task.Authorized = true;
                                break;
                            }
                        }
                    }
                }

            }catch{
                //authorization error, add nlog.
            };
            
        }
    }


}