﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace HrAdmin2.Models.ViewModels
{
    public class ApprovalVm : BaseVm
    {
        private HrAdminDbContext db;

        public List<ApprovalDelegateVm> Delegates { get; set; }

        public List<ApprovalParVm> ParApprovals { get; set; }

        public List<ApprovalAssignedDelegateVm> AssignedDelegates { get; set; }

        public List<ApprovalDelegateVm> FutureAssignedDelegates { get; set; }

        
        //used for drop down list when creating new delegate
        public List<SelectListItem> CurrentEmp { get; set; }

        public Position ApproverPosition { get; set; }

        //first employee match to position in appointment table
        public Employee ApproverEmployee { get; set; }

        public string ApprovalCount
        {
            get
            {
                return ParApprovals.Count.ToString();
            }
        }

        public string DelegateCount
        {
            get
            {
                return Delegates.Count.ToString();
            }
        }

        public string FutureAssignedDelegatesCount
        {
            get
            {
                return FutureAssignedDelegates.Count.ToString();
            }
        }

        public string AssignedDelegatesCount
        {
            get
            {
                int count = 0;

                foreach(var delVm in AssignedDelegates)
                {
                    count += delVm.DelegatedParApprovals.Count;
                }

                return count.ToString();
            }
        }


        public ApprovalVm(int positionId, HrAdminDbContext dbctx)
        {
            db = dbctx;
            //model = p;
            try
            {
                Init(positionId);
            }
            catch
            {

            }
        }

        public ApprovalVm(int positionId)
        {
            db = new HrAdminDbContext();
            //model = p;
            try
            {
                Init(positionId);
            }
            catch
            {

            }
        }

        private void Init(int approverPositionId)
        {
            //get position details
            ApproverPosition = db.GetByProc<Position>("GetPosition",
                parameters: new { Id = approverPositionId })[0];

            //gets active PAR approvals
            ParApprovals = db.GetByProc<ApprovalParVm>("GetApprovalParVms",
                parameters: new { ApproverPositionId = approverPositionId });

            //load ApprovalTaskVms based off Ids.
            foreach (var approval in ParApprovals)
            {
                approval.LoadTaskData();
            }



            //Find all delegate work assigned to employee at this position.
            ApproverEmployee = new Employee() { LastName = "Vacant", FirstName = "" };
            var EmployeeList = db.GetByProc<Employee>("GetEmployeeIdOfPosition",
                parameters: new { PositionId = approverPositionId });
            AssignedDelegates = new List<ApprovalAssignedDelegateVm>();
            FutureAssignedDelegates = new List<ApprovalDelegateVm>();

            if (EmployeeList.Count > 0)
            {
                ApproverEmployee = EmployeeList[0];

                List<ApprovalDelegate> assignedDelegates = db.GetByProc<ApprovalDelegate>("GetApprovalDelegatesOfEmployee",
                    parameters: new { EmployeeId = ApproverEmployee.Id });

                foreach (var del in assignedDelegates)
                {
                    //get app list
                    del.Apps = db.GetByProc<ApprovalApp>("GetApprovalDelegateApps",
                        parameters: new { DelegateId = del.Id });

                    if (del.StartDate > DateTime.Now)
                    {
                        FutureAssignedDelegates.Add(new ApprovalDelegateVm(del, db));
                    }
                    else
                    {
                        AssignedDelegates.Add(new ApprovalAssignedDelegateVm(new ApprovalDelegateVm(del, db), db));
                    }
                }
            }
            



            //GET approval delegate list of position (future and current), then populate vms.
            List<ApprovalDelegate> approvalDelegates = db.GetByProc<ApprovalDelegate>("GetApprovalDelegatesOfPosition",
                parameters: new { PositionId = approverPositionId });

            //load ApprovalDelegateVms based off delegates.
            Delegates = new List<ApprovalDelegateVm>();
            foreach (var approvalDelegate in approvalDelegates)
            {
                //get app list
                approvalDelegate.Apps = db.GetByProc<ApprovalApp>("GetApprovalDelegateApps",
                    parameters: new { DelegateId = approvalDelegate.Id });

                Delegates.Add(new ApprovalDelegateVm(approvalDelegate, db));
            }


            //load employee select list for delegate creation
            CurrentEmp = new List<SelectListItem>();
            CurrentEmp = db.GetByProc<SelectListItem>("GetCurrentEmpSelection", parameters: new { });

        }
    }
}