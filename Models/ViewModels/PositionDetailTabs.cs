﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models.ViewModels
{
    public class PositionDetailTabs
    {
        private PositionVm _pvm;
//        private List<ApptTab> _appts;
//        private List<EmployeeAppt> _appts;
        private List<EmployeeApptVm> _appts;
        private HrAdminDbContext _db;

        public PositionDetailTabs(PositionVm pvm, HrAdminDbContext db)
        {
            _pvm = pvm;
            _db = db;
            Init();
        }

        private void Init()
        {
            _appts = new List<EmployeeApptVm>();
            var aptList = _db.GetByProc<EmployeeAppt>("GetIncumbentAppts", parameters: new { PosId = _pvm.Pos.Id });
            foreach (EmployeeAppt ea in aptList)
            {
                _appts.Add(new EmployeeApptVm(ea, _db));
            }
        }

        public PositionVm Position
        {
            get
            {
                return _pvm;
            }
        }

        public List<EmployeeApptVm> Appointments
//        public List<EmployeeAppt> Appointments
        //public List<ApptTab> Appointments
        {
            get
            {
                return _appts;
            }
        }
    }
}