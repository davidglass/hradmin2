﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
     

    public class EmployeeDetail : ModelBase
    {
        

        public int EmployeeId { get; set; }
        public string LanId { get; set; }
        public int HrmsPersonId { get; set; }
        //public string SSN { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MiddleName { get; set; }
        public string PreferredName { get; set; }
        public string Suffix { get; set; }
        public DateTime? LastUpdated { get; set; }
        public DateTime? DOB { get; set; }
        public string Gender { get; set; } // "1" = male, "2" = female
        public string EmailAddress { get; set; }
        public string GenderDesc { get; set; }
        public string MaritalStatus { get; set; }
        public string MaritalStatusDesc { get; set; }
        public int PositionId { get; set; }
        public int OrgUnitId { get; set; }
        public string OrgUnitDesc { get; set; }
        public int CostCenter { get; set; }
        public int JobId { get; set; }
        public string JobDesc { get; set; }
        public string JobTitle { get; set; }
        public string LegacyClassCode { get; set; }
        public string LegacyClassCodeDesc { get; set; }
        public string LegacyPositionNum { get; set; }
        public int LegacyUnitId { get; set; }
        public DateTime? LegacyLastUpdated { get; set; }
        public string WorkingTitle { get; set; }
        public int HrmsPositionId { get; set; }
        public int SupervisorPositionId { get; set; }
        public bool IsAbolished { get; set; }
        public bool CBA { get; set; }
        public DateTime? ValidFromDate { get; set; }
        public DateTime? ValidToDate { get; set; }
        public DateTime? x_created { get; set; }
        public bool IsInTraining { get; set; }
        public string GeneralDescription { get; set; }
        public string PersonnelSubArea { get; set; }
        public string PersonnelSubAreaDesc { get; set; }
        public bool IsBackgroundCheckRequired { get; set; }
        public string WorkersCompCode { get; set; }
        public string WorkersCompDesc { get; set; }
        public bool IsSupervisor { get; set; }
        public bool IsManager { get; set; }
        public bool IsAppointingAuthority { get; set; }
        public string EeGroup { get; set; }
        public string EeGroupDesc { get; set; }
        public string EeSubGroup { get; set; }
        public string EeSubGroupDesc { get; set; }
        public bool IsSection4 { get; set; }
        public string PayGradeType { get; set; }
        public string PayGradeArea { get; set; }
        public string PayGradeTypeAreaDesc { get; set; }
        public string PayGrade { get; set; }
        public bool IsTandem { get; set; }
        public int LocationId { get; set; }
        public string LocationDesc { get; set; }
        public string OfficeLoc { get; set; }
        public string DefaultWorkScheduleCode { get; set; }
        public string DefaultWorkScheduleDesc { get; set; }
        public string WorkHoursWk1 { get; set; } // in Appointment
        public string WorkHoursWk2 { get; set; }
        public int AppointmentId { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool IsPrimary { get; set; }
        public int FillLevelJobId { get; set; }
        public string FillLevelDesc { get; set; }
        public string FillLevelClassCode { get; set; }
        public string ContractTypeCode { get; set; }
        public string WorkContractTypeDesc { get; set; }
        public string WorkScheduleCode { get; set; }
        public string EmpWorkScheduleDesc { get; set; }
        public decimal EmploymentPercent { get; set; }
        public DateTime? ContractEndDate { get; set; }
        public int PayScaleTypeArea { get; set; }
        public string PayScaleReason { get; set; }
        public string PayScaleReasonDesc { get; set; }
	    public DateTime? PayScaleEffectiveDate { get; set; }
	    public string PayScaleGroup { get; set; }
        public string PayScaleLevel { get; set; }
        public DateTime? PayNextIncrease { get; set; }
        public DateTime? StepMEligibleDate { get; set; }
        public decimal PayCapUtilityLevel { get; set; }
        public decimal PayAnnualSalary { get; set; }
        public decimal PayHourlyRate { get; set; }
        public bool PayShiftDifferential { get; set; }
        public bool StandbyPay { get; set; }
	    public DateTime? AnniversaryDT { get; set; }
        public DateTime? AppointmentDT { get; set; }
        public DateTime? OriginalHireDT { get; set; }
        public DateTime? PriorPidDT { get; set; }
        public DateTime? SeniorityDT { get; set; }
        public DateTime? UnbrokenServiceDT { get; set; }
        public DateTime? VacLvFrozenUpToDT { get; set; } 
        public DateTime? PrsnlHolidayElgbltyDT { get; set; }
        public DateTime? PrsnlLVDayElgbltyDT { get; set; }
        public string DesignatedWorkWeek { get; set; }
        public string Email { get; set; }
        public string WorkPhone { get; set; }
        public bool Disability { get; set; }
        public bool Hispanic { get; set; }
        public string MilBranchCodes { get; set; }
        public string MilStatusCodes { get; set; }
        public string TimeMgtStatus { get; set; }
        public string TimeMgtStatusDesc { get; set; }
        public string ShiftCode { get; set; }
        public string ShiftDesignation { get; set; }
        public string AppRole { get; set; }

        public CurrentUserInfo UserInfo { get; set; }

        public List<EmployeeAddress> Address { get; set; }
        public List<EmergencyContacts> EmerContacts { get; set; }
        public List<EmployeeLeave> Leave { get; set; }

      
        
    }
     
}


