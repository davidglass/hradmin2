﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace HrAdmin2.Models.Validators
{
    // this is a property-level validator.  TODO: probably should just have class-level validator.
    public class EmpHrmsIdValidator
    {
        public static ValidationResult Validate(int empHrmsId, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            if ((pd.ActionCode == "U6" || pd.IsTransferIn) && empHrmsId < 1)
            {
                return new ValidationResult("HRMS ID is required for this action type.");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

    //public class ActionReasonIdValidator
    // also being used for position-only pars...
    public class ApptRequiredIntValidator
    {
        public static ValidationResult Validate(int? arid, ValidationContext vc)
        {
            //ParDetail pd = (ParDetail)vc.ObjectInstance;
            Par pd = (Par)vc.ObjectInstance;
            if (pd.ActionCode.IndexOf('P') != 0 && vc.DisplayName.Equals("ActionReasonId") && ! pd.ActionReasonId.HasValue) //ignore this validation for Position PARs
            {
                return new ValidationResult("Action Reason (subtype) is required.");
            }
            else if (pd.ActionCode.IndexOf('P') != 0 && !pd.IsTransferOut)
            { // non Position PAR, requires Reason:
                if (!arid.HasValue)
                //if (!pd.ActionReasonId.HasValue)
                {
                    return new ValidationResult("A value is required.");
                }
                return ValidationResult.Success;
            }
            return ValidationResult.Success;
        }
    }

    public class PosRequiredIntValidator
    {
        public static ValidationResult Validate(int? val, ValidationContext vc)
        {
            if (!val.HasValue)
            {
                return new ValidationResult("A value is required.");
            }
            return ValidationResult.Success;
        }
    }

    public class ApptDecimalNumRange
    {
        public static ValidationResult Validate(Decimal num, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            if (pd.ActionCode.IndexOf('P') != 0 && ! pd.IsTransferOut) //ignore this validation for Position PARs
            { // non Position PAR, requires Contract Code:
                // *NOTE*, ContractCode Length should be 2, others may be 4 or ?
                if (!(num >= 1 && num <= 100))
                //if (!pd.ActionReasonId.HasValue)
                {
                    return new ValidationResult("value should be between 1 and 100");
                }
                return ValidationResult.Success;
            }
            return ValidationResult.Success;
        }

        public static ValidationResult ValidateSalary(Decimal num, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            if (pd.ActionCode.IndexOf('P') != 0 && !pd.IsTransferOut) //ignore this validation for Position PARs
            { // non Position PAR, requires Contract Code:
                // *NOTE*, ContractCode Length should be 2, others may be 4 or ?
                if (!(num >= 1 && num <= 200000))
                //if (!pd.ActionReasonId.HasValue)
                {
                    return new ValidationResult("value should be between 1 and 200000");
                }
                return ValidationResult.Success;
            }
            return ValidationResult.Success;
        }
    }

    public class CodeStringValidator
    {
        public static ValidationResult Validate(string code, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            // TODO: separate validator for Position CodeStrings...
            if (pd.ActionCode.IndexOf('P') != 0 && !pd.IsTransferOut) //ignore this validation for Position PARs
            { // non Position PAR, requires Contract Code:
                // *NOTE*, ContractCode Length should be 2, others may be 4 or ?
                if (String.IsNullOrEmpty(code))
                {
                    return new ValidationResult("A value is required.");
                }
                return ValidationResult.Success;
            }
            //else if (pd.IsTransferIn && pd.FromAgencyCode == "1630" && vc.DisplayName.Equals("FromAgencyCode"))
            //{
            //    return new ValidationResult("Transfer In requires a non-CTS From agency.");
            //}
            else if (pd.IsTransferOut && pd.TargetAgencyCode == "1630" && vc.DisplayName.Equals("TargetAgencyCode"))
            {
                return new ValidationResult("Transfer Out requires a non-CTS target agency.");
            }
            return ValidationResult.Success;
        }
    }

    public class NewHireSSN
    {
        public static ValidationResult Validate(String ssn, ValidationContext vc)
        {
            Par p = (Par)vc.ObjectInstance;
            if (p.ActionCode.Equals("U0") && String.IsNullOrEmpty(ssn))
            {
                return new ValidationResult("SSN is required for New Hires.");
            }
            if (p.ActionCode.Equals("U0") && !Regex.IsMatch(ssn, "^\\d(.?\\d){8}$")) {
                return new ValidationResult("Invalid SSN.");
            }
            return ValidationResult.Success;
        }
    }

    public class ParRequiredNullable
    {
        public static ValidationResult Validate(DateTime? val, ValidationContext vc)
        {
            Par p = (Par)vc.ObjectInstance;
            if (!val.HasValue)
            {
                // TODO: pass display name instead of member name???
//                return new ValidationResult("A value is required.");
                return new ValidationResult("A value is required.", new string[1] { vc.MemberName });
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

    public class PdRequiredNullable
    {
        public static ValidationResult Validate(DateTime? val, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            // exclude requirement from certain PAR Types, e.g. Separation and Position manipulation:
            if (!val.HasValue && pd.ActionCode != "U5" && pd.ActionCode.IndexOf('P') != 0 && ! pd.IsTransferOut)
            {
                return new ValidationResult("A value is required.");
            }
            else
            {
                return ValidationResult.Success;
            }
        }
    }

    public class RequiredString
    {
        public static ValidationResult Validate(String code, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            if (String.IsNullOrEmpty(code) && !pd.IsTransferOut)
            //if (!pd.ActionReasonId.HasValue)
            {
                return new ValidationResult("A value is required.");
            }
            return ValidationResult.Success;
        }
    }

    public class NewHireRequiredNullableDate
    {
        public static ValidationResult Validate(DateTime? val, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            if (pd.ActionCode.Equals("U0"))
            {
                if (!val.HasValue)
                {
                    return new ValidationResult("A value is required.");
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return ValidationResult.Success;

        }
    }

    public class NewHireRequiredString
    {
        public static ValidationResult Validate(String code, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            if ((pd.ActionCode.Equals("U0") || pd.ActionCode.Equals("U6")) && String.IsNullOrEmpty(code) && !pd.IsTransferOut)
            //if (!pd.ActionReasonId.HasValue)
            {
                return new ValidationResult("A value is required.");
            }
            return ValidationResult.Success;
        }
    }

    public class ApptRequiredNullable
    {
        public static ValidationResult Validate(DateTime? val, ValidationContext vc)
        {
            ParDetail pd = (ParDetail)vc.ObjectInstance;
            // TODO: separate validator for Position CodeStrings...
            if (pd.ActionCode.IndexOf('P') != 0 && pd.ActionCode != "U5" && !pd.IsTransferOut) //ignore this validation for Position PARs
            { // non Position PAR, requires Contract Code:
                if (!val.HasValue)
                {
                    return new ValidationResult("A value is required.");
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return ValidationResult.Success;

        }
    }

    public class ParRequiredString
    {
        public static ValidationResult Validate(String val, ValidationContext vc)
        {
            Par pd = (Par)vc.ObjectInstance;
            // TODO: separate validator for Position CodeStrings...
            if (pd.ActionCode.IndexOf('P') != 0 && pd.ActionCode != "U5" && !pd.IsTransferOut) //ignore this validation for Position PARs
            { // non Position PAR, requires Contract Code:
                if (String.IsNullOrEmpty(val))
                {
                    return new ValidationResult("A value is required.");
                }
                else
                {
                    return ValidationResult.Success;
                }
            }
            return ValidationResult.Success;

        }
    }


    //public class ParDetailValidator
    //{
    //    public static ValidationResult Validate(ParDetail pd, ValidationContext vc)
    //    {
    //        List<string> errFields = new List<string>();
    //        if (pd.ActionCode.IndexOf('P') != 0) { // non Position PAR, requires Reason:
    //            if (!pd.ActionReasonId.HasValue)
    //            {
    //                errFields.Add("ActionReasonId");
    //            }
    //        }
    //        return errFields.Count == 0 ? ValidationResult.Success : new ValidationResult("ParDetail Validation Error", errFields);
    //    }
    //}
}