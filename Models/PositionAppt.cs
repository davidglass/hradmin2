﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class PositionAppt
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool CurrentEmp { get; set; }
    }
}