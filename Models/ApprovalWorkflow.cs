﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ApprovalWorkflow
    {
        public ApprovalWorkflow()
        {
            this.ApprovalTasks = new List<ApprovalTask>();
            this.EmailSubscriptions = new List<String>();
        }

        public enum approvalCode
        {
            AppointingAuthority = 7,
            FirstApproval = 8,
            AppointingWithReview = 9
        }

        public enum statusCode
        {
            Active = 1,
            Canceled = 2,
            Approved = 3,
            Rejected = 4,
            Created = 5
        }

        public enum approvalApp
        {
            PAR = 1,
            PDF = 2
        }

        public int Id { get; set; }
        public statusCode StatusCode { get; set; }
        public approvalCode ApprovalCode { get; set; }
        public approvalApp App { get; set; }
        public int? PositionId { get; set; } 
        public int? PersonId { get; set; } 
        public DateTime CreatedDate { get; set; }
        public List<ApprovalTask> ApprovalTasks { get; set; }
        public List<String> EmailSubscriptions { get; set; }

        //used for hr override, saved to db as new task
        public string HrOverrideComment { get; set; }
        public int? HrOverridePersonId { get; set; }
        public string HrOverridePersonLanId { get; set; }
    }
}