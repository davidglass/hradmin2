﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    // (int Id is inherited from ModelBase)
    public class ParAttachment : ModelBase
    {
        public int ParId { get; set; }
        public string FileName { get; set; }
    }
}