﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace HrAdmin2.Models
{
    public class ExpiringContract : ModelBase
    {
        public int EmpId { get; set; }
        public string EmpName { get; set; }

        public int PosId { get; set; }
        public string PosTitle { get; set; }

        public string ContractType { get; set; }
        public DateTime ApptStart { get; set; }  // may differ from Appointment.StartDate (e.g. complete InTraining changes former)
        public DateTime ContractEnd { get; set; }
        public int DaysLeft { get; set; }
    }
}