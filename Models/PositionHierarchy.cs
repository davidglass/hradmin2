﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;

namespace HrAdmin2.Models
{
    public class PositionHierarchy : ModelBase
    {
        // (base.Id = PositionId)
        // short property names to optimize JSON payload:
        public string T { get; set; } // Title
        public bool V { get; set; } // IsVacant
        public bool SV { get; set; } // IsSupervisor
        public bool MF { get; set; } // Multi-Filled
        public string SN { get; set; } // SurName (last name) of current or most recent incumbent
        public int EmpId { get; set; } // Id of current or most recent incumbent
        public string OU { get; set; }
        public string ShiftCode { get; set; }
        public DateTime ApptStartDate { get; set; }
        public DateTime? ApptEndDate { get; set; }
        [ScriptIgnore]
        public int Depth { get; set; }
        // DirectReports goes in VM:
        // public List<PositionHierarchy> DirectReports {
        // hide base Validate:
        [ScriptIgnore]
        new bool Validate { get; set; }
    }
}