﻿using System;
using System.Data.Entity;
using System.Collections.Generic;

using System.Data.Entity.ModelConfiguration.Conventions;
using HrAdmin2.Models.Util;

namespace HrAdmin2.Models
{
    public class HrAdminDbContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, add the following
        // code to the Application_Start method in your Global.asax file.
        // Note: this will destroy and re-create your database with every model change.
        // 
        // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<HrAdmin2.Models.LocalHrDbContext>());

        public HrAdminDbContext() : base("name=HrAdminDbContext")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
        }

        public bool Exec(string procName, dynamic parameters)
        {
            return new SmartProc(procName, Database.Connection.ConnectionString, parameters).Exec();
        }

        public List<T> GetByProc<T>(string procName, dynamic parameters)
        {
            var sp = new SmartProc(procName, Database.Connection.ConnectionString, parameters);
            List<T> result = sp.All<T>();
            return result;
        }

        public T GetSingleByProc<T>(string procName, dynamic parameters)
        {
            var r = GetByProc<T>(procName, parameters);
            if (r.Count != 1)
            {
                // TODO: Logging util
                throw new Exception("non-single result");
            }
            return r[0];
        }

        //public DbSet<PositionAppointment> PositionAppointments { get; set; }
        //public DbSet<Employee> Employees { get; set; }
        //public DbSet<Position> Positions { get; set; }
        // TODO: decide what to call theses...Requests? Pars?
        // public DbSet<ApptActionRequest> Requests { get; set; }
        public DbSet<AppUser> AppUsers { get; set; }
        public DbSet<CostCenter> CostCenters { get; set; }
        public DbSet<Par> Pars { get; set; }
        public DbSet<ParEmail> ParEmails { get; set; }
        public DbSet<PositionDescription> PositionDescriptions { get; set; }
        public DbSet<PositionDuty> PositionDuties { get; set; }
        public DbSet<OrgUnit> OrgUnits { get; set; }
        public DbSet<JobClass> JobClasses { get; set; }
        public DbSet<ChecklistTable> Checklists { get; set; }
        public DbSet<ChecklistEmail> ChecklistEmails { get; set; }
    }
}