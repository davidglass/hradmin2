﻿using System.Web;
using System.Web.Optimization;

namespace HrAdmin2
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-1.10.2.js"));
                        //"~/Scripts/jquery.maskMoney_min.js",
                        //"~/Scripts/jquery.maskedinput_min.js"));
//                        "~/Scripts/jquery-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-1.10.3.min.js"));
            // renamed minified file since default config ignores them!!!
            //bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
            //            "~/Scripts/jquery-ui-1.10.3.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*",
            //            "~/Scripts/jquery.validate*"));
            // wildcard version match grabs debug version!
            //bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
            //"~/Scripts/knockout-{version}.js"));
//            bundles.Add(new ScriptBundle("~/bundles/knockout").Include(
////            "~/Scripts/knockout-2.2.0.js",
//            "~/Scripts/knockout-2.2.1.js",
//            "~/Scripts/knockout.mapping-latest.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
        }
    }
}