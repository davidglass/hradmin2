﻿using System.Web;
using System.Web.Mvc;
using System.Web.Http.Filters;
using HrAdmin2.Filters;

namespace HrAdmin2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }

        // http://adotnetlife.blogspot.com/2013/01/mvcnet-4-webapi-server-side-model.html
        public static void RegisterHttpFilters(HttpFilterCollection filters)
        {
            filters.Add(new ValidationActionFilter());
        }
    }
}