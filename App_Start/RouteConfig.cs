﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace HrAdmin2
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "ModelDetail",
                url: "{controller}/{id}",
                defaults: new { action = "Detail" }, // *NOTE*, id param is *not* optional for this route
                constraints: new { id = @"\d+" }
            ); // id d+ constraint added to prevent matching /controller/new

            // par/{id}/edit is currently not in use; instead, ViewMode property of viewmodel is toggled for read/edit in Detail.
            routes.MapRoute(
                name: "ModelEdit",
                url: "{controller}/{id}/edit",
                defaults: new { action = "Edit" }, // *NOTE*, id param is *not* optional for this route
                constraints: new { id = @"\d+" }
            ); // id d+ constraint added to prevent matching /controller/new


            // par/{id}/edit is currently not in use; instead, ViewMode property of viewmodel is toggled for read/edit in Detail.
            routes.MapRoute(
                name: "ApprovalRoute",
                url: "position/{id}/approval",
                defaults: new { Controller = "Approval", action="Detail" }, // *NOTE*, id param is *not* optional for this route
                constraints: new { id = @"\d+" }
            ); // id d+ constraint added to prevent matching /controller/new

            routes.MapRoute(
                name: "EmailPreview",
                url: "{controller}/{id}/email",
                defaults: new { action = "EmailPreview" }, // *NOTE*, id param is *not* optional for this route
                constraints: new { id = @"\d+" }
            ); // id d+ constraint added to prevent matching /controller/new

            routes.MapRoute(
                name: "EmailExpiringContract",
                url: "{controller}/email/expiringcontract/{empid}",
                defaults: new { action = "EmailPreviewExpiringContract" }, // *NOTE*, id param is *not* optional for this route
                constraints: new { empid = @"\d+" }
            ); // id d+ constraint added to prevent matching /controller/new

            routes.MapRoute(
                name: "AngularPartial",
                url: "ngpartial/template/{pview}",
                defaults: new
                {
                    controller = "NgPartial",
                    action = "Template"
                }
            );

            routes.MapRoute(          

                name: "AngularTemplate",
                url: "{controller}/partialtemplate",
                defaults: new { action = "GetPartialTemplate" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}",
//                defaults: new { controller = "OrgUnit", action = "Index", id = UrlParameter.Optional }
                //defaults: new { controller = "OrgUnit", action = "Index" }
                defaults: new {
                    controller = "PositionHierarchy",
                    action = "Index"
                    //id = UrlParameter.Optional
                }
// removing SpaController for this release DG 2014-09-22...
                //defaults: new
                //{
                //    controller = "spa",
                //    action = "Index"
                //    //fragment = "/myinfo"
                //}

                                //                linkText: "My Info",
                                //actionName: "Index",
                                //controllerName: "spa",
                                //protocol: null,
                                //hostName: null,
                                //fragment: "/myinfo", // AngularJS route...
                                //routeValues: new object { },
                                //htmlAttributes: new Object { })</li>

            );

           
        }
    }
}