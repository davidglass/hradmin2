﻿using System.Net;
using System.Net.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

// TODO: move these filters into separate project..
namespace HrAdmin2.Filters
{
    // http://adotnetlife.blogspot.com/2013/01/mvcnet-4-webapi-server-side-model.html
    // looks like above may have originated at MSFT, possibly not original author.

    public class ValidationActionFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            if (actionContext.Request.Method == HttpMethod.Get)
            {
                return; // bypass ModelState check for GET
            }
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.NotAcceptable, actionContext.ModelState);  // ModelState is processed on client via JSON.parse().
            }
        }
    }
}