﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using NLog;

namespace HrAdmin2.Filters
{
    public class LoggedExceptionFilterAttribute : ExceptionFilterAttribute
    {


        // inspired slightly by http://stackoverflow.com/questions/8144695/asp-net-mvc-custom-handleerror-filter-specify-view-based-on-exception-type
        //protected override void OnException(ExceptionContext filterContext)
        //{
        //    base.OnException(filterContext);
        //    try
        //    {
        //        HandleError(filterContext.Exception);
        //    }
        //    catch (Exception xcp)
        //    {
        //        throw new Exception("Error handling failed in BaseMvcController", xcp);
        //    }
        //    filterContext.Result = View("~/Views/Shared/Error.cshtml");
        //    filterContext.ExceptionHandled = true;
        //    filterContext.HttpContext.Response.Clear();
        //    filterContext.HttpContext.Response.StatusCode = (int)System.Net.HttpStatusCode.InternalServerError.GetTypeCode();
        //    filterContext.HttpContext.Response.TrySkipIisCustomErrors = true;
        //}

        protected void HandleError(Exception xcp)
        {
            var logger = LogManager.GetCurrentClassLogger();
            var oxcp = GetOriginalException(xcp);
            var lei = new LogEventInfo
            {
                Level = LogLevel.Error,
                Exception = oxcp,
                //                    Message = "Error occurred on PostEmail.  Sent Email object: " + Json.Encode(sent),
//                Message = oxcp.Message + " in " + this.GetType().FullName + ": " + System.Reflection.MethodBase.GetCurrentMethod().Name,
                Message = oxcp.Message + " in " + xcp.TargetSite.DeclaringType.FullName + ": " + xcp.TargetSite.Name,
                TimeStamp = DateTime.Now,
                LoggerName = logger.Name
            };
            // enumerate appSettings from web.config (TODO: use StringBuilder instead):
            var ws = System.Web.Configuration.WebConfigurationManager.AppSettings;
            lei.Message += "<br/><b>web.config appSettings:</b>";
            foreach (var k in ws.AllKeys)
            {
                lei.Message += "<br/>" + k + ": " + ws[k];
            }
            lei.SetStackTrace(new System.Diagnostics.StackTrace(oxcp), 0);
            // TODO: more detailed log of REST status, data, etc.
            logger.Log(lei);
        }

        private Exception GetOriginalException(Exception xcp)
        {
            return (xcp.InnerException == null) ? xcp : GetOriginalException(xcp.InnerException);
        }


        public override void OnException(HttpActionExecutedContext filterContext)
        {
            base.OnException(filterContext);
            try
            {
                HandleError(filterContext.Exception);
            }
            catch (Exception xcp)
            {
                throw new Exception("Error handling failed in BaseApiController", xcp);
            }
//            filterContext.Response = new HttpResponseMessage(HttpStatusCode.InternalServerError);
            filterContext.Response = new HttpResponseMessage()
                {
                    // *NOTE*, IE prevents access to response content on non-AJAX (hidden frame target) post.
                    // if http error status is returned, so omit or set to OK despite error here.
                    // TODO: set up error logging.
                    StatusCode = HttpStatusCode.InternalServerError,
                    Content = new StringContent(System.Web.Helpers.Json.Encode(
                        new
                        {
                            error = "Server Error: " + filterContext.Exception.Message
                        }))
                };
        }
    }
}