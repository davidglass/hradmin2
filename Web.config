﻿<?xml version="1.0" encoding="utf-8"?>
<!--
  For more information on how to configure your ASP.NET application, please visit
  http://go.microsoft.com/fwlink/?LinkId=169433
  -->
<configuration>
  <configSections>
    <!-- For more information on Entity Framework configuration, visit http://go.microsoft.com/fwlink/?LinkID=237468 -->
    <section name="entityFramework" type="System.Data.Entity.Internal.ConfigFile.EntityFrameworkSection, EntityFramework, Version=5.0.0.0, Culture=neutral, PublicKeyToken=b77a5c561934e089" requirePermission="false" />
    <section name="nlog" type="NLog.Config.ConfigSectionHandler, NLog"/>
  </configSections>
  <nlog xmlns="http://www.nlog-project.org/schemas/NLog.xsd"
        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
        throwExceptions="true"
        internalLogLevel="Error"
        internalLogFile="C:\logs\nlog-app.log">
    <!-- 
      internalLogFile="..\..\..\Logs\nlog-app.log">
  See http://nlog-project.org/wiki/Configuration_file 
  for information on customizing logging rules and outputs.
   -->
    <targets>
      <!-- file targets -->
      <target name="asyncFile" xsi:type="AsyncWrapper">
        <target xsi:type="File" name="f" fileName="${basedir}/Logs/${shortdate}.log"
              layout="${longdate} ${uppercase:${level}} ${message} ${aspnet-user-identity}"/>
      </target>
      <!-- database target attributes are set via config transform: -->
      <!--connectionString="data source=ctsl7dsa2175;initial catalog=HrDb;Integrated Security=True;Persist Security Info=True;Pooling=True"-->
      <!--connectionString="data source=ctsl7dsa2175.dis.wa.lcl;initial catalog=HrDb;User=sa;Password=Pur3siggy;Persist Security Info=True;Pooling=True"-->
      <target name="database" xsi:type="Database" keepConnection="true" useTransactions="true"
          dbProvider="System.Data.SqlClient"
          connectionString="data source=sql12;initial catalog=HrDb;User=ctswebapps;Password=(+$w3b@pps;Persist Security Info=True;Pooling=True"
          commandText="INSERT INTO Log(EventDateTime, EventLevel, UserName, MachineName, EventMessage, ErrorSource, ErrorClass, ErrorMessage, InnerErrorMessage, StackTrace) VALUES (@EventDateTime, @EventLevel, @UserName, @MachineName, @EventMessage, @ErrorSource, @ErrorClass, @ErrorMessage, @InnerErrorMessage, @StackTrace)"
       >
        <!-- parameters for the command -->
        <parameter name="@EventDateTime" layout="${date:s}" />
        <parameter name="@EventLevel" layout="${level}" />
        <parameter name="@UserName" layout="${aspnet-user-identity}" />
        <parameter name="@MachineName" layout="${machinename}" />
        <parameter name="@EventMessage" layout="${message}" />
        <!-- DG *NOTE*, Logger.Context dictionary is deprecated.  Use Properties instead. -->
        <parameter name="@ErrorSource" layout="${callsite}" />
        <parameter name="@ErrorClass" layout="${event-context:item=ErrorClass}" />
        <!--<parameter name="@ErrorMethod" layout="${}" />-->
        <parameter name="@ErrorMessage" layout="${event-context:item=ErrorMessage}" />
        <parameter name="@InnerErrorMessage" layout="${event-context:item=InnerErrorMessage}" />
        <parameter name="@StackTrace" layout="${stacktrace}" />
      </target>
      <!-- email target attributes are set via config transform: -->
      <target name="email"
              xsi:type="Mail"
              to="david.glass@cts.wa.gov"
              from="app-error@cts.wa.gov"
              subject="App Error on ${aspnet-request:serverVariable=HTTP_HOST}"
              body="&lt;b&gt;App User:&lt;/b&gt; ${aspnet-user-identity}&lt;br/&gt;
            &lt;b&gt;Error Time:&lt;/b&gt; ${date:s}&lt;br/&gt;
            &lt;b&gt;Logger:&lt;/b&gt; ${logger}&lt;br/&gt;
            &lt;b&gt;Stack Trace:&lt;/b&gt; ${stacktrace}&lt;br/&gt;
            &lt;b&gt;Error Message:&lt;/b&gt; ${message}&lt;br/&gt;"
              html="true"
              replaceNewlineWithBrTagInHtml="true"
              smtpServer="localhost">
      </target>
    </targets>
    <!-- rules set via config transform -->
    <rules />
  </nlog>
  <connectionStrings>
    <!-- IP address avoids DNS lookup, SQL login avoids DC auth (faster connection)  147.55.21.109 dev, .39 prod -->
    <!--<add name="DbConnStr" connectionString="Data Source=sql12;Initial Catalog=HrDb;User=ctswebapps;Password=(+$w3b@pps;Persist Security Info=True;Pooling=True" providerName="System.Data.SqlClient" />-->
    <add name="HrAdminDbContext"
         connectionString="Data Source=sql12;Initial Catalog=HrDb;User=ctswebapps;Password=(+$w3b@pps;Persist Security Info=True;Pooling=True"
         providerName="System.Data.SqlClient" 
         />
    <!--<add name="HrAdminDbContext"
         connectionString="Data Source=ctsl7dsa2175.dis.wa.lcl;Initial Catalog=HrDb;User=sa;Password=Pur3siggy;Persist Security Info=True;Pooling=True"
         providerName="System.Data.SqlClient" 
         />-->
  </connectionStrings>
  <appSettings>
    <add key="parEmailTo" value="david.glass@cts.wa.gov" />
    <!-- EmailToName currently is ignored to enable multiple EmailTos -->
    <add key="parEmailToName" value="DES HRMS Processing" />
    <add key="parEmailBcc" value="david.glass@cts.wa.gov" />
    <add key="checklistEmailToName" value="CTS Checklist Processor" />
    <add key="checklistEmailBcc" value="pauline.mckusick@cts.wa.gov" />
    <add key="smtpServer" value="localhost"/>
    <add key="webpages:Version" value="2.0.0.0" />
    <add key="webpages:Enabled" value="false" />
    <add key="PreserveLoginUrl" value="true" />
    <add key="ClientValidationEnabled" value="true" />
    <add key="UnobtrusiveJavaScriptEnabled" value="true" />
  </appSettings>
  <system.web>
    <compilation debug="true" targetFramework="4.5" />
    <customErrors mode="Off" />
    <!-- this is for ASP.NET 10MB -->
    <httpRuntime targetFramework="4.5" maxRequestLength="10000" />
    <!--<authentication mode="Windows" />-->
    <authorization>
      <deny users="?" />
    </authorization>
    <pages>
      <namespaces>
        <add namespace="System.Web.Helpers" />
        <add namespace="System.Web.Mvc" />
        <add namespace="System.Web.Mvc.Ajax" />
        <add namespace="System.Web.Mvc.Html" />
        <add namespace="System.Web.Optimization" />
        <add namespace="System.Web.Routing" />
        <add namespace="System.Web.WebPages" />
      </namespaces>
    </pages>
  </system.web>
  <system.webServer>
    <!-- DG added applicationInitialization, works with app init IIS extension. -->
    <!-- http://www.iis.net/downloads/microsoft/application-initialization -->
    <!-- **NOTE**, this only works in IIS 7.5. TODO: create, move to Web.ReleaseProd.config -->
    <!-- oddly, this doesn't work in Win7, even with IIS 7.5?  But doesn't hurt IIS Express, it seems...-->
    <!--<applicationInitialization>
      <add initializationPage="/" />
    </applicationInitialization>-->
    <!-- -->
    <!-- httpProtocol section, X-UA-Compatible header added to force standards-mode DG -->
    <httpProtocol>
      <customHeaders>
        <clear />
        <add name="X-UA-Compatible" value="IE=edge" />
      </customHeaders>
    </httpProtocol>
    <validation validateIntegratedModeConfiguration="false" />
    <handlers>
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" />
      <remove name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" />
      <remove name="ExtensionlessUrlHandler-Integrated-4.0" />
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_32bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness32" responseBufferLimit="0" />
      <add name="ExtensionlessUrlHandler-ISAPI-4.0_64bit" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" modules="IsapiModule" scriptProcessor="%windir%\Microsoft.NET\Framework64\v4.0.30319\aspnet_isapi.dll" preCondition="classicMode,runtimeVersionv4.0,bitness64" responseBufferLimit="0" />
      <add name="ExtensionlessUrlHandler-Integrated-4.0" path="*." verb="GET,HEAD,POST,DEBUG,PUT,DELETE,PATCH,OPTIONS" type="System.Web.Handlers.TransferRequestHandler" preCondition="integratedMode,runtimeVersionv4.0" />
    </handlers>
    <security>
      <!-- put this into applicationHost.config:
      <authentication>
        <windowsAuthentication enabled="true" authPersistNonNTLM="true">
        </windowsAuthentication>
      </authentication>
        -->
      <requestFiltering>
        <!-- maxAllowedContentLength is in bytes (B)  -->
        <!-- this is for IIS, 10MB -->
        <requestLimits maxAllowedContentLength="10000000" />
      </requestFiltering>
    </security>
  </system.webServer>
  <runtime>
    <assemblyBinding xmlns="urn:schemas-microsoft-com:asm.v1">
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Helpers" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.Mvc" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-4.0.0.0" newVersion="4.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="System.Web.WebPages" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="1.0.0.0-2.0.0.0" newVersion="2.0.0.0" />
      </dependentAssembly>
      <dependentAssembly>
        <assemblyIdentity name="WebGrease" publicKeyToken="31bf3856ad364e35" />
        <bindingRedirect oldVersion="0.0.0.0-1.3.0.0" newVersion="1.3.0.0" />
      </dependentAssembly>
    </assemblyBinding>
  </runtime>
  <entityFramework>
    <defaultConnectionFactory type="System.Data.Entity.Infrastructure.LocalDbConnectionFactory, EntityFramework">
      <parameters>
        <parameter value="v11.0" />
      </parameters>
    </defaultConnectionFactory>
  </entityFramework>
</configuration>